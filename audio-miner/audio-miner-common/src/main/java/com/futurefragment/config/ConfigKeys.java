package com.futurefragment.config;

public final class ConfigKeys {

	private ConfigKeys() {
	}

	// JWT Config Keys
	public static final String JWT_SECRET = "jwt.secret";
	public static final String JWT_ACCESS_TOKEN_TTL = "jwt.access.token.ttl";
	public static final String JWT_REFRESH_TOKEN_TTL = "jwt.refresh.token.ttl";

	// Google Config Keys
	public static final String GOOGLE_WEB_CLIENT_ID = "google.web.client.id";
	
	// AWS Config Keys
	public static final String AWS_S3_BUCKET = "aws.s3.bucket";
	public static final String AWS_S3_REGION = "aws.s3.region";
	public static final String AWS_S3_ACCESS_KEY_ID = "aws.s3.access.key.id";
	public static final String AWS_S3_SECRET_KEY_ID = "aws.s3.secret.key.id";
	
}
