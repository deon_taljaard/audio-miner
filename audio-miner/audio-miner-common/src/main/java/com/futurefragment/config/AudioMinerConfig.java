package com.futurefragment.config;

import org.apache.deltaspike.core.api.config.ConfigResolver;

public final class AudioMinerConfig {

	private AudioMinerConfig() {
	}

	// JWT Configs
	public static final String JWT_SECRET = ConfigResolver.getPropertyValue(ConfigKeys.JWT_SECRET);
	public static final String ACCESS_TOKEN_TTL = ConfigResolver.getPropertyValue(ConfigKeys.JWT_ACCESS_TOKEN_TTL);
	public static final String REFRESH_TOKEN_TTL = ConfigResolver.getPropertyValue(ConfigKeys.JWT_REFRESH_TOKEN_TTL);

	// Google Configs
	public static final String GOOGLE_WEB_CLIENT_ID = ConfigResolver.getPropertyValue(ConfigKeys.GOOGLE_WEB_CLIENT_ID);

	// AWS Configs
	public static final String AWS_S3_BUCKET = ConfigResolver.getPropertyValue(ConfigKeys.AWS_S3_BUCKET);
	public static final String AWS_S3_REGION = ConfigResolver.getPropertyValue(ConfigKeys.AWS_S3_REGION);
	public static final String AWS_S3_ACCESS_KEY_ID = ConfigResolver.getPropertyValue(ConfigKeys.AWS_S3_ACCESS_KEY_ID);
	public static final String AWS_S3_SECRET_KEY_ID = ConfigResolver.getPropertyValue(ConfigKeys.AWS_S3_SECRET_KEY_ID);
}
