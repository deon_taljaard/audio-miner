package com.futurefragment.enumlookups;

public enum ResourceStatusLookup {
	ACTIVE(1L), INACTIVE(2L), AWAITING_APPROVAL(3L), DELETED(4L), COOL_OFF(5L);

	private Long id;

	private ResourceStatusLookup(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public static String getValuesAsCommaString() {
		StringBuilder sb = new StringBuilder();

		ResourceStatusLookup[] values = values();
		int length = values.length;
		for (int i = 0; i < length; i++) {
			sb.append(values[i].name().toLowerCase());
			if (i < length - 1) {
				sb.append(", ");
			}
		}

		return sb.toString();
	}
}
