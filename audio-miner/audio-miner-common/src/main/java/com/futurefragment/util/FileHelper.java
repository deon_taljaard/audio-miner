package com.futurefragment.util;

import com.futurefragment.dto.common.FileDto;

public final class FileHelper {

	private static final String FORMAT = "%s/%s/%s";

	private FileHelper() {
	}

	public static String getPath(FileDto fileDto) {
		return String.format(FORMAT, fileDto.getPersonS3Key(), fileDto.getFileType().name().toLowerCase(),
				fileDto.getFileS3Key());
	}
}
