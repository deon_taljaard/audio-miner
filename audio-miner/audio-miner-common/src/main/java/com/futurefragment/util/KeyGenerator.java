package com.futurefragment.util;

import java.util.Base64;
import java.util.UUID;

import org.mindrot.jbcrypt.BCrypt;

public final class KeyGenerator {

	public static void main(String... args) {
		System.out.println(newKey());
	}
	
	private KeyGenerator() {
	}

	public static String newKey() {
		return newKey(UUID.randomUUID().toString(), true);
	}

	public static String newKey(String input, boolean base64) {
		String keyHash = BCrypt.hashpw(input, BCrypt.gensalt(10));
		return base64 ? Base64.getEncoder().encodeToString(keyHash.getBytes()) : keyHash;
	}
}
