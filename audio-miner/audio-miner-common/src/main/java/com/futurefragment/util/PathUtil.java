package com.futurefragment.util;

import java.io.UnsupportedEncodingException;
import java.util.Base64;

import com.futurefragment.config.AudioMinerConfig;
import com.futurefragment.constant.CommonConstants;

public final class PathUtil {

	private PathUtil() {
	}

	public static String buildPathFromArgs(String... args) {
		StringBuilder sb = new StringBuilder();

		for (int i = 0; i < args.length; i++) {
			sb.append(args[i]);
			if (i < args.length - 1) {
				sb.append(CommonConstants.S3_PATH_SEPARATOR);
			}
		}

		return sb.toString();
	}

	public static String buildPersonFilePath(String personKey, String fileType, String fileKey) {
		return String.format(CommonConstants.PERSON_FILE_PATH_FORMAT, AudioMinerConfig.AWS_S3_BUCKET, personKey,
				fileType, fileKey);
	}

	public static String getS3KeyFromPath(String path) {
		if (path.contains(CommonConstants.S3_PATH_SEPARATOR)) {
			return path.substring(path.lastIndexOf(CommonConstants.S3_PATH_SEPARATOR), path.length());
		} else {
			return path;
		}
	}

	public static String getDecodedS3KeyFromPath(String path) throws UnsupportedEncodingException {
		String s3KeyFromPath = getS3KeyFromPath(path);
		byte[] decodedKey = Base64.getDecoder().decode(s3KeyFromPath);
		return new String(decodedKey, "utf-8");
	}
}
