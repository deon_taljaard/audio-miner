package com.futurefragment.constant;

public class ExceptionConstants {

	// Application layers

	public static final String APPLICATION_LAYER_RESTFUL = "Restful";
	public static final String APPLICATION_LAYER_CORE = "Core";
	public static final String APPLICATION_LAYER_PERSISTENCE = "Persistence";
	public static final String APPLICATION_LAYER_INTEGRATION = "Integration";

	// Error messages and correction

	public static final String MSG = " Msg: ";
	public static final String ERR_CONTACT_ADMIN_USER_CORRECTION = "Please contact the system administrator.";

}
