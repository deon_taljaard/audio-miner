package com.futurefragment.constant;

public final class CommonConstants {

	private CommonConstants() {
	}

	public static final String S3_PATH_SEPARATOR = "/";

	// Resource path constants
	public static final String PERSONS = "persons";
	public static final String VOCAB_PACKAGES = "vocabPackages";
	public static final String WORDS = "words";
	public static final String IMAGES = "images";
	public static final String AUDIOS = "audios";

	// <s3url://bucket_name/persons/><person_key>/<iamge|audio>/<file_key>
	public static final String PERSON_FILE_PATH_FORMAT = "%s/persons/%s/%s/%s";
}
