package com.futurefragment.exception;

import java.util.List;

public final class AudioMinerExceptionFactory {

    private AudioMinerExceptionFactory() {
    }

    public static AudioMinerException raiseException(ExceptionContext exceptionContext) {
        AudioMinerException voiceCollageException = new AudioMinerException();

        voiceCollageException.getExceptionContexts().add(exceptionContext);

        return voiceCollageException;
    }

    public static AudioMinerException raiseException(List<ExceptionContext> exceptionContexts) {
        return new AudioMinerException(exceptionContexts);
    }
}
