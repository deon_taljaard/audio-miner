package com.futurefragment.exception;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class ExceptionContext implements Serializable {

	private static final long serialVersionUID = 1L;

	private static final String EXCEPTION_CONTEXT_ID = "%s/%s/%s";

	/**
	 * The cause that gets wrapped.
	 */
	private final Throwable cause;

	/**
	 * A string representation of where in the code the exception was thrown.
	 * This context id is constructed in the form of <APPLICATION_LAYER>/
	 * <CLASS_NAME>/<METHOD_NAME>
	 */
	private String contextId;

	/**
	 * The severity of the error. E.g. INFO, WARNING, ERROR, FATAL.
	 */
	protected Severity severity;

	/**
	 * The user error description. Contains the error description to show to the
	 * user.
	 */
	private String userErrorDescription;

	/**
	 * The user error correction. Contains the error correction to show to the
	 * user.
	 */
	private String userErrorCorrection;

	/**
	 * The administrator error description. Contains the error description to
	 * show to the administrator.
	 */
	private String adminErrorDescription;

	/**
	 * The administrator error correction. Contains the error correction to show
	 * to the administrator.
	 */
	private String adminErrorCorrection;

	/**
	 * The error type. A Map of any additional parameters needed to construct a
	 * meaningful error description, either for the users or the application
	 * operators and developers.
	 *
	 * For instance, if a file fails to load, the file name could be kept in
	 * this map. Or, if an operation fails which require 3 parameters to
	 * succeed, the names and values of each parameter could be kept in this
	 * Map.
	 */
	protected Map<String, Object> parameters = new HashMap<>();

	/**
	 * The error trace id. Contains a unique trace id that can be used across
	 * applications and layers to trace an error. This can be used to link up a
	 * error message on the device or admin site to an error on the core rest
	 * services for example. Will be generated when the error is instantiated
	 * and logged appropriately.
	 */
	protected String traceUuid = null;

	private ExceptionContext() {
		this.cause = null;
	}

	private ExceptionContext(final String applicationLayer, final String className, final String methodName,
			final Throwable error) {
		this.contextId = String.format(EXCEPTION_CONTEXT_ID, applicationLayer, className, methodName);
		this.cause = error;
		this.traceUuid = UUID.randomUUID().toString();
	}

	public static ExceptionContext getInstance(final String applicationLayer, final String className,
			final String methodName, final Throwable error) {
		return new ExceptionContext(applicationLayer, className, methodName, error);
	}

	public ExceptionContext setContextId(String contextId) {
		this.contextId = contextId;
		return this;
	}

	public String getContextId() {
		return contextId;
	}

	public Throwable getCause() {
		return cause;
	}

	public String getUserErrorDescription() {
		return userErrorDescription;
	}

	public ExceptionContext setUserErrorDescription(String userErrorDescription) {
		this.userErrorDescription = userErrorDescription;
		return this;
	}

	public String getUserErrorCorrection() {
		return userErrorCorrection;
	}

	public ExceptionContext setUserErrorCorrection(String userErrorCorrection) {
		this.userErrorCorrection = userErrorCorrection;
		return this;
	}

	public String getAdminErrorDescription() {
		return adminErrorDescription;
	}

	public ExceptionContext setAdminErrorDescription(String adminErrorDescription) {
		this.adminErrorDescription = adminErrorDescription;
		return this;
	}

	public String getAdminErrorCorrection() {
		return adminErrorCorrection;
	}

	public ExceptionContext setAdminErrorCorrection(String adminErrorCorrection) {
		this.adminErrorCorrection = adminErrorCorrection;
		return this;
	}

	public Severity getSeverity() {
		return severity;
	}

	public ExceptionContext setSeverity(Severity severity) {
		this.severity = severity;
		return this;
	}

	public Map<String, Object> getParameters() {
		return parameters;
	}

	public ExceptionContext setParameters(Map<String, Object> parameters) {
		this.parameters = parameters;
		return this;
	}

	public ExceptionContext addParameter(String paramName, Object value) {
		this.parameters.put(paramName, value);
		return this;
	}

	public ExceptionContext addParameters(Map<String, Object> newParameters) {
		newParameters.forEach((key, value) -> parameters.put(key, value));
		return this;
	}

	public String getTraceUuid() {
		return traceUuid;
	}

	public void setTraceUuid(String traceUuid) {
		this.traceUuid = traceUuid;
	}
}
