package com.futurefragment.exception;

public enum Severity {

    DEBUG, INFO, WARNING, ERROR, FATAL;
}
