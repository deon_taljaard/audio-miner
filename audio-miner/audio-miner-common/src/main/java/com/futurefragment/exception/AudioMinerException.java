package com.futurefragment.exception;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

public class AudioMinerException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    private List<ExceptionContext> exceptionContexts;

    protected static final String CAUSED_BY = "Caused by: ";
    protected static final String MSG_SPACER = " - ";
    protected static final String NEWLINE_STRING = System.lineSeparator();

    protected static final String ADMIN_ERROR_DESCRIPTION = "Admin Error Description: ";
    protected static final String ADMIN_ERROR_CORRECTION = "Admin Error Correction: ";
    protected static final String SEVERITY = "Severity: ";
    protected static final String CONTEXT = "Context: ";

    public AudioMinerException() {
        this.exceptionContexts = new ArrayList<>();
    }

    public AudioMinerException(List<ExceptionContext> exceptionContexts) {
        this.exceptionContexts = exceptionContexts;
    }

    public List<ExceptionContext> getExceptionContexts() {
        return exceptionContexts;
    }

    public String getLogMessage() {

        StringBuilder builder = new StringBuilder();

        builder.append(ADMIN_ERROR_DESCRIPTION).append(getAdministratorErrorDescription()).append(NEWLINE_STRING);
        builder.append(ADMIN_ERROR_CORRECTION).append(getAdministratorErrorCorrection()).append(NEWLINE_STRING);
        builder.append(SEVERITY).append(getSeverity()).append(NEWLINE_STRING);
        builder.append(CONTEXT).append(getContext()).append(NEWLINE_STRING);

        return builder.toString();
    }

    public String getAdministratorErrorDescription() {
        String adminErrorDescription = null;

        ListIterator<ExceptionContext> listIterator = exceptionContexts.listIterator(exceptionContexts.size());
        while (listIterator.hasPrevious()) {
            ExceptionContext exceptionContext = listIterator.previous();
            if (exceptionContext.getAdminErrorDescription() != null) {
                adminErrorDescription = exceptionContext.getAdminErrorDescription();
                break;
            }
        }

        return adminErrorDescription;
    }

    public String getAdministratorErrorCorrection() {
        String adminErrorCorrection = null;

        ListIterator<ExceptionContext> listIterator = exceptionContexts.listIterator(exceptionContexts.size());
        while (listIterator.hasPrevious()) {
            ExceptionContext exceptionContext = listIterator.previous();
            if (exceptionContext.getAdminErrorCorrection() != null) {
                adminErrorCorrection = exceptionContext.getAdminErrorCorrection();
                break;
            }
        }

        return adminErrorCorrection;
    }

    public String getUserErrorDescription() {
        String userErrorDescription = null;

        ListIterator<ExceptionContext> listIterator = exceptionContexts.listIterator(exceptionContexts.size());
        while (listIterator.hasPrevious()) {
            ExceptionContext exceptionContext = listIterator.previous();
            if (exceptionContext.getUserErrorDescription() != null) {
                userErrorDescription = exceptionContext.getUserErrorDescription();
                break;
            }
        }

        return userErrorDescription;
    }

    public String getUserErrorCorrection() {
        String userErrorCorrection = null;

        ListIterator<ExceptionContext> listIterator = exceptionContexts.listIterator(exceptionContexts.size());
        while (listIterator.hasPrevious()) {
            ExceptionContext exceptionContext = listIterator.previous();
            if (exceptionContext.getUserErrorCorrection() != null) {
                userErrorCorrection = exceptionContext.getUserErrorCorrection();
                break;
            }
        }

        return userErrorCorrection;
    }

    public Severity getSeverity() {
        Severity severity = null;

        ListIterator<ExceptionContext> listIterator = exceptionContexts.listIterator(exceptionContexts.size());
        while (listIterator.hasPrevious()) {
            ExceptionContext exceptionContext = listIterator.previous();
            if (exceptionContext.getAdminErrorCorrection() != null) {
                severity = exceptionContext.getSeverity();
                break;
            }
        }

        return severity;
    }

    public String getContext() {
        String context = null;

        ListIterator<ExceptionContext> listIterator = exceptionContexts.listIterator(exceptionContexts.size());
        while (listIterator.hasPrevious()) {
            ExceptionContext exceptionContext = listIterator.previous();
            if (exceptionContext.getContextId() != null) {
                context = exceptionContext.getContextId();
                break;
            }
        }

        return context;
    }

}
