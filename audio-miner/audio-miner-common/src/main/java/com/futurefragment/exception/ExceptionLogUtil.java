package com.futurefragment.exception;

import java.util.List;

import org.slf4j.Logger;

public class ExceptionLogUtil {

	private static final String ORIGINAL_EX = "Original Exception: ";
	private static final String ADMIN_ERROR_DESCRIPTION = "Admin Error Description: ";
	private static final String ADMIN_ERROR_CORRECTION = "Admin Error Correction: ";
	private static final String SEVERITY = "Severity: ";
	private static final String CONTEXT = "Context: ";
	private static final String TRACE_MESSAGE_UUID = "Trace Id: ";
	private static final String PARAMETERS = "Parameters: ";

	protected static final String CAUSED_BY = "Caused by: ";
	protected static final String MSG_SPACER = " - ";
	protected static final String NEWLINE_STRING = System.lineSeparator();

	private ExceptionLogUtil() {
	}

	public static void logError(AudioMinerException se, Logger logger) {
		if (logger == null) {
			return;
		}

		if (se != null) {
			List<ExceptionContext> errorContextList = se.getExceptionContexts();

			if (errorContextList != null) {
				logErrorContextList(errorContextList, logger);
			} else {
				logger.warn("Error: context list is empty.");
			}
		} else {
			logger.warn("VoiceCollageException is null.");
		}
	}

	private static void logErrorContextList(List<ExceptionContext> exceptionContexts, Logger logger) {
		exceptionContexts.forEach(exceptionContext -> {
			String logMessage = buildLogMessage(exceptionContext);

			if (exceptionContext == null) {
				logger.info(logMessage);
				return;
			}

			if (exceptionContext.getSeverity() == null) {
				logger.info(logMessage);
				return;
			}

			switch (exceptionContext.getSeverity()) {
			case DEBUG:
				logger.debug(logMessage);
				break;
			case WARNING:
				logger.warn(logMessage);
				break;
			case ERROR:
				logger.error(logMessage);
				break;
			case FATAL:
				logger.error(logMessage);
				break;
			default:
				logger.info(logMessage);
				break;
			}
		});
	}

	private static String buildLogMessage(ExceptionContext errorContextInfo) {
		StringBuilder builder = new StringBuilder();

		if (errorContextInfo != null) {
			builder.append(NEWLINE_STRING);
			if (errorContextInfo.getCause() != null) {
				builder.append(ORIGINAL_EX).append(errorContextInfo.getCause()).append(NEWLINE_STRING);
			}
			builder.append(ADMIN_ERROR_DESCRIPTION).append(errorContextInfo.getAdminErrorDescription())
					.append(NEWLINE_STRING);
			builder.append(ADMIN_ERROR_CORRECTION).append(errorContextInfo.getAdminErrorCorrection())
					.append(NEWLINE_STRING);
			builder.append(SEVERITY).append(errorContextInfo.getSeverity()).append(NEWLINE_STRING);
			builder.append(CONTEXT).append(errorContextInfo.getContextId()).append(NEWLINE_STRING);
			builder.append(TRACE_MESSAGE_UUID).append(errorContextInfo.getTraceUuid()).append(NEWLINE_STRING);

			if (errorContextInfo.getParameters() != null) {
				builder.append(PARAMETERS).append(errorContextInfo.getParameters().toString()).append(NEWLINE_STRING);
			}

			builder.append(NEWLINE_STRING);
		} else {
			builder.append("Error: context info is null");
		}

		return builder.toString();
	}

}
