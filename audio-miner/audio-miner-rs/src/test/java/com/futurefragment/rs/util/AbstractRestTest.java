package com.futurefragment.rs.util;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Map;
import java.util.UUID;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response.Status;

import org.jboss.resteasy.core.Dispatcher;
import org.jboss.resteasy.mock.MockDispatcherFactory;
import org.jboss.resteasy.mock.MockHttpRequest;
import org.jboss.resteasy.mock.MockHttpResponse;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.futurefragment.dto.common.DeleteResourceDto;
import com.futurefragment.dto.common.ErrorDto;
import com.futurefragment.dto.common.ResourceCountDto;
import com.futurefragment.dto.common.FutureFragmentCollectionDto;
import com.futurefragment.dto.common.AudioMinerDto;

public class AbstractRestTest {

	protected final Dispatcher dispatcher = MockDispatcherFactory.createDispatcher();

	private static final String RESPONSE_NOT_NULL = "Response Not Null";
	private static final String RESPONSE_EMPTY_STRING = "Response Empty String";

	protected MockHttpRequest httpRequest;
	protected MockHttpResponse httpResponse;

	protected void assertResponse(MockHttpResponse response, Status expectedStatus) {
		assertNotNull(RESPONSE_NOT_NULL, response.getContentAsString());
		assertFalse(RESPONSE_EMPTY_STRING, response.getContentAsString().isEmpty());
		assertEquals(expectedStatus.getStatusCode(), response.getStatus());
	}

	protected void initMockRequestResponse(RequestType requestType, final String serviceRoot) throws Exception {

		switch (requestType) {
		case GET:
			httpRequest = MockHttpRequest.get(serviceRoot);
			break;
		case DELETE:
			httpRequest = MockHttpRequest.delete(serviceRoot);
			break;
		case POST:
			httpRequest = MockHttpRequest.post(serviceRoot);
			break;
		case PUT:
			httpRequest = MockHttpRequest.put(serviceRoot);
			break;
		default:
			httpRequest = MockHttpRequest.get(serviceRoot);
			break;
		}
		httpResponse = new MockHttpResponse();
	}

	protected void initRequestContent(String acceptType, String contentType, String requestBodyAsString) {
		httpRequest.accept(acceptType);
		httpRequest.contentType(contentType);
		if (requestBodyAsString != null) {
			httpRequest.content(requestBodyAsString.getBytes());
		}
	}

	protected enum RequestType {
		GET, POST, PUT, DELETE;
	}

	protected void initMultipartRequest(String uri, Map<String, Object> parts, String mimeType) throws Exception {
		String boundary = UUID.randomUUID().toString();
		httpRequest.contentType("multipart/form-data; boundary=" + boundary);

		// Make sure this is deleted in afterTest()
		File tmpMultipartFile = Files.createTempFile(null, null).toFile();
		FileWriter fileWriter = new FileWriter(tmpMultipartFile);
		fileWriter.append("--").append(boundary);

		// Sticking to normal for-each, lambda approach has messy exception
		// handling
		for (Map.Entry<String, Object> entry : parts.entrySet()) {
			if ((entry.getValue() instanceof String) || (entry.getValue() instanceof Long)) {
				fileWriter.append("\n");
				fileWriter.append("Content-Disposition: form-data; name=\"" + entry.getKey() + "\"").append("\n");
				fileWriter.append("Content-Type: " + MediaType.TEXT_PLAIN).append("\n\n");
				fileWriter.append(entry.getValue().toString()).append("\n");
				fileWriter.append("--").append(boundary);
			} else if (entry.getValue() instanceof File) {
				fileWriter.append("\n");
				File val = (File) entry.getValue();
				fileWriter.append(String.format("Content-Disposition: form-data; name=\"%s\"; filename=\"%s\"",
						entry.getKey(), val.getName())).append("\n");
				fileWriter.append("Content-Type: " + mimeType).append("\n\n");

				FileInputStream fis = new FileInputStream(val);
				int b = fis.read();
				while (b >= 0) {
					fileWriter.write(b);
					b = fis.read();
				}
				fileWriter.append("\n").append("--").append(boundary);
				fis.close();
			} else if (entry.getValue() instanceof FutureFragmentCollectionDto) {
				fileWriter.append("\n");
				fileWriter.append("Content-Disposition: form-data; name=\"" + entry.getKey() + "\"").append("\n");
				fileWriter.append("Content-Type: " + MediaType.APPLICATION_JSON).append("\n\n");
				fileWriter.append(writeDtoToJsonString(entry.getValue())).append("\n");
				fileWriter.append("--").append(boundary);
			}
		}
		fileWriter.append("--");
		fileWriter.flush();
		fileWriter.close();

		httpRequest.setInputStream(new FileInputStream(tmpMultipartFile));
		// return mockRequest;
	}

	protected String writeDtoToJsonString(Object value) throws JsonProcessingException {
		return new ObjectMapper().writer().withoutRootName().writeValueAsString(value);
	}

	protected ErrorDto readErrorDtoFromJsonString(String jsonString) throws JsonProcessingException, IOException {
		return new ObjectMapper().readerFor(new TypeReference<ErrorDto>() {
		}).readValue(jsonString);
	}

	protected ResourceCountDto readResourceCountDtoFromJsonString(String jsonString)
			throws JsonProcessingException, IOException {
		return new ObjectMapper().readerFor(new TypeReference<ResourceCountDto>() {
		}).readValue(jsonString);
	}

	protected DeleteResourceDto readDeleteResourceDtoFromJsonString(String jsonString)
			throws JsonProcessingException, IOException {
		return new ObjectMapper().readerFor(new TypeReference<DeleteResourceDto>() {
		}).readValue(jsonString);
	}

	protected <T extends AudioMinerDto> void verifyValidResponse(FutureFragmentCollectionDto<T> collection) {
		assertThat(httpResponse.getStatus(), is(equalTo(Status.OK.getStatusCode())));
		assertNotNull(collection);
		assertNotNull(collection.getData());
	}

	protected void verifyErrorResponse(ErrorDto error) {
		assertThat(httpResponse.getStatus(), is(greaterThanOrEqualTo(Status.BAD_REQUEST.getStatusCode())));
		assertNotNull(error);
		assertNotNull(error.getDescription());
		assertNotNull(error.getCorrection());
	}

	protected void verifyResourceCountResponse(ResourceCountDto resourceCount) {
		assertThat(httpResponse.getStatus(), is(equalTo(Status.OK.getStatusCode())));
		assertNotNull(resourceCount);
		assertNotNull(resourceCount.getResource());
		assertNotNull(resourceCount.getCount());
	}

	protected void verifyDeleteResourceResponse(DeleteResourceDto deleteResource) {
		assertThat(httpResponse.getStatus(), is(equalTo(Status.OK.getStatusCode())));
		assertNotNull(deleteResource);
		assertNotNull(deleteResource.getResource());
		assertNotNull(deleteResource.getId());
	}
}