package com.futurefragment.rs.validation;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;
import org.junit.Before;
import org.junit.Test;

import com.futurefragment.exception.AudioMinerException;

public class MultipartFormDataInputValidationUtilTest {

	private MultipartFormDataInput newForm = null;
	private InputPart input = null;

	@Before
	public void setup() {
		newForm = mock(MultipartFormDataInput.class);
		input = mock(InputPart.class);
	}

	@Test
	public void shouldPassValidateMultipartFormDataInput() throws IOException {
		// arrange
		Map<String, List<InputPart>> paramsMap = new HashMap<>();
		paramsMap.put("Token", Arrays.asList(input));

		when(newForm.getFormDataMap()).thenReturn(paramsMap);
		when(input.getBodyAsString()).thenReturn("expected token param body");

		// act
		MultipartFormDataInputValidationUtil.validateMultipartFormDataInput(newForm);
	}

	@Test
	public void shouldFailValidateMultipartFormDataInput() {
		// arrange
		Map<String, List<InputPart>> paramsMap = new HashMap<>();

		when(newForm.getFormDataMap()).thenReturn(paramsMap);

		try {
			// act
			MultipartFormDataInputValidationUtil.validateMultipartFormDataInput(newForm);
			fail("AudioMinerException expected");
		} catch (AudioMinerException ex) {
			assertNotNull(ex);
			assertThat(ex.getAdministratorErrorDescription(),
					is(equalTo(MultipartFormDataInputValidationUtil.ERR_INVALID_MULTIPART_FORM_DATA_DESCRIPTION)));
			assertThat(ex.getAdministratorErrorCorrection(),
					is(equalTo(MultipartFormDataInputValidationUtil.ERR_INVALID_MULTIPART_FORM_DATA_CORRECTION)));
		}
	}

}
