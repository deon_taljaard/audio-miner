package com.futurefragment.rs.api;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.List;

import javax.ws.rs.core.MediaType;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.futurefragment.api.ApiKeyResourceStatusManager;
import com.futurefragment.constant.ExceptionConstants;
import com.futurefragment.dto.common.ErrorDto;
import com.futurefragment.dto.common.FutureFragmentCollectionDto;
import com.futurefragment.dto.common.ResourceStatusDto;
import com.futurefragment.dto.common.builder.ResourceStatusDtoBuilder;
import com.futurefragment.exception.AudioMinerExceptionFactory;
import com.futurefragment.exception.ExceptionContext;
import com.futurefragment.rs.util.AbstractRestTest;
import com.futurefragment.rs.util.RsTestConstants;

@RunWith(MockitoJUnitRunner.class)
public class ApiKeyResourceStatusRsTest extends AbstractRestTest {

	private static final String CLASS_NAME = ApiKeyResourceStatusRsTest.class.getSimpleName();
	private static final ExceptionContext VC_EXCEPTION = ExceptionContext
			.getInstance(ExceptionConstants.APPLICATION_LAYER_RESTFUL, CLASS_NAME, "test",
					new RuntimeException("error!"))
			.setAdminErrorDescription("description").setAdminErrorCorrection("correction");

	@Mock
	private ApiKeyResourceStatusManager apiKeyResourceStatusManager;

	@InjectMocks
	private ApiKeyResourceStatusRs apiKeyResourceStatusRs;

	@Captor
	private ArgumentCaptor<List<ResourceStatusDto>> argumentCaptor;

	private static final String REST_SERVICE_ROOT = "/apiKeyResourceStatus";

	@Before
	public void setup() {
		dispatcher.getRegistry().addSingletonResource(apiKeyResourceStatusRs);
		dispatcher.getProviderFactory().createResponseBuilder();
	}

	@Test
	public void shouldGetApiKeyResourceStatusById() throws Exception {
		// arrange
		Long id = 1L;
		when(apiKeyResourceStatusManager.getApiKeyResourceStatusByApiKeyId(anyLong()))
				.thenReturn(ResourceStatusDtoBuilder.createResourceStatus(1));
		initMockRequestResponse(RequestType.GET, REST_SERVICE_ROOT + RsTestConstants.PATH_SEPARATOR + id);
		initRequestContent(MediaType.APPLICATION_JSON, MediaType.APPLICATION_JSON, null);

		// act
		dispatcher.invoke(httpRequest, httpResponse);

		FutureFragmentCollectionDto<ResourceStatusDto> apiKeyResourceStatuses = readApiKeyDtoFromJsonString(
				httpResponse.getContentAsString());

		// assert
		verifyValidResponse(apiKeyResourceStatuses);
		assertThat(apiKeyResourceStatuses.getData().size(), is(equalTo(1)));
		verify(apiKeyResourceStatusManager, times(1)).getApiKeyResourceStatusByApiKeyId(anyLong());
	}

	@Test
	public void shouldHandleExceptionWhenCallingGetApiKeyResourceStatusById() throws Exception {
		// arrange
		Long id = 1L;
		when(apiKeyResourceStatusManager.getApiKeyResourceStatusByApiKeyId(anyLong()))
				.thenThrow(AudioMinerExceptionFactory.raiseException(VC_EXCEPTION));
		initMockRequestResponse(RequestType.GET, REST_SERVICE_ROOT + RsTestConstants.PATH_SEPARATOR + id);
		initRequestContent(MediaType.APPLICATION_JSON, MediaType.APPLICATION_JSON, null);

		// act
		dispatcher.invoke(httpRequest, httpResponse);

		ErrorDto error = readErrorDtoFromJsonString(httpResponse.getContentAsString());

		// assert
		verifyErrorResponse(error);
		verify(apiKeyResourceStatusManager, times(1)).getApiKeyResourceStatusByApiKeyId(anyLong());
	}

	@Test
	public void shouldUpdateApiKeyResourceStatus() throws Exception {
		// arrange
		when(apiKeyResourceStatusManager.updateApiKeyResourceStatus(argumentCaptor.capture()))
				.thenReturn(ResourceStatusDtoBuilder.getResourceStatus());
		initMockRequestResponse(RequestType.PUT, REST_SERVICE_ROOT);
		initRequestContent(MediaType.APPLICATION_JSON, MediaType.APPLICATION_JSON,
				writeDtoToJsonString(getVoiceCollageResourceStatusCollection()));

		// act
		dispatcher.invoke(httpRequest, httpResponse);

		FutureFragmentCollectionDto<ResourceStatusDto> words = readApiKeyDtoFromJsonString(
				httpResponse.getContentAsString());

		// assert
		verifyValidResponse(words);
		verify(apiKeyResourceStatusManager, times(1)).updateApiKeyResourceStatus(argumentCaptor.capture());
	}

	@Test
	public void shouldHandleExceptionWhenUpdatingApiKeyResourceStatus() throws Exception {
		// arrange
		when(apiKeyResourceStatusManager.updateApiKeyResourceStatus(argumentCaptor.capture()))
				.thenThrow(AudioMinerExceptionFactory.raiseException(VC_EXCEPTION));
		initMockRequestResponse(RequestType.PUT, REST_SERVICE_ROOT);
		initRequestContent(MediaType.APPLICATION_JSON, MediaType.APPLICATION_JSON,
				writeDtoToJsonString(getVoiceCollageResourceStatusCollection()));

		// act
		dispatcher.invoke(httpRequest, httpResponse);

		ErrorDto error = readErrorDtoFromJsonString(httpResponse.getContentAsString());

		// assert
		verifyErrorResponse(error);
		verify(apiKeyResourceStatusManager, times(1)).updateApiKeyResourceStatus(argumentCaptor.capture());
	}

	private FutureFragmentCollectionDto<ResourceStatusDto> getVoiceCollageResourceStatusCollection() {
		FutureFragmentCollectionDto<ResourceStatusDto> wordCollectionToUpdate = new FutureFragmentCollectionDto<>();
		wordCollectionToUpdate.setData(ResourceStatusDtoBuilder.getResourceStatus());
		return wordCollectionToUpdate;
	}

	private FutureFragmentCollectionDto<ResourceStatusDto> readApiKeyDtoFromJsonString(String jsonString)
			throws JsonProcessingException, IOException {
		return new ObjectMapper().readerFor(new TypeReference<FutureFragmentCollectionDto<ResourceStatusDto>>() {
		}).readValue(jsonString);
	}
}
