package com.futurefragment.rs.api;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.List;

import javax.ws.rs.core.MediaType;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.futurefragment.api.ApiKeyManager;
import com.futurefragment.constant.ExceptionConstants;
import com.futurefragment.dto.api.ApiKeyDto;
import com.futurefragment.dto.api.builder.ApiKeyDtoBuilder;
import com.futurefragment.dto.common.DeleteResourceDto;
import com.futurefragment.dto.common.ErrorDto;
import com.futurefragment.dto.common.FutureFragmentCollectionDto;
import com.futurefragment.exception.AudioMinerExceptionFactory;
import com.futurefragment.exception.ExceptionContext;
import com.futurefragment.rs.util.AbstractRestTest;
import com.futurefragment.rs.util.RsTestConstants;

@RunWith(MockitoJUnitRunner.class)
public class ApiKeyRsTest extends AbstractRestTest {

	private static final String CLASS_NAME = ApiKeyRsTest.class.getSimpleName();
	private static final ExceptionContext VC_EXCEPTION = ExceptionContext
			.getInstance(ExceptionConstants.APPLICATION_LAYER_RESTFUL, CLASS_NAME, "test",
					new RuntimeException("error!"))
			.setAdminErrorDescription("description").setAdminErrorCorrection("correction");

	@Mock
	private ApiKeyManager apiKeyManager;

	// @Mock
	// private UriInfo uriInfo;

	@InjectMocks
	private ApiKeyRs apiKeyRs;

	@Captor
	private ArgumentCaptor<List<ApiKeyDto>> argumentCaptor;

	private static final String REST_SERVICE_ROOT = "/apiKeys";

	@Before
	public void setup() {
		dispatcher.getRegistry().addSingletonResource(apiKeyRs);
		dispatcher.getProviderFactory().createResponseBuilder();
	}

	@Test
	public void shouldGetApiKeyById() throws Exception {
		// arrange
		Long id = 1L;
		when(apiKeyManager.getApiKeyById(anyLong())).thenReturn(ApiKeyDtoBuilder.createApiKey(1));
		initMockRequestResponse(RequestType.GET, REST_SERVICE_ROOT + RsTestConstants.PATH_SEPARATOR + id);
		initRequestContent(MediaType.APPLICATION_JSON, MediaType.APPLICATION_JSON, null);

		// act
		dispatcher.invoke(httpRequest, httpResponse);

		FutureFragmentCollectionDto<ApiKeyDto> apiKeys = readApiKeyDtoFromJsonString(httpResponse.getContentAsString());

		// assert
		verifyValidResponse(apiKeys);
		assertThat(apiKeys.getData().size(), is(equalTo(1)));
		verify(apiKeyManager, times(1)).getApiKeyById(anyLong());
	}

	@Test
	public void shouldHandleExceptionWhenCallingGetApiKeyById() throws Exception {
		// arrange
		Long id = 1L;
		when(apiKeyManager.getApiKeyById(anyLong()))
				.thenThrow(AudioMinerExceptionFactory.raiseException(VC_EXCEPTION));
		initMockRequestResponse(RequestType.GET, REST_SERVICE_ROOT + RsTestConstants.PATH_SEPARATOR + id);
		initRequestContent(MediaType.APPLICATION_JSON, MediaType.APPLICATION_JSON, null);

		// act
		dispatcher.invoke(httpRequest, httpResponse);

		ErrorDto error = readErrorDtoFromJsonString(httpResponse.getContentAsString());

		// assert
		verifyErrorResponse(error);
		verify(apiKeyManager, times(1)).getApiKeyById(anyLong());
	}

	@Test
	public void shouldUpdateApiKey() throws Exception {
		// arrange
		when(apiKeyManager.updateApiKeys(argumentCaptor.capture())).thenReturn(ApiKeyDtoBuilder.getApiKeys());
		initMockRequestResponse(RequestType.PUT, REST_SERVICE_ROOT);
		initRequestContent(MediaType.APPLICATION_JSON, MediaType.APPLICATION_JSON,
				writeDtoToJsonString(getVoiceCollageApiKeyCollection(true, true)));

		// act
		dispatcher.invoke(httpRequest, httpResponse);

		FutureFragmentCollectionDto<ApiKeyDto> words = readApiKeyDtoFromJsonString(httpResponse.getContentAsString());

		// assert
		verifyValidResponse(words);
		verify(apiKeyManager, times(1)).updateApiKeys(argumentCaptor.capture());
	}

	@Test
	public void shouldHandleExceptionWhenUpdatingApiKey() throws Exception {
		// arrange
		when(apiKeyManager.updateApiKeys(argumentCaptor.capture()))
				.thenThrow(AudioMinerExceptionFactory.raiseException(VC_EXCEPTION));
		initMockRequestResponse(RequestType.PUT, REST_SERVICE_ROOT);
		initRequestContent(MediaType.APPLICATION_JSON, MediaType.APPLICATION_JSON,
				writeDtoToJsonString(getVoiceCollageApiKeyCollection(true, true)));

		// act
		dispatcher.invoke(httpRequest, httpResponse);

		ErrorDto error = readErrorDtoFromJsonString(httpResponse.getContentAsString());

		// assert
		verifyErrorResponse(error);
		verify(apiKeyManager, times(1)).updateApiKeys(argumentCaptor.capture());
	}

	@Test
	public void shouldCreateApiKey() throws Exception {
		// arrange
		when(apiKeyManager.createApiKeys(argumentCaptor.capture())).thenReturn(ApiKeyDtoBuilder.getApiKeys());
		initMockRequestResponse(RequestType.POST, REST_SERVICE_ROOT);
		initRequestContent(MediaType.APPLICATION_JSON, MediaType.APPLICATION_JSON,
				writeDtoToJsonString(getVoiceCollageApiKeyCollection(false, false)));

		// act
		dispatcher.invoke(httpRequest, httpResponse);

		FutureFragmentCollectionDto<ApiKeyDto> words = readApiKeyDtoFromJsonString(httpResponse.getContentAsString());

		// assert
		verifyValidResponse(words);
		verify(apiKeyManager, times(1)).createApiKeys(argumentCaptor.capture());
	}

	@Test
	public void shouldHandleExceptionWhenCreatingApiKey() throws Exception {
		// arrange
		when(apiKeyManager.createApiKeys(argumentCaptor.capture()))
				.thenThrow(AudioMinerExceptionFactory.raiseException(VC_EXCEPTION));
		initMockRequestResponse(RequestType.POST, REST_SERVICE_ROOT);
		initRequestContent(MediaType.APPLICATION_JSON, MediaType.APPLICATION_JSON,
				writeDtoToJsonString(getVoiceCollageApiKeyCollection(false, false)));

		// act
		dispatcher.invoke(httpRequest, httpResponse);

		ErrorDto error = readErrorDtoFromJsonString(httpResponse.getContentAsString());

		// assert
		verifyErrorResponse(error);
		verify(apiKeyManager, times(1)).createApiKeys(argumentCaptor.capture());
	}

	@Test
	public void shoulDeleteApiKeyById() throws Exception {
		// arrange
		Long id = 1L;
		when(apiKeyManager.deleteApiKey(anyLong())).thenReturn(true);
		initMockRequestResponse(RequestType.DELETE, REST_SERVICE_ROOT + RsTestConstants.PATH_SEPARATOR + id);
		initRequestContent(MediaType.APPLICATION_JSON, MediaType.APPLICATION_JSON, null);

		// act
		dispatcher.invoke(httpRequest, httpResponse);

		DeleteResourceDto deleteResource = readDeleteResourceDtoFromJsonString(httpResponse.getContentAsString());

		// assert
		verifyDeleteResourceResponse(deleteResource);
		verify(apiKeyManager, times(1)).deleteApiKey(anyLong());
	}

	@Test
	public void shoulHandleExceptionWhenDeletingApiKeyById() throws Exception {
		// arrange
		Long id = 1L;
		when(apiKeyManager.deleteApiKey(anyLong()))
				.thenThrow(AudioMinerExceptionFactory.raiseException(VC_EXCEPTION));
		initMockRequestResponse(RequestType.DELETE, REST_SERVICE_ROOT + RsTestConstants.PATH_SEPARATOR + id);
		initRequestContent(MediaType.APPLICATION_JSON, MediaType.APPLICATION_JSON, null);

		// act
		dispatcher.invoke(httpRequest, httpResponse);

		ErrorDto error = readErrorDtoFromJsonString(httpResponse.getContentAsString());

		// assert
		verifyErrorResponse(error);
		verify(apiKeyManager, times(1)).deleteApiKey(anyLong());
	}

	private FutureFragmentCollectionDto<ApiKeyDto> getVoiceCollageApiKeyCollection(boolean withId, boolean withKey) {
		List<ApiKeyDto> apiKeysToUdate = ApiKeyDtoBuilder.getApiKeys();
		if (!withId) {
			apiKeysToUdate.forEach(w -> w.setId(null));
		}
		if(!withKey) {
			apiKeysToUdate.forEach(w -> w.setKey(null));
		}
		FutureFragmentCollectionDto<ApiKeyDto> apiKeyCollectionToUpdate = new FutureFragmentCollectionDto<>();
		apiKeyCollectionToUpdate.setData(apiKeysToUdate);

		return apiKeyCollectionToUpdate;
	}

	private FutureFragmentCollectionDto<ApiKeyDto> readApiKeyDtoFromJsonString(String jsonString)
			throws JsonProcessingException, IOException {
		return new ObjectMapper().readerFor(new TypeReference<FutureFragmentCollectionDto<ApiKeyDto>>() {
		}).readValue(jsonString);
	}
}
