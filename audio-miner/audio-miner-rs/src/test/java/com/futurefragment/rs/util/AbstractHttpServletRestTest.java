package com.futurefragment.rs.util;

import static org.mockito.Mockito.doAnswer;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.jboss.resteasy.spi.ResteasyProviderFactory;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

public class AbstractHttpServletRestTest extends AbstractRestTest {

	@Mock
	protected HttpServletRequest request;

	protected Map<String, Object> attributes = new HashMap<>();
	protected Map<String, String> headers = new HashMap<>();

	protected void initHttpServletRequest() {
		ResteasyProviderFactory.getContextDataMap().put(HttpServletRequest.class, request);
	}

	protected void addAndMockAttribute(String name, Object value) {
		attributes.put(name, value);
		mockGetAttribute(name);
	}

	protected void addAndMockHeader(String name, String value) {
		headers.put(name, value);
		mockGetHeader(name);
	}

	protected void mockGetAttribute(String name) {
		// mock the returned value of request.getAttribute(String name)
		doAnswer(new Answer<String>() {
			@Override
			public String answer(InvocationOnMock invocation) throws Throwable {
				Object[] args = invocation.getArguments();
				return (String) attributes.get((String) args[0]);
			}
		}).when(request).getAttribute(name);
	}

	protected void mockGetHeader(String name) {
		// mock the returned value of request.getHeader(String name)
		doAnswer(new Answer<String>() {
			@Override
			public String answer(InvocationOnMock invocation) throws Throwable {
				Object[] args = invocation.getArguments();
				return headers.get((String) args[0]);
			}
		}).when(request).getHeader(name);
	}
		
}