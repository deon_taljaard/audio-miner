package com.futurefragment.rs.audio;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.MediaType;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.futurefragment.audio.AudioManager;
import com.futurefragment.constant.ExceptionConstants;
import com.futurefragment.dto.audio.AudioDto;
import com.futurefragment.dto.audio.builder.AudioDtoBuilder;
import com.futurefragment.dto.audio.builder.AudioDtoBuilder.AudioProperties;
import com.futurefragment.dto.common.DeleteResourceDto;
import com.futurefragment.dto.common.ErrorDto;
import com.futurefragment.dto.common.FutureFragmentCollectionDto;
import com.futurefragment.exception.AudioMinerExceptionFactory;
import com.futurefragment.exception.ExceptionContext;
import com.futurefragment.rs.util.AbstractHttpServletRestTest;
import com.futurefragment.rs.util.RsTestConstants;

@RunWith(MockitoJUnitRunner.class)
public class AudioRsTest extends AbstractHttpServletRestTest {

	private static final String CLASS_NAME = AudioRsTest.class.getSimpleName();
	private static final ExceptionContext VC_EXCEPTION = ExceptionContext
			.getInstance(ExceptionConstants.APPLICATION_LAYER_RESTFUL, CLASS_NAME, "test",
					new RuntimeException("error!"))
			.setAdminErrorDescription("description").setAdminErrorCorrection("correction");

	private static final String TEST_AUDIO_FILE_1 = "src/test/resources/testAudio1.wav";

	private static final String FILE_INPUT = "file";

	@Captor
	private ArgumentCaptor<List<AudioDto>> argumentCaptor;

	@Mock
	private AudioManager audioManager;

	@InjectMocks
	private AudioRs audioRs;

	private static final String REST_SERVICE_ROOT = "/audios";

	@Before
	public void setup() {
		initHttpServletRequest();

		dispatcher.getRegistry().addSingletonResource(audioRs);
		dispatcher.getProviderFactory().createResponseBuilder();
	}

	@Test
	public void shouldGetAudioById() throws Exception {
		// arrange
		Long id = 1L;
		when(audioManager.getAudioById(anyLong()))
				.thenReturn(AudioDtoBuilder.singleAudio(1, Arrays.asList(AudioProperties.CUT_AUDIO)));
		initMockRequestResponse(RequestType.GET, REST_SERVICE_ROOT + RsTestConstants.PATH_SEPARATOR + id);
		initRequestContent(MediaType.APPLICATION_JSON, MediaType.APPLICATION_JSON, null);

		// act
		dispatcher.invoke(httpRequest, httpResponse);

		FutureFragmentCollectionDto<AudioDto> audios = readAudioDtoFromJsonString(httpResponse.getContentAsString());

		// assert
		verifyValidResponse(audios);
		assertThat(audios.getData().size(), is(equalTo(1)));
		verify(audioManager, times(1)).getAudioById(anyLong());
	}

	@Test
	public void shouldHandleExceptionWhenCallingGetAudioById() throws Exception {
		// arrange
		Long id = 1L;
		when(audioManager.getAudioById(anyLong())).thenThrow(AudioMinerExceptionFactory.raiseException(VC_EXCEPTION));
		initMockRequestResponse(RequestType.GET, REST_SERVICE_ROOT + RsTestConstants.PATH_SEPARATOR + id);
		initRequestContent(MediaType.APPLICATION_JSON, MediaType.APPLICATION_JSON, null);

		// act
		dispatcher.invoke(httpRequest, httpResponse);

		ErrorDto error = readErrorDtoFromJsonString(httpResponse.getContentAsString());

		// assert
		verifyErrorResponse(error);
		verify(audioManager, times(1)).getAudioById(anyLong());
	}

	@Test
	public void shouldUploadFileAndUpdateAudio() throws Exception {
		// arrange
		Map<String, Object> parts = new HashMap<>();

		parts.put(FILE_INPUT, new File(TEST_AUDIO_FILE_1));
		parts.put("audios", getVoiceCollageAudioCollection(true));

		when(audioManager.updateAudios(argumentCaptor.capture()))
				.thenReturn(AudioDtoBuilder.with(Collections.emptyList()).audios());

		initMockRequestResponse(RequestType.PUT, REST_SERVICE_ROOT);

		initMultipartRequest(REST_SERVICE_ROOT, parts, "audio/mpeg");

		// act
		dispatcher.invoke(httpRequest, httpResponse);

		FutureFragmentCollectionDto<AudioDto> audios = readAudioDtoFromJsonString(httpResponse.getContentAsString());

		// assert
		verifyValidResponse(audios);
		verify(audioManager, times(1)).updateAudios(argumentCaptor.capture());
	}

	@Test
	public void shouldHandleExceptionWhenUploadingFileAndUpdatingAudio() throws Exception {
		// arrange
		Map<String, Object> parts = new HashMap<>();

		parts.put(FILE_INPUT, new File(TEST_AUDIO_FILE_1));
		parts.put("audios", getVoiceCollageAudioCollection(true));

		when(audioManager.updateAudios(argumentCaptor.capture()))
				.thenThrow(AudioMinerExceptionFactory.raiseException(VC_EXCEPTION));

		initMockRequestResponse(RequestType.PUT, REST_SERVICE_ROOT);

		initMultipartRequest(REST_SERVICE_ROOT, parts, "audio/mpeg");

		// act
		dispatcher.invoke(httpRequest, httpResponse);

		ErrorDto error = readErrorDtoFromJsonString(httpResponse.getContentAsString());

		// assert
		verifyErrorResponse(error);
		verify(audioManager, times(1)).updateAudios(argumentCaptor.capture());
	}

	@Test
	public void shouldUploadFileAndCraeteAudio() throws Exception {
		// arrange
		Map<String, Object> parts = new HashMap<>();

		parts.put(FILE_INPUT, new File(TEST_AUDIO_FILE_1));
		parts.put("audios", getVoiceCollageAudioCollection(false));

		when(audioManager.createAudios(argumentCaptor.capture()))
				.thenReturn(AudioDtoBuilder.with(Collections.emptyList()).audios());

		initMockRequestResponse(RequestType.POST, REST_SERVICE_ROOT);

		initMultipartRequest(REST_SERVICE_ROOT, parts, "audio/mpeg");

		// act
		dispatcher.invoke(httpRequest, httpResponse);

		FutureFragmentCollectionDto<AudioDto> audios = readAudioDtoFromJsonString(httpResponse.getContentAsString());

		// assert
		verifyValidResponse(audios);
		verify(audioManager, times(1)).createAudios(argumentCaptor.capture());
	}

	@Test
	public void shouldHandleExceptionWhenUploadingFileAndCreatingAudio() throws Exception {
		// arrange
		Map<String, Object> parts = new HashMap<>();

		parts.put(FILE_INPUT, new File(TEST_AUDIO_FILE_1));
		parts.put("audios", getVoiceCollageAudioCollection(false));

		when(audioManager.createAudios(argumentCaptor.capture()))
				.thenThrow(AudioMinerExceptionFactory.raiseException(VC_EXCEPTION));

		initMockRequestResponse(RequestType.POST, REST_SERVICE_ROOT);

		initMultipartRequest(REST_SERVICE_ROOT, parts, "audio/mpeg");

		// act
		dispatcher.invoke(httpRequest, httpResponse);

		ErrorDto error = readErrorDtoFromJsonString(httpResponse.getContentAsString());

		// assert
		verifyErrorResponse(error);
		verify(audioManager, times(1)).createAudios(argumentCaptor.capture());
	}

	@Test
	public void shoulDeleteAudioById() throws Exception {
		// arrange
		Long id = 1L;
		when(audioManager.deleteAudio(anyLong())).thenReturn(true);
		initMockRequestResponse(RequestType.DELETE, REST_SERVICE_ROOT + RsTestConstants.PATH_SEPARATOR + id);
		initRequestContent(MediaType.APPLICATION_JSON, MediaType.APPLICATION_JSON, null);

		// act
		dispatcher.invoke(httpRequest, httpResponse);

		DeleteResourceDto deleteResource = readDeleteResourceDtoFromJsonString(httpResponse.getContentAsString());

		// assert
		verifyDeleteResourceResponse(deleteResource);
		verify(audioManager, times(1)).deleteAudio(anyLong());
	}

	@Test
	public void shoulHandleExceptionWhenDeletingAudioById() throws Exception {
		// arrange
		Long id = 1L;
		when(audioManager.deleteAudio(anyLong())).thenThrow(AudioMinerExceptionFactory.raiseException(VC_EXCEPTION));
		initMockRequestResponse(RequestType.DELETE, REST_SERVICE_ROOT + RsTestConstants.PATH_SEPARATOR + id);
		initRequestContent(MediaType.APPLICATION_JSON, MediaType.APPLICATION_JSON, null);

		// act
		dispatcher.invoke(httpRequest, httpResponse);

		ErrorDto error = readErrorDtoFromJsonString(httpResponse.getContentAsString());

		// assert
		verifyErrorResponse(error);
		verify(audioManager, times(1)).deleteAudio(anyLong());
	}

	private FutureFragmentCollectionDto<AudioDto> getVoiceCollageAudioCollection(boolean withId) {
		List<AudioDto> audiosToUdate = AudioDtoBuilder.with(Arrays.asList(AudioProperties.CUT_AUDIO)).audios();
		if (!withId) {
			audiosToUdate.forEach(w -> w.setId(null));
		}
		FutureFragmentCollectionDto<AudioDto> audioCollectionToUpdate = new FutureFragmentCollectionDto<>();
		audioCollectionToUpdate.setData(audiosToUdate);

		return audioCollectionToUpdate;
	}

	private FutureFragmentCollectionDto<AudioDto> readAudioDtoFromJsonString(String jsonString)
			throws JsonProcessingException, IOException {
		return new ObjectMapper().readerFor(new TypeReference<FutureFragmentCollectionDto<AudioDto>>() {
		}).readValue(jsonString);
	}
}
