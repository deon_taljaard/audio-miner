package com.futurefragment.rs.audio.validation;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;

import com.futurefragment.dto.audio.AudioDto;
import com.futurefragment.dto.audio.builder.AudioDtoBuilder;
import com.futurefragment.dto.audio.builder.AudioDtoBuilder.AudioProperties;
import com.futurefragment.exception.AudioMinerException;
import com.futurefragment.rs.validation.ValidationUtil;

public class AudioValidationUtilTest {

	private AudioDto audioDtoWithId;
	private AudioDto audioDtoWithoutId;

	@Before
	public void setup() {
		audioDtoWithId = AudioDtoBuilder.singleAudio(1, Arrays.asList(AudioProperties.CUT_AUDIO));
		audioDtoWithoutId = AudioDtoBuilder.singleAudio(1, Arrays.asList(AudioProperties.CUT_AUDIO));
		audioDtoWithoutId.setId(null);
	}

	@Test
	public void shouldPassValidateAudioDtoForCreateWithValidAudioDto() {
		// act
		AudioValidationUtil.validateAudioDtosForCreate(Arrays.asList(audioDtoWithoutId));
	}

	@Test
	public void shouldPassValidateAudioDtoForUpdateWithValidAudioDto() {
		// act
		AudioValidationUtil.validateAudioDtosForCreate(Arrays.asList(audioDtoWithoutId));
	}

	@Test
	public void shouldFailValidateAudioDtoForCreateIfAudioDtoHasId() {
		try {
			// act
			AudioValidationUtil.validateAudioDtosForCreate(Arrays.asList(audioDtoWithId));
			fail("AudioMinerException expected");
		} catch (AudioMinerException ex) {
			// assert
			assertNotNull(ex);
			assertThat(ex.getAdministratorErrorDescription(),
					is(equalTo(ValidationUtil.ERR_INVALID_ID_CREATE_DESCRIPTION)));
			assertThat(ex.getAdministratorErrorCorrection(),
					is(equalTo(ValidationUtil.ERR_INVALID_ID_CREATE_CORRECTION)));
		}
	}

	@Test
	public void shouldFailValidateAudioDtoForUpdateIfAudioDtoDoesNotHaveId() {
		try {
			// act
			AudioValidationUtil.validateAudioDtosForUpdate(Arrays.asList(audioDtoWithoutId));
			fail("AudioMinerException expected");
		} catch (AudioMinerException ex) {
			// assert
			assertNotNull(ex);
			assertThat(ex.getAdministratorErrorDescription(), is(equalTo(ValidationUtil.ERR_INVALID_ID_DESCRIPTION)));
			assertThat(ex.getAdministratorErrorCorrection(), is(equalTo(ValidationUtil.ERR_INVALID_ID_CORRECTION)));
		}
	}

	@Test
	public void shouldFailValidateAudioDtoIfAudioDtoIsNull() {
		try {
			// act
			audioDtoWithoutId = null;
			AudioValidationUtil.validateAudioDtosForCreate(Arrays.asList(audioDtoWithoutId));
			fail("AudioMinerException expected");
		} catch (AudioMinerException ex) {
			// assert
			assertNotNull(ex);
			assertThat(ex.getAdministratorErrorDescription(), is(equalTo(ValidationUtil.ERR_INVALID_DTO_DESCRIPTION)));
			assertThat(ex.getAdministratorErrorCorrection(), is(equalTo(ValidationUtil.ERR_INVALID_DTO_CORRECTION)));
		}
	}

	@Test
	public void shouldFailValidateAudioDtoIfDescriptionIsNull() {
		try {
			// act
			audioDtoWithoutId.setDescription(null);
			AudioValidationUtil.validateAudioDtosForCreate(Arrays.asList(audioDtoWithoutId));
			fail("AudioMinerException expected");
		} catch (AudioMinerException ex) {
			// assert
			assertNotNull(ex);
			assertThat(ex.getAdministratorErrorDescription(), is(equalTo("Invalid description.")));
			assertThat(ex.getAdministratorErrorCorrection(), is(equalTo("Ensure the description is not empty.")));
		}
	}

	@Test
	public void shouldFailValidateAudioDtoIfResourcePathIsNull() {
		try {
			// act
			audioDtoWithId.setResourcePath(null);
			AudioValidationUtil.validateAudioDtosForUpdate(Arrays.asList(audioDtoWithId));
			fail("AudioMinerException expected");
		} catch (AudioMinerException ex) {
			// assert
			assertNotNull(ex);
			assertThat(ex.getAdministratorErrorDescription(), is(equalTo("Invalid resourcePath.")));
			assertThat(ex.getAdministratorErrorCorrection(), is(equalTo("Ensure the resourcePath is not empty.")));
		}
	}

}
