package com.futurefragment.rs.audio.validation;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;

import com.futurefragment.dto.audio.CutAudioDto;
import com.futurefragment.dto.audio.builder.CutAudioDtoBuilder;
import com.futurefragment.exception.AudioMinerException;
import com.futurefragment.rs.validation.ValidationUtil;

public class CutAudioValidationUtilTest {

	private CutAudioDto cutAudioDtoWithId;
	private CutAudioDto cutAudioDtoWithoutId;

	@Before
	public void setup() {
		cutAudioDtoWithId = CutAudioDtoBuilder.createCutAudio(1);
		cutAudioDtoWithoutId = CutAudioDtoBuilder.createCutAudio(1);
		cutAudioDtoWithoutId.setId(null);
	}

	@Test
	public void shouldPassValidateCutAudioDtoForCreateWithValidCutAudioDto() {
		// act
		CutAudioValidationUtil.validateCutAudioDtosForCreate(Arrays.asList(cutAudioDtoWithoutId));
	}

	@Test
	public void shouldPassValidateCutAudioDtoForUpdateWithValidCutAudioDto() {
		// act
		CutAudioValidationUtil.validateCutAudioDtosForCreate(Arrays.asList(cutAudioDtoWithoutId));
	}

	@Test
	public void shouldFailValidateCutAudioDtoForCreateIfCutAudioDtoHasId() {
		try {
			// act
			CutAudioValidationUtil.validateCutAudioDtosForCreate(Arrays.asList(cutAudioDtoWithId));
			fail("AudioMinerException expected");
		} catch (AudioMinerException ex) {
			// assert
			assertNotNull(ex);
			assertThat(ex.getAdministratorErrorDescription(),
					is(equalTo(ValidationUtil.ERR_INVALID_ID_CREATE_DESCRIPTION)));
			assertThat(ex.getAdministratorErrorCorrection(),
					is(equalTo(ValidationUtil.ERR_INVALID_ID_CREATE_CORRECTION)));
		}
	}

	@Test
	public void shouldFailValidateCutAudioDtoForUpdateIfCutAudioDtoDoesNotHaveId() {
		try {
			// act
			CutAudioValidationUtil.validateCutAudioDtosForUpdate(Arrays.asList(cutAudioDtoWithoutId));
			fail("AudioMinerException expected");
		} catch (AudioMinerException ex) {
			// assert
			assertNotNull(ex);
			assertThat(ex.getAdministratorErrorDescription(), is(equalTo(ValidationUtil.ERR_INVALID_ID_DESCRIPTION)));
			assertThat(ex.getAdministratorErrorCorrection(), is(equalTo(ValidationUtil.ERR_INVALID_ID_CORRECTION)));
		}
	}

	@Test
	public void shouldFailValidateCutAudioDtoIfCutAudioDtoIsNull() {
		try {
			// act
			cutAudioDtoWithoutId = null;
			CutAudioValidationUtil.validateCutAudioDtosForCreate(Arrays.asList(cutAudioDtoWithoutId));
			fail("AudioMinerException expected");
		} catch (AudioMinerException ex) {
			// assert
			assertNotNull(ex);
			assertThat(ex.getAdministratorErrorDescription(), is(equalTo(ValidationUtil.ERR_INVALID_DTO_DESCRIPTION)));
			assertThat(ex.getAdministratorErrorCorrection(), is(equalTo(ValidationUtil.ERR_INVALID_DTO_CORRECTION)));
		}
	}

	@Test
	public void shouldFailValidateCutAudioDtoIfDescriptionIsNull() {
		try {
			// act
			cutAudioDtoWithoutId.setDescription(null);
			CutAudioValidationUtil.validateCutAudioDtosForCreate(Arrays.asList(cutAudioDtoWithoutId));
			fail("AudioMinerException expected");
		} catch (AudioMinerException ex) {
			// assert
			assertNotNull(ex);
			assertThat(ex.getAdministratorErrorDescription(),
					is(equalTo(CutAudioValidationUtil.ERR_INVALID_DESCRIPTION_DESCRIPTION)));
			assertThat(ex.getAdministratorErrorCorrection(),
					is(equalTo(CutAudioValidationUtil.ERR_INVALID_DESCRIPTION_CORRECTION)));
		}
	}

}
