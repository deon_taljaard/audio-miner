package com.futurefragment.rs.validation;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;

import com.futurefragment.dto.common.ResourceStatusDto;
import com.futurefragment.dto.common.builder.ResourceStatusDtoBuilder;
import com.futurefragment.exception.AudioMinerException;

public class ResourceStatusValidationUtilTest {

	private ResourceStatusDto resourceStatusDtoWithId;

	@Before
	public void setup() {
		resourceStatusDtoWithId = ResourceStatusDtoBuilder.createResourceStatus(1);
	}

	@Test
	public void shouldPassValidateResourceStatusDtoWithValidResourceStatusDto() {
		// act
		ResourceStatusValidationUtil.validateResourceStatusDtos(Arrays.asList(resourceStatusDtoWithId));
	}

	@Test
	public void shouldFailValidateResourceStatusDtoForUpdateIfResourceStatusDtoDoesNotHaveId() {
		// arrange
		resourceStatusDtoWithId.setId(null);

		try {
			// act
			ResourceStatusValidationUtil.validateResourceStatusDtos(Arrays.asList(resourceStatusDtoWithId));
			fail("AudioMinerException expected");
		} catch (AudioMinerException ex) {
			// assert
			assertNotNull(ex);
			assertThat(ex.getAdministratorErrorDescription(), is(equalTo(ValidationUtil.ERR_INVALID_ID_DESCRIPTION)));
			assertThat(ex.getAdministratorErrorCorrection(), is(equalTo(ValidationUtil.ERR_INVALID_ID_CORRECTION)));
		}
	}

	@Test
	public void shouldFailValidateResourceStatusDtoIfResourceStatusDtoIsNull() {
		try {
			// act
			resourceStatusDtoWithId = null;
			ResourceStatusValidationUtil.validateResourceStatusDtos(Arrays.asList(resourceStatusDtoWithId));
			fail("AudioMinerException expected");
		} catch (AudioMinerException ex) {
			// assert
			assertNotNull(ex);
			assertThat(ex.getAdministratorErrorDescription(), is(equalTo(ValidationUtil.ERR_INVALID_DTO_DESCRIPTION)));
			assertThat(ex.getAdministratorErrorCorrection(), is(equalTo(ValidationUtil.ERR_INVALID_DTO_CORRECTION)));
		}
	}

	@Test
	public void shouldFailValidateResourceStatusDtoIfDescriptionIsNull() {
		try {
			// act
			resourceStatusDtoWithId.setResourceStatus(null);
			ResourceStatusValidationUtil.validateResourceStatusDtos(Arrays.asList(resourceStatusDtoWithId));
			fail("AudioMinerException expected");
		} catch (AudioMinerException ex) {
			// assert
			assertNotNull(ex);
			assertThat(ex.getAdministratorErrorDescription(),
					is(equalTo(ResourceStatusValidationUtil.ERR_NULL_RESOURCE_STATUS_DESCRIPTION)));
			assertThat(ex.getAdministratorErrorCorrection(), containsString("Ensure the resourceStatus is one of"));
		}
	}

	@Test
	public void shouldFailValidateResourceStatusDtoIfResourceStatusIsInvalid() {
		try {
			// act
			resourceStatusDtoWithId.setResourceStatus("nothing");
			ResourceStatusValidationUtil.validateResourceStatusDtos(Arrays.asList(resourceStatusDtoWithId));
			fail("AudioMinerException expected");
		} catch (AudioMinerException ex) {
			// assert
			assertNotNull(ex);

			assertThat(ex.getAdministratorErrorDescription(),
					is(equalTo(ResourceStatusValidationUtil.ERR_INVALID_RESOURCE_STATUS_DESCRIPTION)));
			assertThat(ex.getAdministratorErrorCorrection(), containsString("Ensure the resourceStatus is one of"));
		}
	}

}
