package com.futurefragment.rs.api.validation;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;

import com.futurefragment.dto.api.ApiKeyDto;
import com.futurefragment.dto.api.builder.ApiKeyDtoBuilder;
import com.futurefragment.exception.AudioMinerException;
import com.futurefragment.rs.validation.ValidationUtil;
import com.futurefragment.util.KeyGenerator;

public class ApiKeyValidationUtilTest {

	private ApiKeyDto apiKeyDtoWithId;
	private ApiKeyDto apiKeyDtoWithoutId;

	@Before
	public void setup() {
		apiKeyDtoWithId = ApiKeyDtoBuilder.createApiKey(1);
		apiKeyDtoWithoutId = ApiKeyDtoBuilder.createApiKey(1);
		apiKeyDtoWithoutId.setId(null);
		apiKeyDtoWithoutId.setKey(null);
	}

	@Test
	public void shouldPassValidateApiKeyDtoForCreateWithValidApiKeyDto() {
		// act
		ApiKeyValidationUtil.validateApiKeyDtosForCreate(Arrays.asList(apiKeyDtoWithoutId));
	}

	@Test
	public void shouldPassValidateApiKeyDtoForUpdateWithValidApiKeyDto() {
		// act
		ApiKeyValidationUtil.validateApiKeyDtosForUpdate(Arrays.asList(apiKeyDtoWithId));
	}

	@Test
	public void shouldFailValidateApiKeyDtoForCreateIfApiKeyDtoHasId() {
		try {
			// act
			ApiKeyValidationUtil.validateApiKeyDtosForCreate(Arrays.asList(apiKeyDtoWithId));
			fail("AudioMinerException expected");
		} catch (AudioMinerException ex) {
			// assert
			assertNotNull(ex);
			assertThat(ex.getAdministratorErrorDescription(),
					is(equalTo(ValidationUtil.ERR_INVALID_ID_CREATE_DESCRIPTION)));
			assertThat(ex.getAdministratorErrorCorrection(),
					is(equalTo(ValidationUtil.ERR_INVALID_ID_CREATE_CORRECTION)));
		}
	}

	@Test
	public void shouldFailValidateApiKeyDtoForCreateIfApiKeyDtoHasPopulatedKeyField() {
		// arrange
		apiKeyDtoWithoutId.setKey(KeyGenerator.newKey());

		try {
			// act
			ApiKeyValidationUtil.validateApiKeyDtosForCreate(Arrays.asList(apiKeyDtoWithoutId));
			fail("AudioMinerException expected");
		} catch (AudioMinerException ex) {
			// assert
			assertNotNull(ex);
			assertThat(ex.getAdministratorErrorDescription(),
					is(equalTo(ApiKeyValidationUtil.ERR_INVALID_KEY_DESCRIPTION)));
			assertThat(ex.getAdministratorErrorCorrection(),
					is(equalTo(ApiKeyValidationUtil.ERR_INVALID_KEY_CORRECTION)));
		}
	}

	@Test
	public void shouldFailValidateApiKeyDtoForUpdateIfApiKeyDtoDoesNotHaveId() {
		try {
			// act
			ApiKeyValidationUtil.validateApiKeyDtosForUpdate(Arrays.asList(apiKeyDtoWithoutId));
			fail("AudioMinerException expected");
		} catch (AudioMinerException ex) {
			// assert
			assertNotNull(ex);
			assertThat(ex.getAdministratorErrorDescription(), is(equalTo(ValidationUtil.ERR_INVALID_ID_DESCRIPTION)));
			assertThat(ex.getAdministratorErrorCorrection(), is(equalTo(ValidationUtil.ERR_INVALID_ID_CORRECTION)));
		}
	}

	@Test
	public void shouldFailValidateApiKeyDtoIfApiKeyDtoIsNull() {
		try {
			// act
			apiKeyDtoWithoutId = null;
			ApiKeyValidationUtil.validateApiKeyDtosForCreate(Arrays.asList(apiKeyDtoWithoutId));
			fail("AudioMinerException expected");
		} catch (AudioMinerException ex) {
			// assert
			assertNotNull(ex);
			assertThat(ex.getAdministratorErrorDescription(), is(equalTo(ValidationUtil.ERR_INVALID_DTO_DESCRIPTION)));
			assertThat(ex.getAdministratorErrorCorrection(), is(equalTo(ValidationUtil.ERR_INVALID_DTO_CORRECTION)));
		}
	}

	@Test
	public void shouldFailValidateApiKeyDtoIfDescriptionIsNull() {
		try {
			// act
			apiKeyDtoWithId.setDescription(null);
			ApiKeyValidationUtil.validateApiKeyDtosForUpdate(Arrays.asList(apiKeyDtoWithId));
			fail("AudioMinerException expected");
		} catch (AudioMinerException ex) {
			// assert
			assertNotNull(ex);
			assertThat(ex.getAdministratorErrorDescription(),
					is(equalTo(ApiKeyValidationUtil.ERR_INVALID_DESCRIPTION_DESCRIPTION)));
			assertThat(ex.getAdministratorErrorCorrection(),
					is(equalTo(ApiKeyValidationUtil.ERR_INVALID_DESCRIPTION_CORRECTION)));
		}
	}

}
