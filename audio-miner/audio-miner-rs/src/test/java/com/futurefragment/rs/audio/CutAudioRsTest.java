package com.futurefragment.rs.audio;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.List;

import javax.ws.rs.core.MediaType;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.futurefragment.audio.CutAudioManager;
import com.futurefragment.constant.ExceptionConstants;
import com.futurefragment.dto.audio.CutAudioDto;
import com.futurefragment.dto.audio.builder.CutAudioDtoBuilder;
import com.futurefragment.dto.common.ErrorDto;
import com.futurefragment.dto.common.FutureFragmentCollectionDto;
import com.futurefragment.exception.AudioMinerExceptionFactory;
import com.futurefragment.exception.ExceptionContext;
import com.futurefragment.rs.util.AbstractRestTest;
import com.futurefragment.rs.util.RsTestConstants;

@RunWith(MockitoJUnitRunner.class)
public class CutAudioRsTest extends AbstractRestTest {

	private static final String CLASS_NAME = CutAudioRsTest.class.getSimpleName();
	private static final ExceptionContext VC_EXCEPTION = ExceptionContext
			.getInstance(ExceptionConstants.APPLICATION_LAYER_RESTFUL, CLASS_NAME, "test",
					new RuntimeException("error!"))
			.setAdminErrorDescription("description").setAdminErrorCorrection("correction");

	@Mock
	private CutAudioManager cutAudioManager;

	@InjectMocks
	private CutAudioRs cutAudioRs;

	@Captor
	private ArgumentCaptor<List<CutAudioDto>> argumentCaptor;

	private static final String REST_SERVICE_ROOT = "/cutAudios";

	@Before
	public void setup() {
		dispatcher.getRegistry().addSingletonResource(cutAudioRs);
		dispatcher.getProviderFactory().createResponseBuilder();
	}

	@Test
	public void shouldGetCutAudioById() throws Exception {
		// arrange
		Long id = 1L;
		when(cutAudioManager.getCutAudioById(anyLong())).thenReturn(CutAudioDtoBuilder.createCutAudio(1));
		initMockRequestResponse(RequestType.GET, REST_SERVICE_ROOT + RsTestConstants.PATH_SEPARATOR + id);
		initRequestContent(MediaType.APPLICATION_JSON, MediaType.APPLICATION_JSON, null);

		// act
		dispatcher.invoke(httpRequest, httpResponse);

		FutureFragmentCollectionDto<CutAudioDto> cutAudioes = readCutAudioDtoFromJsonString(
				httpResponse.getContentAsString());

		// assert
		verifyValidResponse(cutAudioes);
		assertThat(cutAudioes.getData().size(), is(equalTo(1)));
		verify(cutAudioManager, times(1)).getCutAudioById(anyLong());
	}

	@Test
	public void shouldHandleExceptionWhenCallingGetCutAudioById() throws Exception {
		// arrange
		Long id = 1L;
		when(cutAudioManager.getCutAudioById(anyLong()))
				.thenThrow(AudioMinerExceptionFactory.raiseException(VC_EXCEPTION));
		initMockRequestResponse(RequestType.GET, REST_SERVICE_ROOT + RsTestConstants.PATH_SEPARATOR + id);
		initRequestContent(MediaType.APPLICATION_JSON, MediaType.APPLICATION_JSON, null);

		// act
		dispatcher.invoke(httpRequest, httpResponse);

		ErrorDto error = readErrorDtoFromJsonString(httpResponse.getContentAsString());

		// assert
		verifyErrorResponse(error);
		verify(cutAudioManager, times(1)).getCutAudioById(anyLong());
	}

	@Test
	public void shouldUpdateCutAudio() throws Exception {
		// arrange
		when(cutAudioManager.updateCutAudios(argumentCaptor.capture())).thenReturn(CutAudioDtoBuilder.getCutAudios());
		initMockRequestResponse(RequestType.PUT, REST_SERVICE_ROOT);
		initRequestContent(MediaType.APPLICATION_JSON, MediaType.APPLICATION_JSON,
				writeDtoToJsonString(getVoiceCollageCutAudioCollection(true)));

		// act
		dispatcher.invoke(httpRequest, httpResponse);

		FutureFragmentCollectionDto<CutAudioDto> words = readCutAudioDtoFromJsonString(
				httpResponse.getContentAsString());

		// assert
		verifyValidResponse(words);
		verify(cutAudioManager, times(1)).updateCutAudios(argumentCaptor.capture());
	}

	@Test
	public void shouldHandleExceptionWhenUpdatingCutAudio() throws Exception {
		// arrange
		when(cutAudioManager.updateCutAudios(argumentCaptor.capture()))
				.thenThrow(AudioMinerExceptionFactory.raiseException(VC_EXCEPTION));
		initMockRequestResponse(RequestType.PUT, REST_SERVICE_ROOT);
		initRequestContent(MediaType.APPLICATION_JSON, MediaType.APPLICATION_JSON,
				writeDtoToJsonString(getVoiceCollageCutAudioCollection(true)));

		// act
		dispatcher.invoke(httpRequest, httpResponse);

		ErrorDto error = readErrorDtoFromJsonString(httpResponse.getContentAsString());

		// assert
		verifyErrorResponse(error);
		verify(cutAudioManager, times(1)).updateCutAudios(argumentCaptor.capture());
	}

	@Test
	public void shouldCreateCutAudio() throws Exception {
		// arrange
		when(cutAudioManager.createCutAudios(argumentCaptor.capture())).thenReturn(CutAudioDtoBuilder.getCutAudios());
		initMockRequestResponse(RequestType.POST, REST_SERVICE_ROOT);
		initRequestContent(MediaType.APPLICATION_JSON, MediaType.APPLICATION_JSON,
				writeDtoToJsonString(getVoiceCollageCutAudioCollection(false)));

		// act
		dispatcher.invoke(httpRequest, httpResponse);

		FutureFragmentCollectionDto<CutAudioDto> words = readCutAudioDtoFromJsonString(
				httpResponse.getContentAsString());

		// assert
		verifyValidResponse(words);
		verify(cutAudioManager, times(1)).createCutAudios(argumentCaptor.capture());
	}

	@Test
	public void shouldHandleExceptionWhenCreatingCutAudio() throws Exception {
		// arrange
		when(cutAudioManager.createCutAudios(argumentCaptor.capture()))
				.thenThrow(AudioMinerExceptionFactory.raiseException(VC_EXCEPTION));
		initMockRequestResponse(RequestType.POST, REST_SERVICE_ROOT);
		initRequestContent(MediaType.APPLICATION_JSON, MediaType.APPLICATION_JSON,
				writeDtoToJsonString(getVoiceCollageCutAudioCollection(false)));

		// act
		dispatcher.invoke(httpRequest, httpResponse);

		ErrorDto error = readErrorDtoFromJsonString(httpResponse.getContentAsString());

		// assert
		verifyErrorResponse(error);
		verify(cutAudioManager, times(1)).createCutAudios(argumentCaptor.capture());
	}

	private FutureFragmentCollectionDto<CutAudioDto> getVoiceCollageCutAudioCollection(boolean withId) {
		List<CutAudioDto> cutAudioesToUdate = CutAudioDtoBuilder.getCutAudios();
		if (!withId) {
			cutAudioesToUdate.forEach(w -> w.setId(null));
		}
		FutureFragmentCollectionDto<CutAudioDto> cutAudioCollectionToUpdate = new FutureFragmentCollectionDto<>();
		cutAudioCollectionToUpdate.setData(cutAudioesToUdate);

		return cutAudioCollectionToUpdate;
	}

	private FutureFragmentCollectionDto<CutAudioDto> readCutAudioDtoFromJsonString(String jsonString)
			throws JsonProcessingException, IOException {
		return new ObjectMapper().readerFor(new TypeReference<FutureFragmentCollectionDto<CutAudioDto>>() {
		}).readValue(jsonString);
	}
}
