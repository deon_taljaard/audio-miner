package com.futurefragment.constant;

import javax.ws.rs.core.HttpHeaders;

public class RsConstants {

	// Request attributes

	public static final String REQUEST_ATTR_USER_ID = "user_id";

	// Response headers

	public static final String RESPONSE_HEADER_AUTH_TOKEN = "auth_token";
	public static final String RESPONSE_HEADER_REFRESH_TOKEN = "refresh_token";

	// JWT

	public static final String JWT_CLAIM_SUB = "sub";
	public static final String JWT_CLAIM_SCOPE = "scope";

	public static final String JWT_CLAIM_ISS = "voice-collage";

	public static final String JWT_AUTH_BEARER_PATTERN = "^Bearer$";
	public static final String JWT_AUTH_HEADER_SPLIT_TOKEN = " ";
	public static final int JWT_AUTH_HEADER_SCHEME_INDEX = 0;
	public static final int JWT_AUTH_HEADER_CREDENTIALS_INDEX = 1;
	public static final String BYTE_T0_STR_ENCODING = "utf-8";

	// Login authentication

	public static final String LOGIN_AUTH_BASIC_PATTERN = "^Basic$";
	public static final String LOGIN_AUTH_HEADER_SPLIT_TOKEN = ":";
	public static final int LOGIN_AUTH_HEADER_SCHEME_INDEX = 0;
	public static final int LOGIN_AUTH_HEADER_CREDENTIALS_INDEX = 1;
	public static final int LOGIN_AUTH_HEADER_USERNAME_INDEX = 0;
	public static final int LOGIN_AUTH_HEADER_PASSWORD_INDEX = 1;
	public static final int LOGIN_AUTH_USER_PARTS_SIZE = 2;

	public static final String AUTH_LOGIN = "auth_login";
	public static final String REFRESH_TOKEN = "refresh_token";
	public static final String GOOGLE_TOKEN = "google_token";

	// Common exception messages
	public static final String ERR_AUTH_TOKEN_EXPIRED_USER_DESCRIPTION = "Your authentication token has expired";
	public static final String ERR_AUTH_TOKEN_EXPIRED_USER_CORRECTION = "Please log out and log back in";

	public static final String ERR_GOOGLE_ID_TOKEN_EMPTY_DESCRIPTION = "The Google auth token is empty.";
	public static final String ERR_GOOGLE_ID_TOKEN_INVALID_DESCRIPTION = "The Google auth token is invalid.";
	public static final String ERR_GOOGLE_ID_TOKEN_INVALID_CORRECTION = String.format(
			"Ensure the client is populating the request header with a value for '%s'.", HttpHeaders.AUTHORIZATION);

}
