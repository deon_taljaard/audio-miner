package com.futurefragment.rs.api;

import java.util.Arrays;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;

import com.futurefragment.api.ApiKeyResourceStatusManager;
import com.futurefragment.dto.common.FutureFragmentCollectionDto;
import com.futurefragment.dto.common.ResourceStatusDto;
import com.futurefragment.rs.validation.ResourceStatusValidationUtil;
import com.futurefragment.rs.validation.ValidationUtil;
import com.futurefragment.util.jwt.APIKeySecured;
import com.futurefragment.util.response.RsResponseFactory;

@APIKeySecured
@Stateless
@Path("/apiKeyResourceStatus")
public class ApiKeyResourceStatusRs {

	private static final String RESOURCE_PERSONAL_SLOT_RESOURCE_STATUS = "apiKeyResourceStatus";

	@Context
	private HttpServletRequest request;

	@Inject
	private ApiKeyResourceStatusManager apiKeyResourceStatusManager;

	@Inject
	private Logger logger;

	// -------------------------------------------------------
	// @GET apiKeyResourceStatus
	// <host>:<port>/voice-collage/apiKeyResourceStatus/{apiKeyId}
	// -------------------------------------------------------

	@Path("{apiKeyId}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getApiKeyResourceStatusByApiKeyId(@PathParam("apiKeyId") Long id) {
		try {
			ValidationUtil.validateId(ResourceStatusValidationUtil.DTO_TYPE, id);

			ResourceStatusDto apiKeyResourceStatus = apiKeyResourceStatusManager.getApiKeyResourceStatusByApiKeyId(id);

			return RsResponseFactory.getOkResponseForDtos(ResourceStatusDto.class,
					RESOURCE_PERSONAL_SLOT_RESOURCE_STATUS, Arrays.asList(apiKeyResourceStatus), logger);
		} catch (Exception ex) {
			return RsResponseFactory.getErrorResponse(ResourceStatusDto.class, RESOURCE_PERSONAL_SLOT_RESOURCE_STATUS,
					ex, logger);
		}
	}

	// -------------------------------------------------------
	// @PUT apiKeyResourceStatus
	// <host>:<port>/voice-collage/apiKeyResourceStatus
	// -------------------------------------------------------

	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateApiKeyalSlots(FutureFragmentCollectionDto<ResourceStatusDto> apiKeyResourceStatus) {
		try {
			ResourceStatusValidationUtil.validateResourceStatusDtos(apiKeyResourceStatus.getData());

			List<ResourceStatusDto> updatedApiKeyResourceStatus = apiKeyResourceStatusManager
					.updateApiKeyResourceStatus(apiKeyResourceStatus.getData());

			return RsResponseFactory.getOkResponseForDtos(ResourceStatusDto.class,
					RESOURCE_PERSONAL_SLOT_RESOURCE_STATUS, updatedApiKeyResourceStatus, logger);
		} catch (Exception ex) {
			return RsResponseFactory.getErrorResponse(ResourceStatusDto.class, RESOURCE_PERSONAL_SLOT_RESOURCE_STATUS,
					ex, logger);
		}
	}
}
