package com.futurefragment.rs.audio;

import java.util.Arrays;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;

import com.futurefragment.audio.CutAudioManager;
import com.futurefragment.dto.audio.CutAudioDto;
import com.futurefragment.dto.common.FutureFragmentCollectionDto;
import com.futurefragment.rs.audio.validation.CutAudioValidationUtil;
import com.futurefragment.rs.validation.ValidationUtil;
import com.futurefragment.util.jwt.APIKeySecured;
import com.futurefragment.util.response.RsResponseFactory;

@APIKeySecured
@Stateless
@Path("/cutAudios")
public class CutAudioRs {

	private static final String RESOURCE_CUT_AUDIO = "cutAudioes";

	@Inject
	private Logger logger;

	@Inject
	private CutAudioManager cutAudioManager;

	// -------------------------------------------------------------
	// @GET cutAudioes <host>:<port>/voice-collage/cutAudios/{id}
	// -------------------------------------------------------------

	@Path("/{id}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getCutAudioById(@PathParam("id") Long id) {
		try {
			ValidationUtil.validateId(CutAudioValidationUtil.DTO_TYPE, id);

			CutAudioDto cutAudio = cutAudioManager.getCutAudioById(id);

			return RsResponseFactory.getOkResponseForDtos(CutAudioDto.class, RESOURCE_CUT_AUDIO,
					Arrays.asList(cutAudio), logger);
		} catch (Exception ex) {
			return RsResponseFactory.getErrorResponse(CutAudioDto.class, RESOURCE_CUT_AUDIO, ex, logger);
		}
	}

	// ------------------------------------------------------------
	// @PUT cutAudioes <host>:<port>/voice-collage/cutAudios
	// ------------------------------------------------------------

	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateCutAudios(FutureFragmentCollectionDto<CutAudioDto> cutAudios) {
		try {
			CutAudioValidationUtil.validateCutAudioDtosForUpdate(cutAudios.getData());

			List<CutAudioDto> updatedCutAudioes = cutAudioManager.updateCutAudios(cutAudios.getData());

			return RsResponseFactory.getOkResponseForDtos(CutAudioDto.class, RESOURCE_CUT_AUDIO, updatedCutAudioes,
					logger);
		} catch (Exception ex) {
			return RsResponseFactory.getErrorResponse(CutAudioDto.class, RESOURCE_CUT_AUDIO, ex, logger);
		}
	}

	// ------------------------------------------------------------
	// @POST cutAudioes <host>:<port>/voice-collage/cutAudios
	// ------------------------------------------------------------

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response createCutAudios(FutureFragmentCollectionDto<CutAudioDto> cutAudios) {
		try {
			CutAudioValidationUtil.validateCutAudioDtosForCreate(cutAudios.getData());

			List<CutAudioDto> createdCutAudioes = cutAudioManager.createCutAudios(cutAudios.getData());

			return RsResponseFactory.getOkResponseForDtos(CutAudioDto.class, RESOURCE_CUT_AUDIO, createdCutAudioes,
					logger);
		} catch (Exception ex) {
			return RsResponseFactory.getErrorResponse(CutAudioDto.class, RESOURCE_CUT_AUDIO, ex, logger);
		}
	}

	// -------------------------------------------------------
	// @DELETE audios <host>:<port>/voice-collage/audios/{id}
	// -------------------------------------------------------

	@Path("/{id}")
	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteCutAudioById(@PathParam("id") Long id) {
		try {
			ValidationUtil.validateId(CutAudioValidationUtil.DTO_TYPE, id);

			boolean isDeleteSuccessful = cutAudioManager.deleteCutAudio(id);

			return RsResponseFactory.getOkResponseForDeletedResource(CutAudioDto.class, RESOURCE_CUT_AUDIO,
					isDeleteSuccessful, id, logger);
		} catch (Exception ex) {
			return RsResponseFactory.getErrorResponse(CutAudioDto.class, RESOURCE_CUT_AUDIO, ex, logger);
		}
	}
}
