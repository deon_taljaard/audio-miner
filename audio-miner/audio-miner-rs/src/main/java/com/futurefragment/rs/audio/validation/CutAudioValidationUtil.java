package com.futurefragment.rs.audio.validation;

import static com.futurefragment.rs.validation.ValidationUtil.validateDataList;
import static com.futurefragment.rs.validation.ValidationUtil.validateId;
import static com.futurefragment.rs.validation.ValidationUtil.validateIdForCreate;
import static com.futurefragment.rs.validation.ValidationUtil.validateIfNullDto;
import static com.futurefragment.rs.validation.ValidationUtil.validateStringField;

import java.util.List;

import com.futurefragment.dto.audio.CutAudioDto;

public final class CutAudioValidationUtil {

	private static final String METHOD_VALIDATE_CUT_AUDIO_CLASS = "validateCutAudioClassDto";
	private static final String FIELD_CUT_AUDIO_CLASS_DESCRIPTION = "description";

	public static final Class<CutAudioDto> DTO_TYPE = CutAudioDto.class;
	public static final String ERR_INVALID_DESCRIPTION_DESCRIPTION = "Invalid description.";
	public static final String ERR_INVALID_DESCRIPTION_CORRECTION = "Ensure the description is not empty.";

	private CutAudioValidationUtil() {
	}

	public static void validateCutAudioDtosForCreate(List<CutAudioDto> cutAudios) {
		validateDataList(DTO_TYPE, cutAudios);

		cutAudios.forEach(c -> {
			validateCutAudioDto(c);
			validateIdForCreate(DTO_TYPE, c.getId());
		});
	}

	public static void validateCutAudioDtosForUpdate(List<CutAudioDto> cutAudios) {
		validateDataList(DTO_TYPE, cutAudios);

		cutAudios.forEach(c -> {
			validateCutAudioDto(c);
			validateId(DTO_TYPE, c.getId());
		});
	}

	private static void validateCutAudioDto(CutAudioDto cutAudio) {
		validateIfNullDto(DTO_TYPE, cutAudio);

		String description = cutAudio.getDescription();
		validateStringField(DTO_TYPE, description, FIELD_CUT_AUDIO_CLASS_DESCRIPTION, METHOD_VALIDATE_CUT_AUDIO_CLASS);
	}

}
