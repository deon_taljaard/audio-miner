package com.futurefragment.rs.validation;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.futurefragment.constant.ExceptionConstants;
import com.futurefragment.dto.common.AudioMinerDto;
import com.futurefragment.exception.AudioMinerException;
import com.futurefragment.exception.AudioMinerExceptionFactory;
import com.futurefragment.exception.ExceptionContext;
import com.futurefragment.exception.Severity;

public final class ValidationUtil {

	private static final String CLASS_NAME = ValidationUtil.class.getName();
	private static final String METHOD_VALIDATE_ID = "validateId";
	private static final String METHOD_VALIDATE_ID_CREATE = "validateIdForCreate";
	private static final String METHOD_VALIDATE_DATA_LIST = "validateDataList";
	private static final String METHOD_VALIDATE_NULL_IF_DTO = "validateIfNullDto";
	private static final String METHOD_VALIDATE_NULL_OR_EMPTY_FIELD = "validateNotNullOrEmptyString";
	private static final String ERR_INVALID_FIELD = "Invalid %s.";
	public static final String ERR_INVALID_FIELD_CORRECTION = "Ensure the %s is not empty.";
	private static final String FIELD_DATA = "data";
	private static final String FIELD_ID = "id";

	public static final String ERR_INVALID_ID_DESCRIPTION = "Invalid id.";
	public static final String ERR_INVALID_ID_CORRECTION = "Must be numeric and greater than 0.";
	public static final String ERR_INVALID_ID_CREATE_DESCRIPTION = "No id should be present when creating an entity.";
	public static final String ERR_INVALID_ID_CREATE_CORRECTION = "Ensure the id field is not set.";
	public static final String ERR_INVALID_DATA_LIST_DESCRIPTION = "The data array is empty.";
	public static final String ERR_INVALID_DATA_LIST_CORRECTION = "Ensure the array is populated with at least one instance of an entity.";
	public static final String ERR_INVALID_DTO_DESCRIPTION = "Invalid dto.";
	public static final String ERR_INVALID_DTO_CORRECTION = "Ensure the dto is not empty.";

	public static final String ERR_NULL_OR_EMPTY_FIELD_DESCRIPTION = "The %s field is null or empty.";
	public static final String ERR_NULL_OR_EMPTY_FIELD_CORRECTION = "Ensure the %s field is not null or empty.";

	private ValidationUtil() {
	}

	public static <T extends AudioMinerDto> void validateId(final Class<T> dtoType, final String id) {
		if (StringUtils.isNotEmpty(id)) {
			try {
				if (Long.parseLong(id) <= 0) {
					throw getAudioMinerExceptionForId(dtoType, null, id);
				}
			} catch (NumberFormatException ex) {
				throw getAudioMinerExceptionForId(dtoType, ex, id);
			}
		} else {
			throw getAudioMinerExceptionForId(dtoType, null, id);
		}
	}

	public static <T extends AudioMinerDto> void validateId(final Class<T> dtoType, final Long id) {
		if (isInvalidId(id)) {
			throw getAudioMinerExceptionForId(dtoType, null, String.valueOf(id));
		}
	}

	public static boolean isInvalidId(Long id) {
		return id == null || id <= 0;
	}

	public static <T extends AudioMinerDto> void validateIdForCreate(final Class<T> dtoType, Long id) {
		if (id != null) {
			throw getAudioMinerExceptionForInvalidField(dtoType, METHOD_VALIDATE_ID_CREATE, null, FIELD_ID,
					String.valueOf(id), ERR_INVALID_ID_CREATE_DESCRIPTION, ERR_INVALID_ID_CREATE_CORRECTION,
					ERR_INVALID_ID_CREATE_DESCRIPTION, ExceptionConstants.ERR_CONTACT_ADMIN_USER_CORRECTION);
		}
	}

	public static <T extends AudioMinerDto> void validateStringField(final Class<T> dtoType, final String field,
			final String fieldName, final String methodName) {
		if (StringUtils.isEmpty(field)) {
			String errorDescription = String.format(ERR_INVALID_FIELD, fieldName);
			String errorCorrection = String.format(ERR_INVALID_FIELD_CORRECTION, fieldName);
			throw getAudioMinerExceptionForInvalidField(dtoType, methodName, null, fieldName, field, errorDescription,
					errorCorrection, errorDescription, ExceptionConstants.ERR_CONTACT_ADMIN_USER_CORRECTION);
		}
	}

	public static void validateNotNullOrEmptyField(String field, String fieldName) {
		if (StringUtils.isEmpty(field)) {
			// @formatter:off
			final String correctionFormat = String.format(ERR_NULL_OR_EMPTY_FIELD_DESCRIPTION, fieldName);
			ExceptionContext exceptionContext = ExceptionContext
					.getInstance(ExceptionConstants.APPLICATION_LAYER_RESTFUL, CLASS_NAME,
							METHOD_VALIDATE_NULL_OR_EMPTY_FIELD, null)
					.setSeverity(Severity.FATAL).setAdminErrorDescription(correctionFormat)
					.setAdminErrorCorrection(String.format(ERR_NULL_OR_EMPTY_FIELD_CORRECTION, fieldName))
					.setUserErrorDescription(correctionFormat)
					.setUserErrorCorrection(ExceptionConstants.ERR_CONTACT_ADMIN_USER_CORRECTION);
			// @formatter:on

			throw AudioMinerExceptionFactory.raiseException(exceptionContext);
		}
	}

	private static <T extends AudioMinerDto> AudioMinerException getAudioMinerExceptionForId(final Class<T> dtoType,
			final Exception ex, final String id) {
		throw getAudioMinerExceptionForInvalidField(dtoType, METHOD_VALIDATE_ID, ex, FIELD_ID, id,
				ERR_INVALID_ID_DESCRIPTION, ERR_INVALID_ID_CORRECTION, ERR_INVALID_ID_DESCRIPTION,
				ExceptionConstants.ERR_CONTACT_ADMIN_USER_CORRECTION);
	}

	public static <T extends AudioMinerDto> void validateIfNullDto(final Class<T> dtoType, final T dto) {
		if (dto == null) {
			throw getAudioMinerExceptionForInvalidField(dtoType, METHOD_VALIDATE_NULL_IF_DTO, null,
					dtoType.getClass().getSimpleName(), null, ERR_INVALID_DTO_DESCRIPTION, ERR_INVALID_DTO_CORRECTION,
					ERR_INVALID_DTO_DESCRIPTION, ExceptionConstants.ERR_CONTACT_ADMIN_USER_CORRECTION);
		}
	}

	public static <T extends AudioMinerDto> void validateDataList(final Class<T> dtoType, final List<?> data) {
		if (data.isEmpty()) {
			throw getAudioMinerExceptionForInvalidField(dtoType, METHOD_VALIDATE_DATA_LIST, null, FIELD_DATA, "empty",
					ERR_INVALID_DATA_LIST_DESCRIPTION, ERR_INVALID_DATA_LIST_CORRECTION,
					ERR_INVALID_DATA_LIST_DESCRIPTION, ExceptionConstants.ERR_CONTACT_ADMIN_USER_CORRECTION);
		}
	}

	public static <T extends AudioMinerDto> AudioMinerException getAudioMinerExceptionForInvalidField(
			final Class<T> dtoType, final String methodName, final Exception ex, final String fieldName,
			final String fieldValue, final String adminErrorDescription, final String adminErrorCorrection,
			final String userErrorDescription, final String userErrorCorrection) {
		// @formatter:off
		ExceptionContext exceptionContext = ExceptionContext
				.getInstance(ExceptionConstants.APPLICATION_LAYER_RESTFUL, CLASS_NAME, methodName, ex)
				.setSeverity(Severity.FATAL).addParameter("dtoType", dtoType.getSimpleName())
				.addParameter(fieldName, fieldValue).setAdminErrorDescription(adminErrorDescription)
				.setAdminErrorCorrection(adminErrorCorrection).setUserErrorDescription(userErrorDescription)
				.setUserErrorCorrection(userErrorCorrection);
		// @formatter:on

		return AudioMinerExceptionFactory.raiseException(exceptionContext);
	}
}
