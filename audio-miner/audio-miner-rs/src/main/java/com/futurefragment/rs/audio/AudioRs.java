package com.futurefragment.rs.audio;

import java.util.Arrays;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;
import org.slf4j.Logger;

import com.futurefragment.audio.AudioManager;
import com.futurefragment.dto.audio.AudioDto;
import com.futurefragment.dto.common.FileType;
import com.futurefragment.dto.common.FutureFragmentCollectionDto;
import com.futurefragment.rs.audio.validation.AudioValidationUtil;
import com.futurefragment.rs.validation.ValidationUtil;
import com.futurefragment.util.file.RsFileUploadHandler;
import com.futurefragment.util.json.AudioDtoDeserialiserFunction;
import com.futurefragment.util.jwt.APIKeySecured;
import com.futurefragment.util.response.RsResponseFactory;

@APIKeySecured
@Stateless
@Path("/audios")
public class AudioRs {

	private static final String RESOURCE_AUDIO = "audios";

	@Context
	private HttpServletRequest request;

	@Inject
	private Logger logger;

	@Inject
	private AudioManager audioManager;

	// -------------------------------------------------------
	// @GET audios <host>:<port>/voice-collage/audios/{id}
	// -------------------------------------------------------

	@Path("/{id}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAudioById(@PathParam("id") Long id) {
		try {
			ValidationUtil.validateId(AudioValidationUtil.DTO_TYPE, id);

			AudioDto audio = audioManager.getAudioById(id);

			return RsResponseFactory.getOkResponseForDtos(AudioDto.class, RESOURCE_AUDIO, Arrays.asList(audio), logger);
		} catch (Exception ex) {
			return RsResponseFactory.getErrorResponse(AudioDto.class, RESOURCE_AUDIO, ex, logger);
		}
	}

	// -------------------------------------------------------
	// @PUT audios <host>:<port>/voice-collage/audios
	// -------------------------------------------------------

	@PUT
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateAudio(MultipartFormDataInput input) {
		try {
			// parse form data
			FutureFragmentCollectionDto<AudioDto> audios = RsFileUploadHandler.getInstance()
					.parseFormDataForVoiceCollageUploadDto(input.getFormDataMap(), FileType.AUDIO,
							AudioDtoDeserialiserFunction.getInstance());

			// validate
			AudioValidationUtil.validateAudioDtosForUpdate(audios.getData());

			// update
			List<AudioDto> updatedAudios = audioManager.updateAudios(audios.getData());

			return RsResponseFactory.getOkResponseForDtos(AudioDto.class, RESOURCE_AUDIO, updatedAudios,
					logger);
		} catch (Exception ex) {
			return RsResponseFactory.getErrorResponse(AudioDto.class, RESOURCE_AUDIO, ex, logger);
		}
	}

	// -------------------------------------------------------
	// @POST audios <host>:<port>/voice-collage/audios
	// -------------------------------------------------------

	@POST
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.APPLICATION_JSON)
	public Response createAudio(MultipartFormDataInput input) {
		try {
			// parse form data
			FutureFragmentCollectionDto<AudioDto> audios = RsFileUploadHandler.getInstance()
					.parseFormDataForVoiceCollageUploadDto(input.getFormDataMap(), FileType.AUDIO,
							AudioDtoDeserialiserFunction.getInstance());

			// validate
			AudioValidationUtil.validateAudioDtosForCreate(audios.getData());

			// update
			List<AudioDto> createdAudios = audioManager.createAudios(audios.getData());

			return RsResponseFactory.getOkResponseForDtos(AudioDto.class, RESOURCE_AUDIO, createdAudios,
					logger);
		} catch (Exception ex) {
			return RsResponseFactory.getErrorResponse(AudioDto.class, RESOURCE_AUDIO, ex, logger);
		}
	}

	// -------------------------------------------------------
	// @DELETE audios <host>:<port>/voice-collage/audios/{id}
	// -------------------------------------------------------

	@Path("/{id}")
	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteAudioById(@PathParam("id") Long id) {
		try {
			ValidationUtil.validateId(AudioValidationUtil.DTO_TYPE, id);

			boolean isDeleteSuccessful = audioManager.deleteAudio(id);

			return RsResponseFactory.getOkResponseForDeletedResource(AudioDto.class, RESOURCE_AUDIO, isDeleteSuccessful,
					id, logger);
		} catch (Exception ex) {
			return RsResponseFactory.getErrorResponse(AudioDto.class, RESOURCE_AUDIO, ex, logger);
		}
	}

}
