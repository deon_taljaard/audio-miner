package com.futurefragment.rs.api.validation;

import static com.futurefragment.rs.validation.ValidationUtil.getAudioMinerExceptionForInvalidField;
import static com.futurefragment.rs.validation.ValidationUtil.validateDataList;
import static com.futurefragment.rs.validation.ValidationUtil.validateId;
import static com.futurefragment.rs.validation.ValidationUtil.validateIdForCreate;
import static com.futurefragment.rs.validation.ValidationUtil.validateIfNullDto;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.futurefragment.constant.ExceptionConstants;
import com.futurefragment.dto.api.ApiKeyDto;

public final class ApiKeyValidationUtil {

	private static final String METHOD_VALIDATE_API_KEY_DTOS_FOR_CREATE = "validateApiKeyDtosForCreate";
	private static final String METHOD_VALIDATE_API_KEY_DTOS_FOR_UPDATE = "validateApiKeyDtosForUpdate";
	private static final String FIELD_API_KEY_KEY = "key";
	private static final String FIELD_API_KEY_DESCRIPTION = "description";

	public static final Class<ApiKeyDto> DTO_TYPE = ApiKeyDto.class;
	public static final String ERR_INVALID_KEY_DESCRIPTION = "The key field was set for a create API key request.";
	public static final String ERR_INVALID_KEY_CORRECTION = "Do not provide a key in the create API key request. The system will generate one for you.";
	public static final String ERR_EMPTY_KEY_DESCRIPTION = "Invalid key.";
	public static final String ERR_EMPTY_KEY_CORRECTION = "Provide a value for the key field.";
	public static final String ERR_INVALID_DESCRIPTION_DESCRIPTION = "Invalid description.";
	public static final String ERR_INVALID_DESCRIPTION_CORRECTION = "Provide a description for the API key.";

	private ApiKeyValidationUtil() {
	}

	public static void validateApiKeyDtosForCreate(List<ApiKeyDto> apiKeys) {
		validateDataList(DTO_TYPE, apiKeys);

		apiKeys.forEach(key -> {
			validateIfNullDto(DTO_TYPE, key);
			validateIdForCreate(DTO_TYPE, key.getId());

			String keyToCreate = key.getKey();
			if (!StringUtils.isEmpty(keyToCreate)) {
				throw getAudioMinerExceptionForInvalidField(DTO_TYPE, METHOD_VALIDATE_API_KEY_DTOS_FOR_CREATE, null,
						FIELD_API_KEY_KEY, keyToCreate, ERR_INVALID_KEY_DESCRIPTION, ERR_INVALID_KEY_CORRECTION,
						ERR_INVALID_KEY_DESCRIPTION, ExceptionConstants.ERR_CONTACT_ADMIN_USER_CORRECTION);
			}
		});
	}

	public static void validateApiKeyDtosForUpdate(List<ApiKeyDto> apiKeys) {
		validateDataList(DTO_TYPE, apiKeys);

		apiKeys.forEach(key -> {
			validateIfNullDto(DTO_TYPE, key);
			validateId(DTO_TYPE, key.getId());

			String description = key.getDescription();
			if (StringUtils.isEmpty(description)) {
				throw getAudioMinerExceptionForInvalidField(DTO_TYPE, METHOD_VALIDATE_API_KEY_DTOS_FOR_UPDATE, null,
						FIELD_API_KEY_DESCRIPTION, description, ERR_INVALID_DESCRIPTION_DESCRIPTION,
						ERR_INVALID_DESCRIPTION_CORRECTION, ERR_INVALID_DESCRIPTION_DESCRIPTION,
						ExceptionConstants.ERR_CONTACT_ADMIN_USER_CORRECTION);
			}
		});
	}

	public static void validateKeyForQuery(String key) {
		if (StringUtils.isEmpty(key)) {
			throw getAudioMinerExceptionForInvalidField(DTO_TYPE, METHOD_VALIDATE_API_KEY_DTOS_FOR_CREATE, null,
					FIELD_API_KEY_KEY, key, ERR_EMPTY_KEY_DESCRIPTION, ERR_EMPTY_KEY_CORRECTION,
					ERR_EMPTY_KEY_DESCRIPTION, ExceptionConstants.ERR_CONTACT_ADMIN_USER_CORRECTION);
		}
	}

}
