package com.futurefragment.rs.validation;

import org.apache.commons.lang3.StringUtils;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

import com.futurefragment.constant.ExceptionConstants;
import com.futurefragment.exception.AudioMinerExceptionFactory;
import com.futurefragment.exception.ExceptionContext;
import com.futurefragment.exception.Severity;

public final class MultipartFormDataInputValidationUtil {

	private static final String CLASS_NAME = MultipartFormDataInputValidationUtil.class.getSimpleName();
	private static final String METOD_VALIDATE_FORM_DATA = "validateMultipartFormDataInput";

	private static final String FIELD_MULTIPART_FORM_DATA = "MultipartFormDataInput.formDataMap.key";

	public static final String ERR_INVALID_MULTIPART_FORM_DATA_DESCRIPTION = "No input part found that contains data.";
	public static final String ERR_INVALID_MULTIPART_FORM_DATA_CORRECTION = "Ensure the multipart form data input contains 1 input part.";

	private MultipartFormDataInputValidationUtil() {
	}

	public static void validateMultipartFormDataInput(MultipartFormDataInput input) {
		boolean keyExists = input.getFormDataMap().keySet().stream().anyMatch(s -> StringUtils.isNotEmpty(s));

		if (!keyExists) {
			// @formatter:off
			ExceptionContext exceptionContext = ExceptionContext
					.getInstance(ExceptionConstants.APPLICATION_LAYER_RESTFUL, CLASS_NAME, METOD_VALIDATE_FORM_DATA, null)
					.setSeverity(Severity.FATAL)
					.addParameter(FIELD_MULTIPART_FORM_DATA, null)
					.setAdminErrorDescription(ERR_INVALID_MULTIPART_FORM_DATA_DESCRIPTION)
					.setAdminErrorCorrection(ERR_INVALID_MULTIPART_FORM_DATA_CORRECTION)
					.setUserErrorDescription(ERR_INVALID_MULTIPART_FORM_DATA_DESCRIPTION)
					.setUserErrorCorrection(ExceptionConstants.ERR_CONTACT_ADMIN_USER_CORRECTION);
			// @formatter:on

			throw AudioMinerExceptionFactory.raiseException(exceptionContext);
		}
	}

}
