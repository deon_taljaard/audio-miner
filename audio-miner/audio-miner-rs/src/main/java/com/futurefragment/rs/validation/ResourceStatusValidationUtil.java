package com.futurefragment.rs.validation;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.futurefragment.constant.ExceptionConstants;
import com.futurefragment.dto.common.ResourceStatusDto;
import com.futurefragment.enumlookups.ResourceStatusLookup;
import com.futurefragment.exception.AudioMinerExceptionFactory;
import com.futurefragment.exception.ExceptionContext;
import static com.futurefragment.rs.validation.ValidationUtil.*;

public final class ResourceStatusValidationUtil {

	private static final String CLASS_NAME = ResourceStatusValidationUtil.class.getSimpleName();
	private static final String METHOD_NAME = "validateResourceStatus";

	private static final String FIELD_RESOURCE_STATUS = "resourceStatus";

	public static final Class<ResourceStatusDto> DTO_TYPE = ResourceStatusDto.class;
	public static final String ERR_NULL_RESOURCE_STATUS_DESCRIPTION = "Null or empty resourceStatus.";
	public static final String ERR_INVALID_RESOURCE_STATUS_DESCRIPTION = "Invalid resourceStatus.";
	public static final String ERR_INVALID_RESOURCE_STATUS_CORRECTION = String
			.format("Ensure the resourceStatus is one of %s.", ResourceStatusLookup.getValuesAsCommaString());

	private ResourceStatusValidationUtil() {
	}

	public static void validateResourceStatusDtos(List<ResourceStatusDto> resourceStatus) {
		validateDataList(DTO_TYPE, resourceStatus);

		resourceStatus.forEach(p -> {
			validateIfNullDto(DTO_TYPE, p);
			validateResourceStatus(p.getResourceStatus());
			validateId(DTO_TYPE, p.getId());

			validateResourceStatus(p.getResourceStatus());
		});
	}

	public static void validateResourceStatus(String resourceStatus) {
		if (StringUtils.isEmpty(resourceStatus)) {
			// @formatter:off
			ExceptionContext exceptionContext = ExceptionContext
					.getInstance(ExceptionConstants.APPLICATION_LAYER_CORE, CLASS_NAME, METHOD_NAME, null)
					.addParameter(FIELD_RESOURCE_STATUS, resourceStatus)
					.setAdminErrorDescription(ERR_NULL_RESOURCE_STATUS_DESCRIPTION)
					.setAdminErrorCorrection(ERR_INVALID_RESOURCE_STATUS_CORRECTION)
					.setUserErrorDescription(ERR_NULL_RESOURCE_STATUS_DESCRIPTION)
					.setUserErrorCorrection(ExceptionConstants.ERR_CONTACT_ADMIN_USER_CORRECTION);

			throw AudioMinerExceptionFactory.raiseException(exceptionContext);
			// @formatter:on
		}

		try {
			ResourceStatusLookup.valueOf(resourceStatus.toUpperCase());
		} catch (IllegalArgumentException ex) {
			// @formatter:off
			ExceptionContext exceptionContext = ExceptionContext
					.getInstance(ExceptionConstants.APPLICATION_LAYER_CORE, CLASS_NAME, METHOD_NAME, ex)
					.addParameter(FIELD_RESOURCE_STATUS, resourceStatus)
					.setAdminErrorDescription(ERR_INVALID_RESOURCE_STATUS_DESCRIPTION)
					.setAdminErrorCorrection(ERR_INVALID_RESOURCE_STATUS_CORRECTION)
					.setUserErrorDescription(ERR_INVALID_RESOURCE_STATUS_DESCRIPTION)
					.setUserErrorCorrection(ExceptionConstants.ERR_CONTACT_ADMIN_USER_CORRECTION);

			throw AudioMinerExceptionFactory.raiseException(exceptionContext);
			// @formatter:on
		}
	}
}
