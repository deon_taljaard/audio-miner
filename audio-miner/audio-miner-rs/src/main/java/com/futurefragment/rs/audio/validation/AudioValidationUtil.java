package com.futurefragment.rs.audio.validation;

import static com.futurefragment.rs.validation.ValidationUtil.validateDataList;
import static com.futurefragment.rs.validation.ValidationUtil.validateId;
import static com.futurefragment.rs.validation.ValidationUtil.validateIdForCreate;
import static com.futurefragment.rs.validation.ValidationUtil.validateIfNullDto;
import static com.futurefragment.rs.validation.ValidationUtil.validateStringField;

import java.util.List;

import com.futurefragment.dto.audio.AudioDto;

public final class AudioValidationUtil {

	private static final String METHOD_VALIDATE_AUDIO = "validateAudioDto";
	private static final String FIELD_AUDIO_DESCRIPTION = "description";
	private static final String FIELD_AUDIO_RESOURCE_PATH = "resourcePath";

	public static final Class<AudioDto> DTO_TYPE = AudioDto.class;

	private AudioValidationUtil() {
	}

	public static void validateAudioDtosForCreate(List<AudioDto> audios) {
		validateDataList(DTO_TYPE, audios);

		audios.forEach(a -> {
			validateAudioDto(a);
			validateIdForCreate(DTO_TYPE, a.getId());
		});
	}

	public static void validateAudioDtosForUpdate(List<AudioDto> audios) {
		validateDataList(DTO_TYPE, audios);

		audios.forEach(a -> {
			validateAudioDto(a);
			validateId(DTO_TYPE, a.getId());
			validateAudioResourcePath(a.getResourcePath());
		});
	}

	public static void validateAudioDescription(String description) {
		validateStringField(DTO_TYPE, description, FIELD_AUDIO_DESCRIPTION, METHOD_VALIDATE_AUDIO);
	}

	public static void validateAudioResourcePath(String resourcePath) {
		validateStringField(DTO_TYPE, resourcePath, FIELD_AUDIO_RESOURCE_PATH, METHOD_VALIDATE_AUDIO);
	}

	public static void validateAudioDto(AudioDto audio) {
		validateIfNullDto(DTO_TYPE, audio);

		validateAudioDescription(audio.getDescription());
	}

}
