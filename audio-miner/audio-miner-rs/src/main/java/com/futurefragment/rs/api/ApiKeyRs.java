package com.futurefragment.rs.api;

import java.util.Arrays;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.slf4j.Logger;

import com.futurefragment.api.ApiKeyManager;
import com.futurefragment.dto.api.ApiKeyDto;
import com.futurefragment.dto.common.FutureFragmentCollectionDto;
import com.futurefragment.rs.api.validation.ApiKeyValidationUtil;
import com.futurefragment.rs.validation.ValidationUtil;
import com.futurefragment.util.jwt.APIKeySecured;
import com.futurefragment.util.response.RsResponseFactory;

@APIKeySecured
@Stateless
@Path("/apiKeys")
public class ApiKeyRs {

	private static final String RESOURCE_API_KEY = "apiKeys";

	@Inject
	private Logger logger;

	@Inject
	private ApiKeyManager apiKeyManager;

	// -------------------------------------------------------
	// @GET apiKeys <host>:<port>/voice-collage/apiKeys/{id}
	// -------------------------------------------------------

	@Path("{id}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getApiKeyById(@PathParam("id") Long id) {
		try {
			ApiKeyDto apiKey = apiKeyManager.getApiKeyById(id);

			return RsResponseFactory.getOkResponseForDtos(ApiKeyDto.class, RESOURCE_API_KEY, Arrays.asList(apiKey),
					logger);
		} catch (Exception ex) {
			return RsResponseFactory.getErrorResponse(ApiKeyDto.class, RESOURCE_API_KEY, ex, logger);
		}
	}

	// -------------------------------------------------------
	// @PUT apiKeys <host>:<port>/voice-collage/apiKeys
	// -------------------------------------------------------

	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateApiKeys(FutureFragmentCollectionDto<ApiKeyDto> apiKeys) {
		try {
			ApiKeyValidationUtil.validateApiKeyDtosForUpdate(apiKeys.getData());

			List<ApiKeyDto> updatedApiKeys = apiKeyManager.updateApiKeys(apiKeys.getData());

			return RsResponseFactory.getOkResponseForDtos(ApiKeyDto.class, RESOURCE_API_KEY, updatedApiKeys, logger);
		} catch (Exception ex) {
			return RsResponseFactory.getErrorResponse(ApiKeyDto.class, RESOURCE_API_KEY, ex, logger);
		}
	}

	// -------------------------------------------------------
	// @POST apiKeys <host>:<port>/voice-collage/apiKeys
	// -------------------------------------------------------

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response createApiKeys(FutureFragmentCollectionDto<ApiKeyDto> apiKeys) {
		try {
			ApiKeyValidationUtil.validateApiKeyDtosForCreate(apiKeys.getData());

			List<ApiKeyDto> createdApiKeys = apiKeyManager.createApiKeys(apiKeys.getData());

			return RsResponseFactory.getOkResponseForDtos(ApiKeyDto.class, RESOURCE_API_KEY, createdApiKeys, logger);
		} catch (Exception ex) {
			return RsResponseFactory.getErrorResponse(ApiKeyDto.class, RESOURCE_API_KEY, ex, logger);
		}
	}

	// -------------------------------------------------------
	// @DELETE apiKeys <host>:<port>/voice-collage/apiKeys/{id}
	// -------------------------------------------------------

	@Path("{id}")
	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteApiKeyById(@PathParam("id") Long id) {
		try {
			ValidationUtil.validateId(ApiKeyValidationUtil.DTO_TYPE, id);

			boolean isDeleteSuccessful = apiKeyManager.deleteApiKey(id);

			return RsResponseFactory.getOkResponseForDeletedResource(ApiKeyDto.class, RESOURCE_API_KEY,
					isDeleteSuccessful, id, logger);
		} catch (Exception ex) {
			return RsResponseFactory.getErrorResponse(ApiKeyDto.class, RESOURCE_API_KEY, ex, logger);
		}
	}

	// --------------------------------------------------------------------------
	// @GET voices <host>:<port>/voice-collage/apiKeys/query
	// Supported query params: key
	// Examples:
	// * <host>:<port>/voice-collage/apiKeys/query?name=Stewy
	// --------------------------------------------------------------------------

	// TODO: Is exposing this method necessary?

	@Path("/query")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getApiKeyByQuery(@Context UriInfo info) {
		try {
			MultivaluedMap<String, String> queryParameters = info.getQueryParameters();

			String key = queryParameters.getFirst("key");

			ApiKeyValidationUtil.validateKeyForQuery(key);

			return apiKeyManager.getApiKeyByKey(key)
					.map(apiKey -> RsResponseFactory.getOkResponseForDtos(ApiKeyDto.class, RESOURCE_API_KEY,
							Arrays.asList(apiKey), logger))
					.orElse(RsResponseFactory.getErrorResponse(ApiKeyDto.class, RESOURCE_API_KEY,
							new Exception(String.format("No key found with key '%s'", key)), logger));
		} catch (Exception ex) {
			return RsResponseFactory.getErrorResponse(ApiKeyDto.class, RESOURCE_API_KEY, ex, logger);
		}
	}
}
