package com.futurefragment.util.file;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;

import org.apache.commons.io.IOUtils;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;

import com.futurefragment.constant.ExceptionConstants;
import com.futurefragment.dto.common.AudioMinerUploadDto;
import com.futurefragment.dto.common.FileDto;
import com.futurefragment.dto.common.FileType;
import com.futurefragment.dto.common.FutureFragmentCollectionDto;
import com.futurefragment.exception.ExceptionContext;
import com.futurefragment.exception.Severity;
import com.futurefragment.exception.AudioMinerExceptionFactory;
import com.futurefragment.util.json.CheckedFunction;

public final class RsFileUploadHandler {

	private static final String CLASS_NAME = RsFileUploadHandler.class.getSimpleName();

	private static final String METHOD_CREATE_VOICE_COLLAGE_UPLOAD_DTO = "createVoiceCollageUploadDto";
	private static final String METHOD_EXTRACT_CONTENT_TYPE = "extractContentTypeFromHeader";

	private static final String ERR_UNABLE_TO_CONVERT_TO_BYTE_ARR_DESCRIPTION = "An error occurred while converting the file to a byte array.";
	private static final String ERR_INVALID_CONTENT_TYPE_DESCRIPTION = "No Content-Type was defined for the form part.";
	private static final String ERR_INVALID_CONTENT_TYPE_CORRECTION = "Please provide a valid Content-Type, for example: 'Content-Type application/json'";

	private static final List<String> SUPPORTED_MIME_TYPES = Arrays.asList(MediaType.APPLICATION_OCTET_STREAM,
			MediaType.APPLICATION_FORM_URLENCODED, "audio/mpeg", "image/png");

	private static final RsFileUploadHandler INSTANCE = new RsFileUploadHandler();

	private RsFileUploadHandler() {
	}

	public static RsFileUploadHandler getInstance() {
		return INSTANCE;
	}

	public <T extends AudioMinerUploadDto> FutureFragmentCollectionDto<T> parseFormDataForVoiceCollageUploadDto(
			Map<String, List<InputPart>> uploadForm, FileType fileType,
			CheckedFunction<InputStream, FutureFragmentCollectionDto<T>> deserialiserFunction) {

		FutureFragmentCollectionDto<T> uploadCollection = null;
		FileDto file = null;

		for (Map.Entry<String, List<InputPart>> entry : uploadForm.entrySet()) {
			if (entry.getKey() != null) {
				List<InputPart> inputParts = entry.getValue();
				for (InputPart inputPart : inputParts) {
					try {
						// TODO: Use this map for metadata?
						MultivaluedMap<String, String> headers = inputPart.getHeaders();

						InputStream inputStream = inputPart.getBody(InputStream.class, null);

						String contentType = extractContentTypeFromHeader(headers);
						if (SUPPORTED_MIME_TYPES.contains(contentType)) {
							file = new FileDto();
							byte[] fileData = IOUtils.toByteArray(inputStream);
							file.setFileData(fileData);
							file.setFileType(fileType);
						} else if (MediaType.APPLICATION_JSON.equals(contentType)
								|| contentType.contains(MediaType.APPLICATION_JSON)) {
							uploadCollection = deserialiserFunction.apply(inputStream);
							if (file != null && uploadCollection != null) {
								uploadCollection.getData().get(0).setFile(file);
								break;
							}
						}

					} catch (IOException ex) {
						// @formatter:off
						ExceptionContext exceptionContext = ExceptionContext
								.getInstance(ExceptionConstants.APPLICATION_LAYER_RESTFUL, CLASS_NAME,
										METHOD_CREATE_VOICE_COLLAGE_UPLOAD_DTO, ex)
								.setSeverity(Severity.FATAL)
								.setAdminErrorDescription(ERR_UNABLE_TO_CONVERT_TO_BYTE_ARR_DESCRIPTION)
								.setAdminErrorCorrection(ExceptionConstants.ERR_CONTACT_ADMIN_USER_CORRECTION)
								.setUserErrorDescription(ERR_UNABLE_TO_CONVERT_TO_BYTE_ARR_DESCRIPTION)
								.setUserErrorCorrection(ExceptionConstants.ERR_CONTACT_ADMIN_USER_CORRECTION);
						// @formatter:on

						throw AudioMinerExceptionFactory.raiseException(exceptionContext);
					}
				}
			}
		}

		return uploadCollection;
	}

	private String extractContentTypeFromHeader(MultivaluedMap<String, String> headers) {
		List<String> contentTypes = headers.get(HttpHeaders.CONTENT_TYPE);

		if (contentTypes == null || contentTypes.isEmpty()) {
			// @formatter:off
			ExceptionContext exceptionContext = ExceptionContext
					.getInstance(ExceptionConstants.APPLICATION_LAYER_RESTFUL, CLASS_NAME, METHOD_EXTRACT_CONTENT_TYPE,
							null)
					.setSeverity(Severity.FATAL).setAdminErrorDescription(ERR_INVALID_CONTENT_TYPE_DESCRIPTION)
					.setAdminErrorCorrection(ERR_INVALID_CONTENT_TYPE_CORRECTION)
					.setUserErrorDescription(ERR_INVALID_CONTENT_TYPE_DESCRIPTION)
					.setUserErrorCorrection(ExceptionConstants.ERR_CONTACT_ADMIN_USER_CORRECTION);
			// @formatter:on

			throw AudioMinerExceptionFactory.raiseException(exceptionContext);
		}

		return contentTypes.get(0);
	}

	// These functions are not currently being used. Need to establish an
	// approach to extract metadata from upload request.

	// private static String getFileName(MultivaluedMap<String, String> header)
	// {
	// String[] contentDisposition =
	// header.getFirst("Content-Disposition").split(";");
	//
	// for (String filename : contentDisposition) {
	// if ((filename.trim().startsWith("filename"))) {
	// String[] name = filename.split("=");
	//
	// String finalFileName = name[1].trim().replaceAll("\"", "");
	// return finalFileName;
	// }
	// }
	//
	// return "unknown";
	// }
}
