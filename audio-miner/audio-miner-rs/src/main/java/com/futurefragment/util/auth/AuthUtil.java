package com.futurefragment.util.auth;

import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;

import com.futurefragment.constant.ExceptionConstants;
import com.futurefragment.constant.RsConstants;
import com.futurefragment.dto.person.PersonAuthDto;
import com.futurefragment.exception.AudioMinerException;
import com.futurefragment.exception.ExceptionContext;
import com.futurefragment.exception.Severity;
import com.futurefragment.exception.AudioMinerExceptionFactory;

public final class AuthUtil {

	private static final String CLASS_NAME = AuthUtil.class.getSimpleName();

	private static final int AUTH_HEADER_FORMAT_NUM_PARTS = 2;
	private static final String METHOD_NAME = "validateAuthToken";

	protected static final String ERR_UNAUTHED_AUTH_HEADER_NOT_FOUND = "Unauthorized: No authentication header was found";
	protected static final String ERR_UNAUTHED_INVALID_AUTH_HEADER_FORMAT = "Unauthorized: Invalid authentication header format";

	protected static final String ERR_NULL_OR_EMPTY_AUTH_HEADER = "Auth header is null.";
	protected static final String ERR_MALFORMED_AUTH_HEADER = "Auth header is malformed.";
	protected static final String ERR_CREDENTIALS_NOT_ENCODED = "User credentials are not properly Base64 encoded.";
	protected static final String ERR_AUTH_CORRECTION = "Ensure an auth header is placed in the request header.";
	protected static final String ERR_USER_IDS_DONT_MATCH = "The user is not authorized to access this resource.";

	private AuthUtil() {
	}

	public static final PersonAuthDto getPersonAuthCredentials(final String authHeader) {
		if (!StringUtils.isEmpty(authHeader)) {
			String[] parts = authHeader.split(RsConstants.JWT_AUTH_HEADER_SPLIT_TOKEN);
			if (isValidAuthBasicHeader(parts)) {
				return getPersonAuthCredentials(parts);
			} else {
				throw createExceptionForAuthError(ERR_MALFORMED_AUTH_HEADER, null);
			}
		} else {
			throw createExceptionForAuthError(ERR_NULL_OR_EMPTY_AUTH_HEADER, null);
		}
	}

	protected static final PersonAuthDto getPersonAuthCredentials(final String[] parts) {
		try {
			byte[] decode = Base64.getDecoder().decode(parts[RsConstants.LOGIN_AUTH_HEADER_CREDENTIALS_INDEX]);

			PersonAuthDto personAuth = new PersonAuthDto();

			String userPass = new String(decode, RsConstants.BYTE_T0_STR_ENCODING);
			String[] userPassParts = userPass.split(RsConstants.LOGIN_AUTH_HEADER_SPLIT_TOKEN);

			if (userPassParts.length == RsConstants.LOGIN_AUTH_USER_PARTS_SIZE) {
				String email = userPassParts[RsConstants.LOGIN_AUTH_HEADER_USERNAME_INDEX];
				personAuth.setEmail(email);

				char[] pass = userPassParts[RsConstants.LOGIN_AUTH_HEADER_PASSWORD_INDEX].toCharArray();
				personAuth.setPassword(pass);

				return personAuth;
			} else {
				throw createExceptionForAuthError(ERR_MALFORMED_AUTH_HEADER, null);
			}
		} catch (Exception ex) {
			throw createExceptionForAuthError(ERR_CREDENTIALS_NOT_ENCODED, ex);
		}
	}

	protected static final boolean isValidAuthBasicHeader(final String[] parts) {
		// @foramtter:off
		return parts != null && parts.length == 2 && parts[RsConstants.LOGIN_AUTH_HEADER_SCHEME_INDEX].trim()
				.matches(RsConstants.LOGIN_AUTH_BASIC_PATTERN);
		// @formatter:on
	}

	public static final String getTokenFromHeader(final String authHeader, final Logger logger) {
		String token = null;

		if (!StringUtils.isEmpty(authHeader)) {
			String[] parts = authHeader.split(" ");
			if (parts.length != AUTH_HEADER_FORMAT_NUM_PARTS) {
				logger.error(ERR_UNAUTHED_INVALID_AUTH_HEADER_FORMAT);
			} else {
				String scheme = parts[RsConstants.JWT_AUTH_HEADER_SCHEME_INDEX];
				String credentials = parts[RsConstants.JWT_AUTH_HEADER_CREDENTIALS_INDEX];

				Pattern pattern = Pattern.compile(RsConstants.JWT_AUTH_BEARER_PATTERN, Pattern.CASE_INSENSITIVE);
				if (pattern.matcher(scheme).matches()) {
					token = credentials;
				}
			}
		} else {
			logger.error(ERR_UNAUTHED_AUTH_HEADER_NOT_FOUND);
		}

		return token;
	}

	public static void validateOAuthToken(String token) {
		if (StringUtils.isEmpty(token)) {
			// @formatter:off
            ExceptionContext exceptionContext = ExceptionContext.getInstance(ExceptionConstants.APPLICATION_LAYER_RESTFUL, 
                    CLASS_NAME, METHOD_NAME, null)
                    .setSeverity(Severity.FATAL)
                    .setAdminErrorDescription(RsConstants.ERR_GOOGLE_ID_TOKEN_EMPTY_DESCRIPTION)
                    .setAdminErrorCorrection(RsConstants.ERR_GOOGLE_ID_TOKEN_INVALID_CORRECTION)
                    .setUserErrorDescription(RsConstants.ERR_GOOGLE_ID_TOKEN_EMPTY_DESCRIPTION)
                    .setUserErrorCorrection(ExceptionConstants.ERR_CONTACT_ADMIN_USER_CORRECTION);
            // @formatter:on

			throw AudioMinerExceptionFactory.raiseException(exceptionContext);
		}
	}

	private static final AudioMinerException createExceptionForAuthError(final String message, final Exception ex) {
		return createExceptionForAuthError(message, ex, new HashMap<>());
	}

	private static final AudioMinerException createExceptionForAuthError(final String message, final Exception ex,
			final Map<String, Object> parameters) {
		// @formatter:off
        ExceptionContext exceptionContext = ExceptionContext.getInstance(ExceptionConstants.APPLICATION_LAYER_RESTFUL, 
                CLASS_NAME, "createExceptionForAuthError", ex)
        	    .setSeverity(Severity.FATAL)
                .setAdminErrorDescription(message + ((ex != null) ? " Msg: " + ex.getMessage() : ""))
                .setAdminErrorCorrection(ERR_AUTH_CORRECTION)
                .setUserErrorDescription(RsConstants.ERR_AUTH_TOKEN_EXPIRED_USER_DESCRIPTION)
                .setUserErrorCorrection(RsConstants.ERR_AUTH_TOKEN_EXPIRED_USER_CORRECTION);
        
        exceptionContext.getParameters().putAll(parameters);
        // @formatter:on

		return AudioMinerExceptionFactory.raiseException(exceptionContext);
	}
}
