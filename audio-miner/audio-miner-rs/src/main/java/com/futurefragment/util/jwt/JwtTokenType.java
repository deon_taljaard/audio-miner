package com.futurefragment.util.jwt;

public enum JwtTokenType {
	ACCESS, REFRESH;
}
