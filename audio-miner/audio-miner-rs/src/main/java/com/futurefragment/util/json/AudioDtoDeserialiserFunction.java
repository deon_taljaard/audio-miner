package com.futurefragment.util.json;

import java.io.IOException;
import java.io.InputStream;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.futurefragment.dto.audio.AudioDto;
import com.futurefragment.dto.common.FutureFragmentCollectionDto;

public final class AudioDtoDeserialiserFunction implements CheckedFunction<InputStream, FutureFragmentCollectionDto<AudioDto>> {

	private static final AudioDtoDeserialiserFunction INSTANCE = new AudioDtoDeserialiserFunction();

	private AudioDtoDeserialiserFunction() {
	}

	public static AudioDtoDeserialiserFunction getInstance() {
		return INSTANCE;
	}

	@Override
	public FutureFragmentCollectionDto<AudioDto> apply(InputStream input) throws IOException {
		return new ObjectMapper().readerFor(new TypeReference<FutureFragmentCollectionDto<AudioDto>>() {
		}).readValue(input);
	}
}
