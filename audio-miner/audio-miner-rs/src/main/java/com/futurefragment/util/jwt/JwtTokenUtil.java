package com.futurefragment.util.jwt;

import java.io.UnsupportedEncodingException;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.TimeZone;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTCreator.Builder;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.Claim;
import com.futurefragment.config.AudioMinerConfig;
import com.futurefragment.constant.ExceptionConstants;
import com.futurefragment.constant.RsConstants;
import com.futurefragment.exception.AudioMinerException;
import com.futurefragment.exception.ExceptionContext;
import com.futurefragment.exception.Severity;
import com.futurefragment.exception.AudioMinerExceptionFactory;

public final class JwtTokenUtil {

	private static final String CLASS_NAME = JwtTokenUtil.class.getSimpleName();

	private static final String METHOD_GENERATE_TOKEN = "generateToken";
	private static final String METHOD_VERIFY_TOKEN = "verifyToken";

	private static final String ERR_JWT_TOKEN_SIGN_DESCRIPTION = "Unable to sign authentication token.";
	private static final String ERR_JWT_INVALID_ENCODING_DESCRIPTION = "Invalid argument or encoding.";

	private JwtTokenUtil() {
	}

	public static String generateToken(String userId, JwtTokenType tokenType) {
		try {
			// @formatter:off
			Builder build = JWT.create()
					.withIssuer(RsConstants.JWT_CLAIM_ISS)
					.withSubject(userId)
					.withClaim(RsConstants.JWT_CLAIM_SCOPE, "roles app_user");
			// @formatter:on
			
			Calendar tokenExpTime = Calendar.getInstance(TimeZone.getTimeZone(ZoneId.of("UTC")));
			if (JwtTokenType.ACCESS.equals(tokenType)) {
				tokenExpTime.add(Calendar.MINUTE, Integer.valueOf(AudioMinerConfig.ACCESS_TOKEN_TTL));
			} else {
				tokenExpTime.add(Calendar.DAY_OF_YEAR, Integer.valueOf(AudioMinerConfig.REFRESH_TOKEN_TTL));
			}
			
			build.withExpiresAt(new Date(tokenExpTime.getTimeInMillis()));

			return build.sign(Algorithm.HMAC512(AudioMinerConfig.JWT_SECRET));
		} catch (JWTCreationException ex) {
			throw createAudioMinerExceptionForJwtException(ex, METHOD_GENERATE_TOKEN, ERR_JWT_TOKEN_SIGN_DESCRIPTION,
					ExceptionConstants.ERR_CONTACT_ADMIN_USER_CORRECTION);
		} catch (IllegalArgumentException | UnsupportedEncodingException ex) {
			throw createAudioMinerExceptionForJwtException(ex, METHOD_GENERATE_TOKEN,
					ERR_JWT_INVALID_ENCODING_DESCRIPTION, ExceptionConstants.ERR_CONTACT_ADMIN_USER_CORRECTION);
		}
	}

	public static Map<String, Claim> verifyToken(String token) {

		try {
			// @formatter:off
			JWTVerifier verifier = JWT.require(Algorithm.HMAC512(AudioMinerConfig.JWT_SECRET))
					.withIssuer(RsConstants.JWT_CLAIM_ISS)
					.build(); 
			// @formatter:on				

			JWT jwt = (JWT) verifier.verify(token);

			return jwt.getClaims();
		} catch (JWTVerificationException ex) {
			throw createAudioMinerExceptionForJwtException(ex, METHOD_VERIFY_TOKEN,
					"Authentication token has expired.", RsConstants.ERR_AUTH_TOKEN_EXPIRED_USER_CORRECTION);
		} catch (IllegalArgumentException | UnsupportedEncodingException ex) {
			throw createAudioMinerExceptionForJwtException(ex, METHOD_VERIFY_TOKEN,
					ERR_JWT_INVALID_ENCODING_DESCRIPTION, RsConstants.ERR_AUTH_TOKEN_EXPIRED_USER_CORRECTION);
		} catch (Exception ex) {
			throw createAudioMinerExceptionForJwtException(ex, METHOD_VERIFY_TOKEN,
					"A general exception occurred while verifying token. Unable to verify authentication token.",
					ExceptionConstants.ERR_CONTACT_ADMIN_USER_CORRECTION);
		}
	}

	private static AudioMinerException createAudioMinerExceptionForJwtException(final Exception ex,
			final String methodName, final String errDescription, final String errCorrection) {
		// @formatter:off
        ExceptionContext exceptionContext = ExceptionContext.getInstance(ExceptionConstants.APPLICATION_LAYER_RESTFUL, 
                CLASS_NAME, methodName, ex)
                .setSeverity(Severity.FATAL)
                .setAdminErrorDescription(errDescription + ((ex != null) ? " Msg: " + ex.getMessage() : ""))
                .setAdminErrorCorrection(errCorrection)
                .setUserErrorDescription(errDescription)
                .setUserErrorCorrection(ExceptionConstants.ERR_CONTACT_ADMIN_USER_CORRECTION);
        // @formatter:on

		return AudioMinerExceptionFactory.raiseException(exceptionContext);
	}

}
