package com.futurefragment.util.json;

import java.io.IOException;

@FunctionalInterface
public interface CheckedFunction<T, R> {
	R apply(T t) throws IOException;
}
