package com.futurefragment.util.response;

import java.util.List;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.futurefragment.constant.ExceptionConstants;
import com.futurefragment.dto.common.AudioMinerDto;
import com.futurefragment.dto.common.AudioMinerResponseDto;
import com.futurefragment.dto.common.DeleteResourceDto;
import com.futurefragment.dto.common.ErrorDto;
import com.futurefragment.dto.common.FutureFragmentCollectionDto;
import com.futurefragment.dto.common.ResourceCountDto;
import com.futurefragment.exception.AudioMinerException;
import com.futurefragment.exception.AudioMinerExceptionFactory;
import com.futurefragment.exception.ExceptionContext;
import com.futurefragment.exception.ExceptionLogUtil;
import com.futurefragment.exception.Severity;

public final class RsResponseFactory {

	private static final String CLASS_NAME = RsResponseFactory.class.getSimpleName();
	private static final String METHOD_GET_OK_RESPONSE = "getOkResponseForDtos";

	private static final String ERR_JSON_PARSE_DESCRIPTION = "A JSON parsing error occured.";

	private RsResponseFactory() {
	}

	public static <T extends AudioMinerDto> Response getOkResponseForDtos(final Class<T> dtoType,
			final String resource, final List<T> dtos, final Logger logger) {

		FutureFragmentCollectionDto<T> response = new FutureFragmentCollectionDto<>();
		response.setResource(resource);
		response.getData().addAll(dtos);

		return getResponse(dtoType, response, resource, logger);
	}

	public static <T extends AudioMinerDto> Response getOkResponseForResourceCount(final Class<T> dtoType,
			final String resource, final Long count, final Logger logger) {
		ResourceCountDto response = new ResourceCountDto();
		response.setCount(count);
		response.setResource(resource);

		return getResponse(dtoType, response, resource, logger);
	}

	public static <T extends AudioMinerDto> Response getOkResponseForDeletedResource(final Class<T> dtoType,
			final String resource, final boolean isDeleteSuccessful, final Long deletedId, final Logger logger) {

		DeleteResourceDto response = new DeleteResourceDto();
		response.setSuccess(isDeleteSuccessful);
		response.setResource(resource);
		response.setId(deletedId);

		return getResponse(dtoType, response, resource, logger);
	}

	public static <T extends AudioMinerDto> Response getErrorResponse(final Class<T> dtoType, final String resource,
			final Exception ex, final Logger logger) {
		return getErrorResponse(dtoType, resource, ex, Status.BAD_REQUEST, logger);
	}

	public static <T extends AudioMinerDto> Response getErrorResponse(final Class<T> dtoType, final String resource,
			final Exception ex, final Status status, final Logger logger) {
		ErrorDto response = new ErrorDto();

		if (ex != null) {
			if (ex instanceof AudioMinerException) {
				AudioMinerException vcex = (AudioMinerException) ex;
				setMessagesForErrorDto(response, vcex);
				ExceptionLogUtil.logError(vcex, logger);
			} else if (ex.getCause() instanceof AudioMinerException) {
				AudioMinerException vcex = (AudioMinerException) ex.getCause();
				setMessagesForErrorDto(response, vcex);
				ExceptionLogUtil.logError(vcex, logger);
			} else {
				logger.error(ex.getMessage());
				response.setDescription(ex.getMessage());
				response.setCorrection(ExceptionConstants.ERR_CONTACT_ADMIN_USER_CORRECTION);
			}
		} else {
			response.setDescription("An unknown error has occured.");
			response.setCorrection(ExceptionConstants.ERR_CONTACT_ADMIN_USER_CORRECTION);
		}

		try {
			// @formatter:off
			return Response.status(status)
					.entity(JsonObjectMapper
							.createObjectMapper()
							.writer()
							.withoutRootName()
							.writeValueAsString(response))
					.build();
			// @formatter:on

		} catch (JsonProcessingException jsonEx) {
			return getErrorResponseForJsonProcessingException(dtoType, resource, logger, jsonEx);
		}
	}

	private static void setMessagesForErrorDto(ErrorDto response, AudioMinerException vcex) {
		response.setDescription(vcex.getAdministratorErrorDescription());
		response.setCorrection(vcex.getAdministratorErrorCorrection());
	}

	private static <T extends AudioMinerDto, S extends AudioMinerResponseDto> Response getResponse(
			final Class<T> dtoType, final S response, final String resource, final Logger logger) {
		try {
			// @formatter:off
			return Response.status(Status.OK)
					.entity(JsonObjectMapper
							.createObjectMapper()
							.writer()
							.withoutRootName()
							.writeValueAsString(response))
					.build();
			// @formatter:on

		} catch (JsonProcessingException ex) {
			return getErrorResponseForJsonProcessingException(dtoType, resource, logger, ex);
		}
	}

	private static <T extends AudioMinerDto> Response getErrorResponseForJsonProcessingException(
			final Class<T> dtoType, final String resource, final Logger logger, final Exception ex) {
		// @formatter:off
		
		ExceptionContext exceptionContext = ExceptionContext
				.getInstance(ExceptionConstants.APPLICATION_LAYER_PERSISTENCE, CLASS_NAME, METHOD_GET_OK_RESPONSE, ex)
				.setSeverity(Severity.FATAL)
				.addParameter("resource", resource)
				.setAdminErrorDescription("An error occured while parsing the DTO to JSON. Msg: " + ex.getMessage())
				.setAdminErrorCorrection("Ensure the DTOs are properly constructed.")
				.setUserErrorDescription(ERR_JSON_PARSE_DESCRIPTION)
				.setUserErrorCorrection(ExceptionConstants.ERR_CONTACT_ADMIN_USER_CORRECTION);

		if(dtoType != null) {
			exceptionContext.addParameter("dtoType", dtoType.getClass().getSimpleName());
		}
		// @formatter:on

		ExceptionLogUtil.logError(AudioMinerExceptionFactory.raiseException(exceptionContext), logger);

		return Response.status(Status.INTERNAL_SERVER_ERROR).entity("A JSON parsing error occured.").build();

	}
}
