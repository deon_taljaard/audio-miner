package com.futurefragment.filter;

import java.util.function.Function;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;

import com.futurefragment.exception.ExceptionLogUtil;
import com.futurefragment.exception.AudioMinerException;
import com.futurefragment.util.auth.AuthUtil;

public class SecureFilterUtil {

	private SecureFilterUtil() {
	}

	public static void applyFilter(ContainerRequestContext requestContext, Function<String, Boolean> secureFunction,
			Logger logger) {
		try {
			String token = AuthUtil.getTokenFromHeader(requestContext.getHeaderString(HttpHeaders.AUTHORIZATION),
					logger);
			if (token != null) {
				try {
					if (!secureFunction.apply(token)) {
						unauhtorised(requestContext);
					}
				} catch (AudioMinerException ex) {
					ExceptionLogUtil.logError(ex, logger);

					unauhtorised(requestContext);
				} catch (Exception ex) {
					logger.error(ex.getMessage());

					unauhtorised(requestContext);
				}
			} else {
				unauhtorised(requestContext);
			}
		} catch (Exception ex) {
			unauhtorised(requestContext);
		}
	}

	public static void unauhtorised(ContainerRequestContext requestContext) {
		requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED).build());
	}
}
