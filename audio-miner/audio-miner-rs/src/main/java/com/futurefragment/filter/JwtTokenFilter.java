package com.futurefragment.filter;

import java.io.IOException;
import java.util.Map;
import java.util.function.Function;

import javax.annotation.Priority;
import javax.inject.Inject;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.ext.Provider;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;

import com.auth0.jwt.interfaces.Claim;
import com.futurefragment.constant.RsConstants;
import com.futurefragment.util.jwt.JWTSecured;
import com.futurefragment.util.jwt.JwtTokenUtil;

@JWTSecured
@Provider
@Priority(Priorities.AUTHENTICATION)
public class JwtTokenFilter implements ContainerRequestFilter {

	@Inject
	private Logger logger;

	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {
		SecureFilterUtil.applyFilter(requestContext, new JwtTokenSecureFilterFunction(requestContext), logger);
	}

	private final class JwtTokenSecureFilterFunction implements Function<String, Boolean> {

		private final ContainerRequestContext requestContext;

		public JwtTokenSecureFilterFunction(ContainerRequestContext requestContext) {
			this.requestContext = requestContext;
		}

		@Override
		public Boolean apply(final String token) {
			Map<String, Claim> decodedInfo = JwtTokenUtil.verifyToken(token);
			String userId = (String) decodedInfo.get(RsConstants.JWT_CLAIM_SUB).asString();

			if (StringUtils.isEmpty(userId)) {
				return false;
			} else {
				requestContext.setProperty(RsConstants.REQUEST_ATTR_USER_ID, userId);
				return true;
			}
		}
	}

}