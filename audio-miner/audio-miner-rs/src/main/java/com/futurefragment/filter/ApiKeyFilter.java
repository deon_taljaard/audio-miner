package com.futurefragment.filter;

import java.io.IOException;
import java.util.Optional;
import java.util.function.Function;

import javax.annotation.Priority;
import javax.inject.Inject;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.ext.Provider;

import org.slf4j.Logger;

import com.futurefragment.api.ApiKeyManager;
import com.futurefragment.dto.api.ApiKeyDto;
import com.futurefragment.util.jwt.APIKeySecured;

@APIKeySecured
@Provider
@Priority(Priorities.AUTHENTICATION)
public class ApiKeyFilter implements ContainerRequestFilter {

	@Inject
	private Logger logger;

	@Inject
	private ApiKeyManager apiKeyManager;

	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {
		SecureFilterUtil.applyFilter(requestContext, new ApiKeyFilterSecureFunction(), logger);
	}

	private final class ApiKeyFilterSecureFunction implements Function<String, Boolean> {
		@Override
		public Boolean apply(final String token) {
			Optional<ApiKeyDto> apiKeyByKey = apiKeyManager.getApiKeyByKey(token);

			if (apiKeyByKey.isPresent()) {
				// Potentially set additional information in header to verify
				// correct token usage, etc.
				return true;
			} else {
				return false;
			}
		}
	}

}