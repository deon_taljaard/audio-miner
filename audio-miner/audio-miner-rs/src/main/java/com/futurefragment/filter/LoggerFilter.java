package com.futurefragment.filter;

import java.io.IOException;

import javax.annotation.Priority;
import javax.inject.Inject;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.ext.Provider;

import org.slf4j.Logger;

import com.google.common.net.HttpHeaders;

//Note, this is an unbounded filter
@Provider
@Priority(Priorities.USER)
public class LoggerFilter implements ContainerResponseFilter {

	private static final String DIVIDER = String.format("%60s", ' ').replace(' ', '*');

	@Inject
	private Logger logger;

	@Override
	public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext)
			throws IOException {
		StringBuilder sb = new StringBuilder();

		sb.append(System.lineSeparator());
		sb.append(DIVIDER);
		sb.append(System.lineSeparator());
		sb.append("Request by UA:\t\t");
		sb.append(requestContext.getHeaderString(HttpHeaders.USER_AGENT));
		sb.append(System.lineSeparator());
		sb.append(requestContext.getMethod() + " to endpoint:\t");
		sb.append(requestContext.getUriInfo().getPath());
		sb.append(System.lineSeparator());
		sb.append("Response:\t\t");
		sb.append(responseContext.getStatusInfo().getStatusCode());
		sb.append(" - ");
		sb.append(responseContext.getStatusInfo().getReasonPhrase());
		sb.append(System.lineSeparator());
		sb.append(DIVIDER);
		sb.append(System.lineSeparator());

		logger.info(sb.toString());
	}

}