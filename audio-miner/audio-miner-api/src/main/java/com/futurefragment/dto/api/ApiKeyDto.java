package com.futurefragment.dto.api;

import java.io.Serializable;

import com.futurefragment.dto.common.ResourceStatusDto;
import com.futurefragment.dto.common.AudioMinerDto;

public class ApiKeyDto implements Serializable, AudioMinerDto {

	private static final long serialVersionUID = 1L;

	private Long id;
	private String key;
	private String description;
	private ResourceStatusDto apiKeyResourceStatus;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public ResourceStatusDto getApiKeyResourceStatus() {
		return apiKeyResourceStatus;
	}

	public void setApiKeyResourceStatus(ResourceStatusDto apiKeyResourceStatus) {
		this.apiKeyResourceStatus = apiKeyResourceStatus;
	}

}
