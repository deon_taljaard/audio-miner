package com.futurefragment.dto.common;

import java.io.Serializable;

public class DeleteResourceDto implements Serializable, AudioMinerResponseDto {

	private static final long serialVersionUID = 1L;

	private boolean success;
	private String resource;
	private Long id;

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getResource() {
		return resource;
	}

	public void setResource(String resource) {
		this.resource = resource;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
