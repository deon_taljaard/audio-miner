package com.futurefragment.dto.common;

import java.io.Serializable;

public class ResourceCountDto implements Serializable, AudioMinerResponseDto {

	private static final long serialVersionUID = 1L;

	private String resource;
	private Long count;

	public String getResource() {
		return resource;
	}

	public void setResource(String resource) {
		this.resource = resource;
	}

	public Long getCount() {
		return count;
	}

	public void setCount(Long count) {
		this.count = count;
	}
}
