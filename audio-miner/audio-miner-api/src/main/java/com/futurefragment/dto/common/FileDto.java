package com.futurefragment.dto.common;

public class FileDto {

	private String personS3Key;
	private String fileS3Key;
	private byte[] fileData;
	private FileType fileType;

	public FileDto() {
	}

	public String getPersonS3Key() {
		return personS3Key;
	}

	public void setPersonS3Key(String persons3Key) {
		this.personS3Key = persons3Key;
	}

	public String getFileS3Key() {
		return fileS3Key;
	}

	public void setFileS3Key(String fileS3Key) {
		this.fileS3Key = fileS3Key;
	}

	public byte[] getFileData() {
		return fileData;
	}

	public void setFileData(byte[] fileData) {
		this.fileData = fileData;
	}

	public FileType getFileType() {
		return fileType;
	}

	public void setFileType(FileType fileType) {
		this.fileType = fileType;
	}
}
