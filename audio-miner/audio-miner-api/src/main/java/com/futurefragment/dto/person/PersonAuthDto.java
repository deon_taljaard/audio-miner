package com.futurefragment.dto.person;

import java.io.Serializable;

import com.futurefragment.dto.common.AudioMinerDto;

public class PersonAuthDto implements Serializable, AudioMinerDto {

	private static final long serialVersionUID = 1L;

	private Long id;
	private String email;
	// use mutable array over immutable string - memory inspection
	private char[] password;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setPassword(char[] password) {
		this.password = password;
	}

	public char[] getPassword() {
		return password;
	}

}
