package com.futurefragment.dto.audio;

import java.io.Serializable;
import java.util.Date;

import com.futurefragment.dto.common.AudioMinerDto;

public class CutAudioDto implements Serializable, AudioMinerDto {

	private static final long serialVersionUID = 1L;

	private Long id;
	private String description;
	private String resourcePath;
	private Integer sequenceId;
	private Long audioId;
	private Date createdDate;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getResourcePath() {
		return resourcePath;
	}

	public void setResourcePath(String resourcePath) {
		this.resourcePath = resourcePath;
	}

	public Integer getSequenceId() {
		return sequenceId;
	}

	public void setSequenceId(Integer sequenceId) {
		this.sequenceId = sequenceId;
	}

	public Long getAudioId() {
		return audioId;
	}

	public void setAudioId(Long audioId) {
		this.audioId = audioId;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

}
