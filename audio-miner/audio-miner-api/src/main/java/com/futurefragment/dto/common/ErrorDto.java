package com.futurefragment.dto.common;

import java.io.Serializable;

public class ErrorDto implements Serializable, AudioMinerResponseDto {

	private static final long serialVersionUID = 1L;

	private String description;
	private String correction;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCorrection() {
		return correction;
	}

	public void setCorrection(String correction) {
		this.correction = correction;
	}

}
