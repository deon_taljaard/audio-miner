package com.futurefragment.dto.common;

public enum FileType {
	IMAGE, AUDIO;
}
