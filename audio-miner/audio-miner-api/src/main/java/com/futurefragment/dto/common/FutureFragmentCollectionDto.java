package com.futurefragment.dto.common;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class FutureFragmentCollectionDto<T extends AudioMinerDto> implements Serializable, AudioMinerResponseDto {

	private static final long serialVersionUID = 1L;

	private String resource;
	private List<T> data;

	public String getResource() {
		return resource;
	}

	public void setResource(String resource) {
		this.resource = resource;
	}

	public List<T> getData() {
		if (data == null) {
			data = new ArrayList<>();
		}
		return data;
	}

	public void setData(List<T> data) {
		this.data = data;
	}

}
