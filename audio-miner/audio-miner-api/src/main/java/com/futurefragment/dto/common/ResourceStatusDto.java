package com.futurefragment.dto.common;

import java.io.Serializable;
import java.util.Date;

public class ResourceStatusDto implements Serializable, AudioMinerDto {

	private static final long serialVersionUID = 1L;

	private Long id;
	private String resourceStatus;
	private Date createdDate;
	private Date updatedDate;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getResourceStatus() {
		return resourceStatus;
	}

	public void setResourceStatus(String description) {
		this.resourceStatus = description;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
}
