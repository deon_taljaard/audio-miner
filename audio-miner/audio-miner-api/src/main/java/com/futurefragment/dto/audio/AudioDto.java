package com.futurefragment.dto.audio;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.futurefragment.dto.common.AudioMinerUploadDto;
import com.futurefragment.dto.common.FileDto;

public class AudioDto implements Serializable, AudioMinerUploadDto {

	private static final long serialVersionUID = 1L;

	private Long id;
	private String description;
	private String resourcePath;
	private Date createdDate;
	private List<CutAudioDto> cutAudios;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getResourcePath() {
		return resourcePath;
	}

	public void setResourcePath(String resourcePath) {
		this.resourcePath = resourcePath;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public List<CutAudioDto> getCutAudios() {
		return cutAudios;
	}

	public void setCutAudios(List<CutAudioDto> cutAudios) {
		this.cutAudios = cutAudios;
	}

	@Override
	public void setFile(FileDto file) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public FileDto getFile() {
		// TODO Auto-generated method stub
		return null;
	}

}
