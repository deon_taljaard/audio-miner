package com.futurefragment.dto.common;

/**
 * Marker interface for DTOs that is composed of a <tt>FileDto</tt> object.
 */
public interface AudioMinerUploadDto extends AudioMinerDto {

	void setFile(FileDto file);

	FileDto getFile();
}
