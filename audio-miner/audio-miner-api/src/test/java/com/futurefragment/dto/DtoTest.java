package com.futurefragment.dto;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.StringContains.containsString;
import static org.junit.Assert.assertNotNull;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class DtoTest {

	protected String writeDtoToJsonString(Object value) throws JsonProcessingException {
		return new ObjectMapper().writer().withoutRootName().writeValueAsString(value);
	}

	protected void assertCommonJsonFields(String jsonString) {
		// assert
		assertNotNull(jsonString);
		// resource
		assertThat(jsonString, containsString("resource"));
		// Json root
		assertThat(jsonString, containsString("data"));
		assertThat(jsonString, containsString("id"));
	}


}
