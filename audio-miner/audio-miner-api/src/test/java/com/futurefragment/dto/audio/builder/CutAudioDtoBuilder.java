package com.futurefragment.dto.audio.builder;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.stream.IntStream;

import com.futurefragment.dto.audio.CutAudioDto;

public final class CutAudioDtoBuilder {

	private CutAudioDtoBuilder() {
	}

	public static List<CutAudioDto> getCutAudios() {
		List<CutAudioDto> cutAudios = new ArrayList<>();

		IntStream.range(1, 2).forEach(i -> cutAudios.add(createCutAudio(i)));

		return cutAudios;
	}

	public static CutAudioDto createCutAudio(int i) {
		CutAudioDto cutAudio = new CutAudioDto();

		cutAudio.setId(Long.valueOf(i));
		cutAudio.setDescription("Cut audio des " + i);
		cutAudio.setResourcePath("path/to/file");
		cutAudio.setSequenceId(1);
		cutAudio.setAudioId(1L);
		cutAudio.setCreatedDate(Calendar.getInstance().getTime());

		return cutAudio;
	}
}
