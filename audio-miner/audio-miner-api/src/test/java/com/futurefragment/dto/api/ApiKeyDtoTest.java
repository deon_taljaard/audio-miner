package com.futurefragment.dto.api;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.StringContains.containsString;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;

import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.futurefragment.dto.DtoTest;
import com.futurefragment.dto.api.ApiKeyDto;
import com.futurefragment.dto.api.builder.ApiKeyDtoBuilder;
import com.futurefragment.dto.common.FutureFragmentCollectionDto;
import com.futurefragment.util.FileReaderUtil;

public class ApiKeyDtoTest extends DtoTest {

	@Test
	public void shouldCorrectlyTransformApiKeyDtoToJsonString() throws JsonProcessingException {
		// arrange
		FutureFragmentCollectionDto<ApiKeyDto> apiKeys = new FutureFragmentCollectionDto<>();
		apiKeys.setResource("apiKeys");
		apiKeys.getData().addAll(ApiKeyDtoBuilder.getApiKeys());

		// act
		String jsonString = writeDtoToJsonString(apiKeys);

		// assert
		assertCommonJsonFields(jsonString);
		// ApiKey
		assertThat(jsonString, containsString("key"));
		assertThat(jsonString, containsString("description"));
		assertThat(jsonString, containsString("apiKeyResourceStatus"));
	}

	@Test
	public void shouldCorrectlyTransformJsonStringToApiKeyDto() throws JsonProcessingException, IOException {
		// arrange
		String jsonString = FileReaderUtil.getFileContent("dto/api/apiKeys.json");

		// act
		FutureFragmentCollectionDto<ApiKeyDto> apiKeys = new ObjectMapper()
				.readerFor(new TypeReference<FutureFragmentCollectionDto<ApiKeyDto>>() {
				}).readValue(jsonString);

		// assert
		assertNotNull(apiKeys);
		ApiKeyDto apiKey = apiKeys.getData().get(0);
		// ApiKey
		assertNotNull(apiKey.getId());
		assertNotNull(apiKey.getKey());
		assertNotNull(apiKey.getDescription());
		// ApiKey resource status
		assertNotNull(apiKey.getApiKeyResourceStatus());
	}

}
