package com.futurefragment.dto.common.builder;

import com.futurefragment.dto.common.DeleteResourceDto;

public class DeleteResourceDtoBuilder {

	private DeleteResourceDtoBuilder() {
	}

	public static DeleteResourceDto createDeleteResource() {
		DeleteResourceDto deleteResource = new DeleteResourceDto();
		deleteResource.setId(1L);
		deleteResource.setResource("persons");
		deleteResource.setSuccess(true);
		return deleteResource;
	}

}
