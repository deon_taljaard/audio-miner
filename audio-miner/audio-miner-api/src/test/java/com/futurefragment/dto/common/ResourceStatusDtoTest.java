package com.futurefragment.dto.common;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.StringContains.containsString;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;

import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.futurefragment.dto.DtoTest;
import com.futurefragment.dto.common.ResourceStatusDto;
import com.futurefragment.dto.common.FutureFragmentCollectionDto;
import com.futurefragment.dto.common.builder.ResourceStatusDtoBuilder;
import com.futurefragment.util.FileReaderUtil;

public class ResourceStatusDtoTest extends DtoTest {

	@Test
	public void shouldCorrectlyTransformVoiceResourceStatusDtoToJsonString() throws JsonProcessingException {
		// arrange
		FutureFragmentCollectionDto<ResourceStatusDto> resourceStatus = new FutureFragmentCollectionDto<>();
		resourceStatus.setResource("resourceStatus");
		resourceStatus.getData().addAll(ResourceStatusDtoBuilder.getResourceStatus());

		// act
		String jsonString = writeDtoToJsonString(resourceStatus);

		assertCommonJsonFields(jsonString);
		// resource status
		assertThat(jsonString, containsString("resourceStatus"));
		// Created date
		assertThat(jsonString, containsString("createdDate"));
		// Updated date
		assertThat(jsonString, containsString("updatedDate"));
	}

	@Test
	public void shouldCorrectlyTransformJsonStringToVoiceResourceStatusDto()
			throws JsonProcessingException, IOException {
		// arrange
		String jsonString = FileReaderUtil.getFileContent("dto/common/resourceStatus.json");

		// act
		FutureFragmentCollectionDto<ResourceStatusDto> resourceStatus = new ObjectMapper()
				.readerFor(new TypeReference<FutureFragmentCollectionDto<ResourceStatusDto>>() {
				}).readValue(jsonString);

		// assert
		assertNotNull(resourceStatus);
		ResourceStatusDto voiceState = resourceStatus.getData().get(0);
		// Voice
		assertNotNull(voiceState.getId());
		assertNotNull(voiceState.getResourceStatus());
	}
}
