package com.futurefragment.dto.common.builder;

import com.futurefragment.dto.common.ResourceCountDto;

public class ResourceCountDtoBuilder {

	private ResourceCountDtoBuilder() {
	}

	public static ResourceCountDto createResourceCount() {
		ResourceCountDto personResourceCount = new ResourceCountDto();
		personResourceCount.setCount(Long.valueOf(10));
		personResourceCount.setResource("persons");
		return personResourceCount;
	}
}
