package com.futurefragment.dto.common.builder;

import com.futurefragment.dto.common.ErrorDto;

public class ErrorDtoBuilder {

	private ErrorDtoBuilder() {
	}

	public static ErrorDto createError() {
		ErrorDto error = new ErrorDto();
		error.setCorrection("Correction message");
		error.setDescription("Description message");
		return error;
	}

}
