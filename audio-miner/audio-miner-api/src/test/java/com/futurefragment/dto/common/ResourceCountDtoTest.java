package com.futurefragment.dto.common;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.StringContains.containsString;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;

import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.futurefragment.dto.DtoTest;
import com.futurefragment.dto.common.ResourceCountDto;
import com.futurefragment.dto.common.builder.ResourceCountDtoBuilder;
import com.futurefragment.util.FileReaderUtil;

public class ResourceCountDtoTest extends DtoTest {

	@Test
	public void shouldCorrectlyTransformVoiceResourceCountDtoToJsonString() throws JsonProcessingException {
		// arrange
		ResourceCountDto resourceCount = ResourceCountDtoBuilder.createResourceCount();

		// act
		String jsonString = writeDtoToJsonString(resourceCount);

		// resource count
		assertThat(jsonString, containsString("count"));
		assertThat(jsonString, containsString("resource"));
	}

	@Test
	public void shouldCorrectlyTransformJsonStringToVoiceResourceCountDto()
			throws JsonProcessingException, IOException {
		// arrange
		String jsonString = FileReaderUtil.getFileContent("dto/common/resourceCount.json");

		// act
		ResourceCountDto resourceCount = new ObjectMapper().readerFor(new TypeReference<ResourceCountDto>() {
		}).readValue(jsonString);

		// assert
		assertNotNull(resourceCount);
		// Resource count
		assertNotNull(resourceCount.getCount());
		assertNotNull(resourceCount.getResource());
	}
}
