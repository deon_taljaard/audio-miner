package com.futurefragment.dto.common;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.StringContains.containsString;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;

import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.futurefragment.dto.DtoTest;
import com.futurefragment.dto.common.DeleteResourceDto;
import com.futurefragment.dto.common.builder.DeleteResourceDtoBuilder;
import com.futurefragment.util.FileReaderUtil;

public class DeleteResourceDtoTest extends DtoTest {

	@Test
	public void shouldCorrectlyTransformVoiceDeleteResourceDtoToJsonString() throws JsonProcessingException {
		// arrange
		DeleteResourceDto deleteResource = DeleteResourceDtoBuilder.createDeleteResource();

		// act
		String jsonString = writeDtoToJsonString(deleteResource);
		
		// DeleteResource
		assertThat(jsonString, containsString("id"));
		assertThat(jsonString, containsString("resource"));
		assertThat(jsonString, containsString("success"));
	}

	@Test
	public void shouldCorrectlyTransformJsonStringToVoiceDeleteResourceDto()
			throws JsonProcessingException, IOException {
		// arrange
		String jsonString = FileReaderUtil.getFileContent("dto/common/deleteResource.json");

		// act
		DeleteResourceDto deleteResource = new ObjectMapper().readerFor(new TypeReference<DeleteResourceDto>() {
		}).readValue(jsonString);

		// assert
		assertNotNull(deleteResource);
		// DeleteResource
		assertNotNull(deleteResource.getId());
		assertNotNull(deleteResource.getResource());
	}
}
