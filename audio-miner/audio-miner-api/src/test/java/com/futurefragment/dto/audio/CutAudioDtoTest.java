package com.futurefragment.dto.audio;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.StringContains.containsString;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;

import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.futurefragment.dto.DtoTest;
import com.futurefragment.dto.audio.CutAudioDto;
import com.futurefragment.dto.audio.builder.CutAudioDtoBuilder;
import com.futurefragment.dto.common.FutureFragmentCollectionDto;
import com.futurefragment.util.FileReaderUtil;

public class CutAudioDtoTest extends DtoTest {

	@Test
	public void shouldCorrectlyTransformCutAudioDtoToJsonString() throws JsonProcessingException {
		// arrange
		FutureFragmentCollectionDto<CutAudioDto> cutAudio = new FutureFragmentCollectionDto<>();
		cutAudio.setResource("cutAudios");
		cutAudio.getData().addAll(CutAudioDtoBuilder.getCutAudios());

		// act
		String jsonString = writeDtoToJsonString(cutAudio);
		
		assertCommonJsonFields(jsonString);
		assertThat(jsonString, containsString("description"));
		assertThat(jsonString, containsString("resourcePath"));
		assertThat(jsonString, containsString("createdDate"));
		assertThat(jsonString, containsString("sequenceId"));
		assertThat(jsonString, containsString("audioId"));
	}

	@Test
	public void shouldCorrectlyTransformJsonStringToCutAudioDto() throws JsonProcessingException, IOException {
		// arrange
		String jsonString = FileReaderUtil.getFileContent("dto/audio/cutAudios.json");

		// act
		FutureFragmentCollectionDto<CutAudioDto> genres = new ObjectMapper()
				.readerFor(new TypeReference<FutureFragmentCollectionDto<CutAudioDto>>() {
				}).readValue(jsonString);

		// assert
		assertNotNull(genres);
		CutAudioDto genre = genres.getData().get(0);
		assertNotNull(genre.getId());
		assertNotNull(genre.getDescription());
		assertNotNull(genre.getResourcePath());
		assertNotNull(genre.getCreatedDate());
		assertNotNull(genre.getSequenceId());
		assertNotNull(genre.getAudioId());
	}
}
