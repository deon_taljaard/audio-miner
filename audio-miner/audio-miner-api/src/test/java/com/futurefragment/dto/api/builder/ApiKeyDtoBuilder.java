package com.futurefragment.dto.api.builder;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.IntStream;

import com.futurefragment.dto.api.ApiKeyDto;
import com.futurefragment.dto.common.builder.ResourceStatusDtoBuilder;

public final class ApiKeyDtoBuilder {

	private ApiKeyDtoBuilder() {
	}

	public static List<ApiKeyDto> getApiKeys() {
		List<ApiKeyDto> apiKeys = new ArrayList<>();

		IntStream.range(1, 2).forEach(i -> apiKeys.add(createApiKey(i)));

		return apiKeys;
	}

	public static ApiKeyDto createApiKey(int i) {
		ApiKeyDto apiKey = new ApiKeyDto();

		apiKey.setId(Long.valueOf(i));
		apiKey.setKey(UUID.randomUUID().toString());
		apiKey.setDescription("Mobile Client 1 Key");
		apiKey.setApiKeyResourceStatus(ResourceStatusDtoBuilder.createResourceStatus(i));

		return apiKey;
	}
}
