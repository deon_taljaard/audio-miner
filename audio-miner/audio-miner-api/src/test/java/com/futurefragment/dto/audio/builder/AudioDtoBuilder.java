package com.futurefragment.dto.audio.builder;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.stream.IntStream;

import com.futurefragment.dto.audio.AudioDto;

public final class AudioDtoBuilder {

	private List<AudioDto> audios;

	public enum AudioProperties {
		CUT_AUDIO;
	}

	private AudioDtoBuilder(List<AudioDto> audios) {
		this.audios = audios;
	}

	public static AudioDtoBuilder with(List<AudioProperties> audioProperties) {
		List<AudioDto> audios = new ArrayList<>();

		AudioDtoBuilder audioDtoBuilder = new AudioDtoBuilder(audios);

		IntStream.range(1, 2).forEach(i -> audios.add(singleAudio(i, audioProperties)));

		return audioDtoBuilder;
	}

	public static AudioDto singleAudio(int i, List<AudioProperties> audioProperties) {
		AudioDto audio = new AudioDto();

		audio.setId(Long.valueOf(i));
		audio.setDescription("Audio desc " + i);
		audio.setResourcePath("path/to/file");
		audio.setCreatedDate(Calendar.getInstance().getTime());
		if (audioProperties.contains(AudioProperties.CUT_AUDIO)) {
			audio.setCutAudios(CutAudioDtoBuilder.getCutAudios());
		}

		return audio;
	}

	public List<AudioDto> audios() {
		return this.audios;
	}
}
