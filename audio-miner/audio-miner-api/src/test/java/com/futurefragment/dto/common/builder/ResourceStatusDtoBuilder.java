package com.futurefragment.dto.common.builder;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

import com.futurefragment.dto.common.ResourceStatusDto;

public final class ResourceStatusDtoBuilder {

	private ResourceStatusDtoBuilder() {
	}

	public static List<ResourceStatusDto> getResourceStatus() {
		List<ResourceStatusDto> resourceStatus = new ArrayList<>();

		IntStream.range(1, 2).forEach(i -> resourceStatus.add(createResourceStatus(i)));

		return resourceStatus;
	}

	public static ResourceStatusDto createResourceStatus(int i) {
		ResourceStatusDto resourceStatus = new ResourceStatusDto();
		resourceStatus.setId(Long.valueOf(i));
		resourceStatus.setResourceStatus("active");
		return resourceStatus;
	}
}
