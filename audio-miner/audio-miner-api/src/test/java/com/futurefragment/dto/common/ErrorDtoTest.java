package com.futurefragment.dto.common;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.StringContains.containsString;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;

import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.futurefragment.dto.DtoTest;
import com.futurefragment.dto.common.ErrorDto;
import com.futurefragment.dto.common.builder.ErrorDtoBuilder;
import com.futurefragment.util.FileReaderUtil;

public class ErrorDtoTest extends DtoTest {

	@Test
	public void shouldCorrectlyTransformVoiceErrorDtoToJsonString() throws JsonProcessingException {
		// arrange
		ErrorDto error = ErrorDtoBuilder.createError();

		// act
		String jsonString = writeDtoToJsonString(error);

		// Error
		assertThat(jsonString, containsString("description"));
		assertThat(jsonString, containsString("correction"));
	}

	@Test
	public void shouldCorrectlyTransformJsonStringToVoiceErrorDto() throws JsonProcessingException, IOException {
		// arrange
		String jsonString = FileReaderUtil.getFileContent("dto/common/error.json");

		// act
		ErrorDto error = new ObjectMapper().readerFor(new TypeReference<ErrorDto>() {
		}).readValue(jsonString);

		// assert
		assertNotNull(error);
		// Error
		assertNotNull(error.getDescription());
		assertNotNull(error.getCorrection());
	}
}
