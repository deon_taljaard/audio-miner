package com.futurefragment.dto.audio;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.StringContains.containsString;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.util.Arrays;

import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.futurefragment.dto.DtoTest;
import com.futurefragment.dto.audio.builder.AudioDtoBuilder;
import com.futurefragment.dto.audio.builder.AudioDtoBuilder.AudioProperties;
import com.futurefragment.dto.common.FutureFragmentCollectionDto;
import com.futurefragment.util.FileReaderUtil;

public class AudioDtoTest extends DtoTest {

	@Test
	public void shouldCorrectlyTransformAudioDtoToJsonString() throws JsonProcessingException {
		// arrange
		FutureFragmentCollectionDto<AudioDto> audios = new FutureFragmentCollectionDto<>();
		audios.setResource("audios");
		audios.getData().addAll(AudioDtoBuilder.with(Arrays.asList(AudioProperties.CUT_AUDIO)).audios());

		// act
		String jsonString = writeDtoToJsonString(audios);

		assertCommonJsonFields(jsonString);
		assertThat(jsonString, containsString("description"));
		assertThat(jsonString, containsString("resourcePath"));
		assertThat(jsonString, containsString("createdDate"));
		assertThat(jsonString, containsString("cutAudios"));
	}

	@Test
	public void shouldCorrectlyTransformJsonStringToAudioDto() throws JsonProcessingException, IOException {
		// arrange
		String jsonString = FileReaderUtil.getFileContent("dto/audio/audios.json");

		// act
		FutureFragmentCollectionDto<AudioDto> audios = new ObjectMapper()
				.readerFor(new TypeReference<FutureFragmentCollectionDto<AudioDto>>() {
				}).readValue(jsonString);

		// assert
		assertNotNull(audios);
		AudioDto audio = audios.getData().get(0);
		assertNotNull(audio.getId());
		assertNotNull(audio.getDescription());
		assertNotNull(audio.getResourcePath());
		assertNotNull(audio.getCreatedDate());
		assertNotNull(audio.getCutAudios());
	}
}
