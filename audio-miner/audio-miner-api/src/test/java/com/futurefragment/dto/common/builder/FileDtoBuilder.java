package com.futurefragment.dto.common.builder;

import java.util.UUID;

import com.futurefragment.dto.common.FileDto;
import com.futurefragment.dto.common.FileType;

public class FileDtoBuilder {

	private FileDtoBuilder() {
	}

	public static FileDto createFileDto() {
		FileDto fileDto = new FileDto();
		fileDto.setFileData(new byte[] {});
		fileDto.setFileS3Key(UUID.randomUUID().toString());
		fileDto.setPersonS3Key(UUID.randomUUID().toString());
		fileDto.setFileType(FileType.AUDIO);
		return fileDto;
	}
}
