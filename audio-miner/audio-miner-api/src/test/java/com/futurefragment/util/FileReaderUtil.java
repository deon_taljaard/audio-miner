package com.futurefragment.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public final class FileReaderUtil {

	private FileReaderUtil() {}
	
	public static String getFileContent(final String fileName) {
		try {
			InputStream inputStream = FileReaderUtil.class.getClassLoader().getResourceAsStream(fileName);

			final BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
			final StringBuilder result = new StringBuilder();
			String line = null;
			try {
				while ((line = reader.readLine()) != null) {
					result.append(line);
				}
			} catch (IOException e) {
				result.append(e.getMessage());
			} finally {
				reader.close();
				inputStream.close();
			}

			return result.toString();
		} catch (Exception ex) {
			return String.format("%s:%s", ex.getMessage(), ex.getCause());
		}
	}

}
