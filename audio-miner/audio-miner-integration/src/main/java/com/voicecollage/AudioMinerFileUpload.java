package com.voicecollage;

public interface AudioMinerFileUpload {

	void uploadFile(byte[] fileData, String resourceKey);

	void deleteObject(String resourceKey);
}
