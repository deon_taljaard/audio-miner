package com.futurefragment.aws;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import javax.ejb.Stateless;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.model.DeleteObjectRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.futurefragment.config.AudioMinerConfig;
import com.futurefragment.constant.ExceptionConstants;
import com.futurefragment.exception.ExceptionContext;
import com.futurefragment.exception.Severity;
import com.futurefragment.exception.AudioMinerException;
import com.futurefragment.exception.AudioMinerExceptionFactory;
import com.voicecollage.AudioMinerFileUpload;

@Stateless
public class S3FileUpload implements AudioMinerFileUpload {

	private static final String CLASS_NAME = S3FileUpload.class.getSimpleName();

	private static final String METHOD_HANDLE_PUT_OBJ = "handlePutObject";

	private static final String ERR_AWS_SERVICE_EXCEPTION_DESCRIPTION = "The file upload request made it to Amazon S3, but was rejected with an error response for some reason.";
	private static final String ERR_AWS_SERVICE_EXCEPTION_CORRECTION = "Look at the exception paramters to fix the issue.";
	private static final String ERR_AWS_CLIENT_EXCEPTION_DESCRIPTION = "The Amazon S3 client encountered an internal error while trying to communicate with S3, such as not being able to access the network.";
	private static final String ERR_AWS_CLIENT_EXCEPTION_CORRECTION = "Ensure the host machine has network access.";
	private static final String ERR_UNKNOWN_AWS_EXCEPTION_DESCRIPTION = "An unknown error occurred while communicating with AWS.";

	// TODO: Not necessary to create a folder, only upload the object.

	@Override
	public void uploadFile(byte[] fileData, String resourceKey) {
		InputStream fileContent = new ByteArrayInputStream(fileData);

		ObjectMetadata metadata = new ObjectMetadata();
		metadata.setContentLength(fileData.length);

		handlePutObject(new PutObjectRequest(AudioMinerConfig.AWS_S3_BUCKET, resourceKey, fileContent, metadata));
	}

	@Override
	public void deleteObject(String resourceKey) {
		try {
			S3Client.getInstance().deleteObject(new DeleteObjectRequest(AudioMinerConfig.AWS_S3_BUCKET, resourceKey));
		} catch (Exception ex) {
			handleAwsExceptions(ex);
		}
	}

	private void handlePutObject(PutObjectRequest putObjectRequest) {
		try {
			S3Client.getInstance().putObject(putObjectRequest);
		} catch (Exception ex) {
			handleAwsExceptions(ex);
		}
	}

	private void handleAwsExceptions(Exception ex) {
		if (ex instanceof AmazonServiceException) {
			AmazonServiceException asEx = (AmazonServiceException) ex;
			Map<String, Object> parameters = new HashMap<>();

			parameters.put("HTTP Status Code", asEx.getStatusCode());
			parameters.put("AWS Error Code", asEx.getErrorCode());
			parameters.put("Error Type", asEx.getErrorType());
			parameters.put("Request ID", asEx.getRequestId());

			throw createVoiceCollageExceptionForS3Exception(ex, parameters, ERR_AWS_SERVICE_EXCEPTION_DESCRIPTION,
					ERR_AWS_SERVICE_EXCEPTION_CORRECTION);
		} else if (ex instanceof AmazonClientException) {
			throw createVoiceCollageExceptionForS3Exception(ex, new HashMap<>(), ERR_AWS_CLIENT_EXCEPTION_DESCRIPTION,
					ERR_AWS_CLIENT_EXCEPTION_CORRECTION);
		} else {
			throw createVoiceCollageExceptionForS3Exception(ex, new HashMap<>(), ERR_UNKNOWN_AWS_EXCEPTION_DESCRIPTION,
					ExceptionConstants.ERR_CONTACT_ADMIN_USER_CORRECTION);
		}
	}

	private AudioMinerException createVoiceCollageExceptionForS3Exception(Exception ex,
			Map<String, Object> parameters, String adminErrorDescription, String adminErrorCorrection) {
		//@formatter:off
		
		ExceptionContext exceptionContext = ExceptionContext.getInstance(ExceptionConstants.APPLICATION_LAYER_INTEGRATION, CLASS_NAME, METHOD_HANDLE_PUT_OBJ, ex)
				.setSeverity(Severity.ERROR)
				.setParameters(parameters)
				.setAdminErrorDescription(adminErrorDescription)
				.setAdminErrorCorrection(adminErrorCorrection)
				.setUserErrorDescription(adminErrorDescription)
				.setUserErrorCorrection(ExceptionConstants.ERR_CONTACT_ADMIN_USER_CORRECTION);
		
		//@formatter:on

		return AudioMinerExceptionFactory.raiseException(exceptionContext);
	}

}
