package com.futurefragment.aws;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.futurefragment.config.AudioMinerConfig;

public final class S3Client {

	private S3Client() {
	}

	public static AmazonS3 getInstance() {
		// @formatter:off
		return AmazonS3ClientBuilder.standard()
				.withRegion(Regions.EU_WEST_1)
				.withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(AudioMinerConfig.AWS_S3_ACCESS_KEY_ID, AudioMinerConfig.AWS_S3_SECRET_KEY_ID)))
				.build();
		// @formatter:on
	}

}
