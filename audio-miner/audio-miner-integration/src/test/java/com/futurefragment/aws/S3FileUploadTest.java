package com.futurefragment.aws;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNotNull;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.amazonaws.services.s3.model.DeleteObjectRequest;
import com.amazonaws.services.s3.model.ListObjectsV2Request;
import com.amazonaws.services.s3.model.ListObjectsV2Result;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.futurefragment.config.AudioMinerConfig;

/**
 * Unfortunately, this test case can only be run when there is a stable Internet
 * connection, hence the tests are ignored and run manually for now.
 */

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class S3FileUploadTest {

	private static final String TEST_IMAGE_FILE = "src/test/resources/testImage.png";
	private static final String RESOURCE_KEY = "path/to/testImage.png";

	@Ignore
	@Test
	public void t1ShouldUploadObjectToS3() throws IOException {
		// Arrange
		Path testObject = new File(TEST_IMAGE_FILE).toPath();
		byte[] fileData = Files.readAllBytes(testObject);

		InputStream fileContent = new ByteArrayInputStream(fileData);

		ObjectMetadata metadata = new ObjectMetadata();
		metadata.setContentLength(fileData.length);

		PutObjectRequest putObjectRequest = new PutObjectRequest(AudioMinerConfig.AWS_S3_BUCKET, RESOURCE_KEY,
				fileContent, metadata);

		// Act
		PutObjectResult result = S3Client.getInstance().putObject(putObjectRequest);

		// Assert
		assertNotNull(result);
		assertNotNull(result.getContentMd5());
	}

	@Ignore
	@Test
	public void t2ShouldGetObjectFromS3() {
		// Act
		List<S3ObjectSummary> objectSummaries = getObjectSummariesForBucket();

		// Assert
		List<S3ObjectSummary> filter = objectSummaries.stream().filter(s -> RESOURCE_KEY.equals(s.getKey()))
				.collect(Collectors.toList());
		assertThat(filter.isEmpty(), is(false));
	}

	@Ignore
	@Test
	public void t3ShouldDeleteObjectFromS3() {
		// Arrange
		DeleteObjectRequest deleteObjectRequest = new DeleteObjectRequest(AudioMinerConfig.AWS_S3_BUCKET,
				RESOURCE_KEY);

		// Act
		S3Client.getInstance().deleteObject(deleteObjectRequest);

		// Assert
		List<S3ObjectSummary> objectSummaries = getObjectSummariesForBucket();
		List<S3ObjectSummary> filter = objectSummaries.stream().filter(s -> RESOURCE_KEY.equals(s.getKey()))
				.collect(Collectors.toList());
		assertThat(filter.isEmpty(), is(true));
	}

	private List<S3ObjectSummary> getObjectSummariesForBucket() {
		// Arrange
		ListObjectsV2Request listObjectsV2Request = new ListObjectsV2Request()
				.withBucketName(AudioMinerConfig.AWS_S3_BUCKET);

		// Act
		ListObjectsV2Result listObjectsV2 = S3Client.getInstance().listObjectsV2(listObjectsV2Request);

		return listObjectsV2.getObjectSummaries();
	}
}
