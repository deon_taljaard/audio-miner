package com.futurefragment.api;

import java.util.List;

import com.futurefragment.dto.common.ResourceStatusDto;

public interface ApiKeyResourceStatusManager {

	ResourceStatusDto getApiKeyResourceStatusByApiKeyId(Long id);

	List<ResourceStatusDto> updateApiKeyResourceStatus(List<ResourceStatusDto> resourceResourceStatus);
}
