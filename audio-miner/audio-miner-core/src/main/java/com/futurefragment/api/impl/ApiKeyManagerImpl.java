package com.futurefragment.api.impl;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.ejb.Stateless;
import javax.inject.Inject;

import com.futurefragment.api.ApiKeyManager;
import com.futurefragment.api.dao.ApiKeyDao;
import com.futurefragment.api.entity.ApiKey;
import com.futurefragment.api.mapper.ApiKeyMapper;
import com.futurefragment.dto.api.ApiKeyDto;

@Stateless
public class ApiKeyManagerImpl implements ApiKeyManager {

	@Inject
	private ApiKeyDao apiKeyDao;

	// Functions
	private final Function<ApiKey, ApiKey> CREATE_API_KEY_FUNCTION = (apiKey) -> apiKeyDao.create(apiKey);
	private final Function<ApiKey, ApiKey> UPDATE_API_KEY_FUNCTION = (apiKey) -> apiKeyDao.update(apiKey);

	@Override
	public ApiKeyDto getApiKeyById(Long id) {
		return new ApiKeyMapper().mapEntityToDto(apiKeyDao.findById(id));
	}

	@Override
	public Optional<ApiKeyDto> getApiKeyByKey(String key) {
		return apiKeyDao.findApiKeyByKey(key).map(new ApiKeyMapper()::mapEntityToDto);
	}

	@Override
	public List<ApiKeyDto> updateApiKeys(List<ApiKeyDto> apiKeys) {
		return createOrUpdateApiKeys(apiKeys, UPDATE_API_KEY_FUNCTION);
	}

	@Override
	public List<ApiKeyDto> createApiKeys(List<ApiKeyDto> apiKeys) {
		return createOrUpdateApiKeys(apiKeys, CREATE_API_KEY_FUNCTION);
	}

	// TODO: find a cleaner way to handle deletes
	@Override
	public boolean deleteApiKey(Long id) {
		apiKeyDao.delete(id);

		return true;
	}

	private List<ApiKeyDto> createOrUpdateApiKeys(List<ApiKeyDto> apiKeys,
			Function<ApiKey, ApiKey> createOrUpdateFunction) {
		ApiKeyMapper apiKeyMapper = new ApiKeyMapper();

		List<ApiKey> apiKeysToUpdate = apiKeys.stream().map(apiKeyMapper::mapDtoToEntity).collect(Collectors.toList());

		List<ApiKey> updatedEntities = apiKeysToUpdate.stream().map(createOrUpdateFunction::apply)
				.collect(Collectors.toList());

		return updatedEntities.stream().map(apiKeyMapper::mapEntityToDto).collect(Collectors.toList());
	}
}
