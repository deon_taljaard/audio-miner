package com.futurefragment.api.mapper;

import com.futurefragment.api.entity.ApiKeyResourceStatusLookup;
import com.futurefragment.common.MapDtoToEntity;
import com.futurefragment.common.MapEntityToDto;
import com.futurefragment.common.ResourceStatusMapper;
import com.futurefragment.dto.common.ResourceStatusDto;

public class ApiKeyResourceStatusMapper implements MapDtoToEntity<ResourceStatusDto, ApiKeyResourceStatusLookup>,
		MapEntityToDto<ApiKeyResourceStatusLookup, ResourceStatusDto> {

	@Override
	public ResourceStatusDto mapEntityToDto(ApiKeyResourceStatusLookup entity) {
		ResourceStatusDto apiKeyResourceStatus = new ResourceStatusDto();

		apiKeyResourceStatus.setId(entity.getId());
		apiKeyResourceStatus.setCreatedDate(entity.getCreatedDate());
		apiKeyResourceStatus.setUpdatedDate(entity.getUpdatedDate());

		// resource status
		apiKeyResourceStatus.setResourceStatus(entity.getResourceStatus().getDescription());

		return apiKeyResourceStatus;
	}

	@Override
	public ApiKeyResourceStatusLookup mapDtoToEntity(ResourceStatusDto dto) {
		ApiKeyResourceStatusLookup apiKeyResourceStatus = new ApiKeyResourceStatusLookup();

		apiKeyResourceStatus.setId(dto.getId());

		// resource status
		apiKeyResourceStatus.setResourceStatus(
				new ResourceStatusMapper().mapResourceStatusToResourceStatusEntity(dto.getResourceStatus()));

		return apiKeyResourceStatus;
	}
}
