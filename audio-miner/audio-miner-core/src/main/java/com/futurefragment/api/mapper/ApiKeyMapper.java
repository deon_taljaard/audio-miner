package com.futurefragment.api.mapper;

import com.futurefragment.api.entity.ApiKey;
import com.futurefragment.common.MapDtoToEntity;
import com.futurefragment.common.MapEntityToDto;
import com.futurefragment.dto.api.ApiKeyDto;

public class ApiKeyMapper implements MapDtoToEntity<ApiKeyDto, ApiKey>, MapEntityToDto<ApiKey, ApiKeyDto> {

	@Override
	public ApiKeyDto mapEntityToDto(ApiKey entity) {
		ApiKeyDto apiKey = new ApiKeyDto();

		// apiKey properties
		apiKey.setId(entity.getId());
		apiKey.setKey(entity.getKey());
		apiKey.setDescription(entity.getDescription());

		// apiKey resource status
		apiKey.setApiKeyResourceStatus(
				new ApiKeyResourceStatusMapper().mapEntityToDto(entity.getApiKeyResourceStatus()));

		return apiKey;
	}

	@Override
	public ApiKey mapDtoToEntity(ApiKeyDto dto) {
		ApiKey apiKey = new ApiKey();

		// apiKey properties
		apiKey.setId(dto.getId());
		apiKey.setDescription(dto.getDescription());

		// apiKey resource status
		apiKey.setApiKeyResourceStatus(new ApiKeyResourceStatusMapper().mapDtoToEntity(dto.getApiKeyResourceStatus()));

		// add apiKey to apiKey resource status in order for jpa to map the
		// id from apiKey to apiKey resource status in the one-to-one
		// relationship
		apiKey.getApiKeyResourceStatus().setApiKey(apiKey);

		return apiKey;
	}

}
