package com.futurefragment.api;

import java.util.List;
import java.util.Optional;

import com.futurefragment.dto.api.ApiKeyDto;

public interface ApiKeyManager {

	ApiKeyDto getApiKeyById(Long id);

	Optional<ApiKeyDto> getApiKeyByKey(String key);

	List<ApiKeyDto> updateApiKeys(List<ApiKeyDto> apiKeys);

	List<ApiKeyDto> createApiKeys(List<ApiKeyDto> apiKeys);

	boolean deleteApiKey(Long id);

}
