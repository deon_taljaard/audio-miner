package com.futurefragment.api.impl;

import java.util.List;
import java.util.stream.Collectors;

import javax.ejb.Stateless;
import javax.inject.Inject;

import com.futurefragment.api.ApiKeyResourceStatusManager;
import com.futurefragment.api.dao.ApiKeyResourceStatusLookupDao;
import com.futurefragment.api.entity.ApiKeyResourceStatusLookup;
import com.futurefragment.api.mapper.ApiKeyResourceStatusMapper;
import com.futurefragment.dto.common.ResourceStatusDto;

@Stateless
public class ApiKeyResourceStatusManagerImpl implements ApiKeyResourceStatusManager {

	@Inject
	private ApiKeyResourceStatusLookupDao apiKeyResourceStatusLookupDao;

	@Override
	public ResourceStatusDto getApiKeyResourceStatusByApiKeyId(Long id) {
		return new ApiKeyResourceStatusMapper().mapEntityToDto(apiKeyResourceStatusLookupDao.findById(id));
	}

	@Override
	public List<ResourceStatusDto> updateApiKeyResourceStatus(List<ResourceStatusDto> apiKeyResourceStatus) {
		ApiKeyResourceStatusMapper apiKeyResourceStatusMapper = new ApiKeyResourceStatusMapper();

		List<ApiKeyResourceStatusLookup> apiKeyResourceStatusesToUpdate = apiKeyResourceStatus.stream()
				.map(apiKeyResourceStatusMapper::mapDtoToEntity).collect(Collectors.toList());

		List<ApiKeyResourceStatusLookup> updatedEntities = apiKeyResourceStatusesToUpdate.stream()
				.map(apiKeyResourceStatusLookupDao::update).collect(Collectors.toList());

		return updatedEntities.stream().map(apiKeyResourceStatusMapper::mapEntityToDto).collect(Collectors.toList());
	}

}
