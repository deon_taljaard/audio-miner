package com.futurefragment.common;

import com.futurefragment.common.entity.ResourceStatus;
import com.futurefragment.enumlookups.ResourceStatusLookup;

public final class ResourceStatusMapper {

	public ResourceStatus mapResourceStatusToResourceStatusEntity(String status) {
		ResourceStatus resourceStatus = new ResourceStatus();

		ResourceStatusLookup resourceStatusLookup = ResourceStatusLookup.valueOf(status.toUpperCase());
		resourceStatus.setId(resourceStatusLookup.getId());
		resourceStatus.setDescription(resourceStatusLookup.name().toLowerCase());

		return resourceStatus;
	}

}
