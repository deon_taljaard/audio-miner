package com.futurefragment.common;

public interface MapDtoToEntity<T, K> {

	K mapDtoToEntity(T dto);
}
