package com.futurefragment.common;

public interface MapEntityToDto<T, K> {

	K mapEntityToDto(T entity);
}
