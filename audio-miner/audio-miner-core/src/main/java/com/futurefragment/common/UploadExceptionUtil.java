package com.futurefragment.common;

import java.util.Map;

import com.futurefragment.constant.ExceptionConstants;
import com.futurefragment.exception.ExceptionContext;
import com.futurefragment.exception.Severity;
import com.futurefragment.exception.AudioMinerException;
import com.futurefragment.exception.AudioMinerExceptionFactory;

public final class UploadExceptionUtil {

	private UploadExceptionUtil() {
	}

	public static AudioMinerException createVoiceCollageExceptionForUploadException(final String className,
			final String methodName, final Exception ex, final Map<String, Object> parameters,
			final String adminErrorDescription, final String adminErrorCorrection) {
		//@formatter:off
		
		ExceptionContext exceptionContext = ExceptionContext.getInstance(ExceptionConstants.APPLICATION_LAYER_INTEGRATION, className, methodName, ex)
				.setSeverity(Severity.ERROR)
				.setParameters(parameters)
				.setAdminErrorDescription(adminErrorDescription)
				.setAdminErrorCorrection(adminErrorCorrection)
				.setUserErrorDescription(adminErrorDescription)
				.setUserErrorCorrection(ExceptionConstants.ERR_CONTACT_ADMIN_USER_CORRECTION);
		
		//@formatter:on

		return AudioMinerExceptionFactory.raiseException(exceptionContext);
	}
}
