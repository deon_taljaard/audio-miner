package com.futurefragment.audio.mapper;

import java.util.stream.Collectors;

import com.futurefragment.audio.entity.Audio;
import com.futurefragment.common.MapDtoToEntity;
import com.futurefragment.common.MapEntityToDto;
import com.futurefragment.dto.audio.AudioDto;

public final class AudioMapper implements MapDtoToEntity<AudioDto, Audio>, MapEntityToDto<Audio, AudioDto> {

	@Override
	public AudioDto mapEntityToDto(Audio entity) {
		AudioDto audio = new AudioDto();

		audio.setId(entity.getId());
		audio.setDescription(entity.getDescription());
		audio.setResourcePath(entity.getResourcePath());
		audio.setCreatedDate(entity.getCreatedDate());

		CutAudioMapper cutAudioMapper = new CutAudioMapper();
		audio.setCutAudios(
				entity.getCutAudios().stream().map(cutAudioMapper::mapEntityToDto).collect(Collectors.toList()));

		return audio;
	}

	@Override
	public Audio mapDtoToEntity(AudioDto dto) {
		Audio audio = new Audio();

		audio.setId(dto.getId());
		audio.setDescription(dto.getDescription());
		audio.setResourcePath(dto.getResourcePath());
		audio.setCreatedDate(dto.getCreatedDate());

		CutAudioMapper cutAudioMapper = new CutAudioMapper();
		if (dto.getCutAudios() != null) {
			audio.setCutAudios(
					dto.getCutAudios().stream().map(cutAudioMapper::mapDtoToEntity).collect(Collectors.toList()));
		}

		return audio;
	}

}
