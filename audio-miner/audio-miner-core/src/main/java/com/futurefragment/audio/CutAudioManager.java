package com.futurefragment.audio;

import java.util.List;
import java.util.Optional;

import com.futurefragment.dto.audio.CutAudioDto;

public interface CutAudioManager {

	CutAudioDto getCutAudioById(Long id);

	Optional<CutAudioDto> getCutAudioByName(String description);

	List<CutAudioDto> updateCutAudios(List<CutAudioDto> cutAudios);

	List<CutAudioDto> createCutAudios(List<CutAudioDto> cutAudios);

	boolean deleteCutAudio(Long id);
}
