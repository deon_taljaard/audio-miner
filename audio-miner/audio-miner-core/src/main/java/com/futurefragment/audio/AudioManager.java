package com.futurefragment.audio;

import java.util.List;

import com.futurefragment.dto.audio.AudioDto;

public interface AudioManager {

	AudioDto getAudioById(Long id);

	List<AudioDto> updateAudios(List<AudioDto> audios);

	List<AudioDto> createAudios(List<AudioDto> audios);

	boolean deleteAudio(Long id);
}
