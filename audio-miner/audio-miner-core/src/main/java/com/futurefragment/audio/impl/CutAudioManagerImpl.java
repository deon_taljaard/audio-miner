package com.futurefragment.audio.impl;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.ejb.Stateless;
import javax.inject.Inject;

import com.futurefragment.audio.CutAudioManager;
import com.futurefragment.audio.dao.CutAudioDao;
import com.futurefragment.audio.entity.CutAudio;
import com.futurefragment.audio.mapper.CutAudioMapper;
import com.futurefragment.dto.audio.CutAudioDto;

@Stateless
public class CutAudioManagerImpl implements CutAudioManager {

	@Inject
	private CutAudioDao cutAudioDao;

	// Functions
	private final Function<CutAudio, CutAudio> CREATE_CUT_AUDIO_FUNCTION = (audio) -> cutAudioDao.create(audio);
	private final Function<CutAudio, CutAudio> UPDATE_CUT_AUDIO_FUNCTION = (audio) -> cutAudioDao.update(audio);

	@Override
	public CutAudioDto getCutAudioById(Long id) {
		return new CutAudioMapper().mapEntityToDto(cutAudioDao.findById(id));
	}

	@Override
	public Optional<CutAudioDto> getCutAudioByName(String description) {
		return cutAudioDao.findCutAudioByName(description).map(new CutAudioMapper()::mapEntityToDto);
	}

	@Override
	public List<CutAudioDto> updateCutAudios(List<CutAudioDto> cutAudios) {
		return createOrUpdateCutAudios(cutAudios, UPDATE_CUT_AUDIO_FUNCTION);
	}

	@Override
	public List<CutAudioDto> createCutAudios(List<CutAudioDto> cutAudios) {
		return createOrUpdateCutAudios(cutAudios, CREATE_CUT_AUDIO_FUNCTION);
	}

	private List<CutAudioDto> createOrUpdateCutAudios(List<CutAudioDto> CutAudios,
			Function<CutAudio, CutAudio> createOrUpdateCutAudioFunction) {
		CutAudioMapper CutAudioMapper = new CutAudioMapper();

		List<CutAudio> CutAudioToUpdate = CutAudios.stream().map(CutAudioMapper::mapDtoToEntity)
				.collect(Collectors.toList());

		List<CutAudio> updatedEntities = CutAudioToUpdate.stream().map(createOrUpdateCutAudioFunction::apply)
				.collect(Collectors.toList());

		return updatedEntities.stream().map(CutAudioMapper::mapEntityToDto).collect(Collectors.toList());
	}

	@Override
	public boolean deleteCutAudio(Long id) {
		cutAudioDao.delete(id);

		return true;
	}

}
