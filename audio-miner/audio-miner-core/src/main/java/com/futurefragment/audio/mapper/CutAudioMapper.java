package com.futurefragment.audio.mapper;

import com.futurefragment.audio.entity.Audio;
import com.futurefragment.audio.entity.CutAudio;
import com.futurefragment.common.MapDtoToEntity;
import com.futurefragment.common.MapEntityToDto;
import com.futurefragment.dto.audio.CutAudioDto;

public class CutAudioMapper implements MapDtoToEntity<CutAudioDto, CutAudio>, MapEntityToDto<CutAudio, CutAudioDto> {

	@Override
	public CutAudioDto mapEntityToDto(CutAudio entity) {
		CutAudioDto cutAudio = new CutAudioDto();

		cutAudio.setId(entity.getId());
		cutAudio.setDescription(entity.getDescription());
		cutAudio.setResourcePath(entity.getResourcePath());
		cutAudio.setAudioId(entity.getAudio().getId());
		cutAudio.setSequenceId(entity.getSequenceId());
		cutAudio.setCreatedDate(entity.getCreatedDate());

		return cutAudio;
	}

	@Override
	public CutAudio mapDtoToEntity(CutAudioDto dto) {
		CutAudio cutAudio = new CutAudio();

		cutAudio.setId(dto.getId());
		cutAudio.setDescription(dto.getDescription());
		cutAudio.setResourcePath(dto.getResourcePath());

		Audio audio = new Audio();
		audio.setId(dto.getAudioId());
		cutAudio.setAudio(audio);

		cutAudio.setSequenceId(dto.getSequenceId());
		cutAudio.setCreatedDate(dto.getCreatedDate());

		return cutAudio;
	}

}
