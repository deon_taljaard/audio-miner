package com.futurefragment.audio.impl;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.ejb.Stateless;
import javax.inject.Inject;

import com.futurefragment.audio.AudioManager;
import com.futurefragment.audio.dao.AudioDao;
import com.futurefragment.audio.entity.Audio;
import com.futurefragment.audio.mapper.AudioMapper;
import com.futurefragment.dto.audio.AudioDto;

@Stateless
public class AudioManagerImpl implements AudioManager {

	@Inject
	private AudioDao AudioDao;

	// Functions
	private final Function<Audio, Audio> CREATE_AUDIO_FUNCTION = (audio) -> AudioDao.create(audio);
	private final Function<Audio, Audio> UPDATE_AUDIO_FUNCTION = (audio) -> AudioDao.update(audio);

	@Override
	public AudioDto getAudioById(Long id) {
		return new AudioMapper().mapEntityToDto(AudioDao.findById(id));
	}

	@Override
	public List<AudioDto> updateAudios(List<AudioDto> Audios) {
		return createOrUpdateAudios(Audios, UPDATE_AUDIO_FUNCTION);
	}

	@Override
	public List<AudioDto> createAudios(List<AudioDto> Audios) {
		return createOrUpdateAudios(Audios, CREATE_AUDIO_FUNCTION);
	}

	private List<AudioDto> createOrUpdateAudios(List<AudioDto> Audios,
			Function<Audio, Audio> createOrUpdateAudioFunction) {
		AudioMapper AudioMapper = new AudioMapper();

		List<Audio> AudioToUpdate = Audios.stream().map(AudioMapper::mapDtoToEntity).collect(Collectors.toList());

		List<Audio> updatedEntities = AudioToUpdate.stream().map(createOrUpdateAudioFunction::apply)
				.collect(Collectors.toList());

		return updatedEntities.stream().map(AudioMapper::mapEntityToDto).collect(Collectors.toList());
	}

	@Override
	public boolean deleteAudio(Long id) {
		AudioDao.delete(id);

		return true;
	}

}
