package com.futurefragment.audio.impl;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.futurefragment.audio.builder.AudioEntityBuilder;
import com.futurefragment.audio.dao.AudioDao;
import com.futurefragment.audio.entity.Audio;
import com.futurefragment.dto.audio.AudioDto;
import com.futurefragment.dto.audio.builder.AudioDtoBuilder;
import com.futurefragment.dto.audio.builder.AudioDtoBuilder.AudioProperties;

@RunWith(MockitoJUnitRunner.class)
public class AudioManagerImplTest {

	@Mock
	private AudioDao audioDao;

	@InjectMocks
	private AudioManagerImpl audioManager;

	@Test
	public void shouldFindAudioById() {
		// arrange
		Audio mockAudio = AudioEntityBuilder.createAudioWithId(1L);
		when(audioDao.findById(anyLong())).thenReturn(mockAudio);

		// act
		AudioDto audio = audioManager.getAudioById(1L);

		// assert
		assertNotNull(audio);
		verify(audioDao, times(1)).findById(anyLong());
	}

	@Test
	public void shouldUpdateAudios() {
		// arrange
		List<AudioDto> mockAudios = AudioDtoBuilder.with(Arrays.asList(AudioProperties.CUT_AUDIO)).audios();

		Audio mockAudio = AudioEntityBuilder.createAudioWithId(1L);
		when(audioDao.update(any(Audio.class))).thenReturn(mockAudio);

		// act
		List<AudioDto> updatedAudios = audioManager.updateAudios(mockAudios);

		// assert
		assertNotNull(updatedAudios);
		verify(audioDao, times(1)).update(any(Audio.class));
	}

	@Test
	public void shouldCreateAudios() {
		// arrange
		List<AudioDto> mockAudios = AudioDtoBuilder.with(Arrays.asList(AudioProperties.CUT_AUDIO)).audios();
		Audio mockAudio = AudioEntityBuilder.createAudioWithId(1L);

		when(audioDao.create(any(Audio.class))).thenReturn(mockAudio);

		// act
		List<AudioDto> createdAudios = audioManager.createAudios(mockAudios);

		// assert
		assertNotNull(createdAudios);
		verify(audioDao, times(1)).create(any(Audio.class));
	}

	@Test
	public void shouldDeleteAudioById() {
		// arrange
		doNothing().when(audioDao).delete(anyLong());

		// act
		boolean success = audioManager.deleteAudio(1L);

		// assert
		assertThat("Audio deletion failed", success);
		verify(audioDao, times(1)).delete(anyLong());
	}
}
