package com.futurefragment.audio.impl;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.futurefragment.audio.builder.CutAudioEntityBuilder;
import com.futurefragment.audio.dao.CutAudioDao;
import com.futurefragment.audio.entity.CutAudio;
import com.futurefragment.dto.audio.CutAudioDto;
import com.futurefragment.dto.audio.builder.CutAudioDtoBuilder;

@RunWith(MockitoJUnitRunner.class)
public class CutAudioManagerImplTest {

	@Mock
	private CutAudioDao cutAudioDao;

	@InjectMocks
	private CutAudioManagerImpl cutAudioManager;

	@Test
	public void shouldFindCutAudioById() {
		// arrange
		CutAudio mockCutAudio = CutAudioEntityBuilder.createCutAudioWithId(1L);
		when(cutAudioDao.findById(anyLong())).thenReturn(mockCutAudio);

		// act
		CutAudioDto cutAudio = cutAudioManager.getCutAudioById(1L);

		// assert
		assertNotNull(cutAudio);
		verify(cutAudioDao, times(1)).findById(anyLong());
	}

	@Test
	public void shouldFindCutAudioByName() {
		// arrange
		CutAudio mockCutAudio = CutAudioEntityBuilder.createCutAudioWithId(1L);
		when(cutAudioDao.findCutAudioByName(anyString())).thenReturn(Optional.of(mockCutAudio));

		// act
		Optional<CutAudioDto> maybeCutAudio = cutAudioManager.getCutAudioByName("audio 1");

		// assert
		assertNotNull(maybeCutAudio);
		assertTrue(maybeCutAudio.isPresent());
		verify(cutAudioDao, times(1)).findCutAudioByName(anyString());
	}

	@Test
	public void shouldUpdateCutAudios() {
		// arrange
		List<CutAudioDto> mockCutAudios = CutAudioDtoBuilder.getCutAudios();
		CutAudio mockCutAudio = CutAudioEntityBuilder.createCutAudio();
		when(cutAudioDao.update(any(CutAudio.class))).thenReturn(mockCutAudio);

		// act
		List<CutAudioDto> updatedCutAudios = cutAudioManager.updateCutAudios(mockCutAudios);

		// assert
		assertNotNull(updatedCutAudios);
		verify(cutAudioDao, times(1)).update(any(CutAudio.class));
	}

	@Test
	public void shouldCreateCutAudios() {
		// arrange
		List<CutAudioDto> mockCutAudios = CutAudioDtoBuilder.getCutAudios();
		CutAudio mockCutAudio = CutAudioEntityBuilder.createCutAudio();
		when(cutAudioDao.create(any(CutAudio.class))).thenReturn(mockCutAudio);

		// act
		List<CutAudioDto> createdCutAudios = cutAudioManager.createCutAudios(mockCutAudios);

		// assert
		assertNotNull(createdCutAudios);
		verify(cutAudioDao, times(1)).create(any(CutAudio.class));
	}

	@Test
	public void shouldDeleteCutAudioById() {
		// arrange
		doNothing().when(cutAudioDao).delete(anyLong());

		// act
		boolean success = cutAudioManager.deleteCutAudio(1L);

		// assert
		assertThat("Cut audio deletion failed", success);
		verify(cutAudioDao, times(1)).delete(anyLong());
	}
}
