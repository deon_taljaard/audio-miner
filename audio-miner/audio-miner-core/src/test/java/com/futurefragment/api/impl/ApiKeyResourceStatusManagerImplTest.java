package com.futurefragment.api.impl;

import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.futurefragment.api.builder.ApiKeyResourceStatusEntityBuilder;
import com.futurefragment.api.dao.ApiKeyResourceStatusLookupDao;
import com.futurefragment.api.entity.ApiKeyResourceStatusLookup;
import com.futurefragment.api.impl.ApiKeyResourceStatusManagerImpl;
import com.futurefragment.dto.common.ResourceStatusDto;
import com.futurefragment.dto.common.builder.ResourceStatusDtoBuilder;

@RunWith(MockitoJUnitRunner.class)
public class ApiKeyResourceStatusManagerImplTest {

	@Mock
	private ApiKeyResourceStatusLookupDao apiKeyResourceStatusLookupDao;

	@InjectMocks
	private ApiKeyResourceStatusManagerImpl apiKeyResourceStatusManager;

	@Test
	public void shouldFindApiKeyResourceStatusById() {
		// arrange
		ApiKeyResourceStatusLookup mockApiKeyResourceStatus = ApiKeyResourceStatusEntityBuilder
				.createResourceStatus(1L);
		when(apiKeyResourceStatusLookupDao.findById(anyLong())).thenReturn(mockApiKeyResourceStatus);

		// act
		ResourceStatusDto apiKeyResourceStatus = apiKeyResourceStatusManager.getApiKeyResourceStatusByApiKeyId(1l);

		// assert
		assertNotNull(apiKeyResourceStatus);
		verify(apiKeyResourceStatusLookupDao, times(1)).findById(anyLong());
	}

	@Test
	public void shouldUpdateApiKeyResourceStatus() {
		// arrange
		List<ResourceStatusDto> mockResourceStatus = ResourceStatusDtoBuilder.getResourceStatus();
		ApiKeyResourceStatusLookup mockResourceState = ApiKeyResourceStatusEntityBuilder.createResourceStatus(1L);
		when(apiKeyResourceStatusLookupDao.update(any(ApiKeyResourceStatusLookup.class))).thenReturn(mockResourceState);

		// act
		List<ResourceStatusDto> updatedApiKeyResourceStatus = apiKeyResourceStatusManager
				.updateApiKeyResourceStatus(mockResourceStatus);

		// assert
		assertNotNull(updatedApiKeyResourceStatus);
		verify(apiKeyResourceStatusLookupDao, times(1)).update(any(ApiKeyResourceStatusLookup.class));
	}

}
