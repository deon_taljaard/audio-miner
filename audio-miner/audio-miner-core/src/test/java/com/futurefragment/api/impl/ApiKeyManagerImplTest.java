package com.futurefragment.api.impl;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.futurefragment.api.builder.ApiKeyEntityBuilder;
import com.futurefragment.api.dao.ApiKeyDao;
import com.futurefragment.api.entity.ApiKey;
import com.futurefragment.api.impl.ApiKeyManagerImpl;
import com.futurefragment.dto.api.ApiKeyDto;
import com.futurefragment.dto.api.builder.ApiKeyDtoBuilder;

@RunWith(MockitoJUnitRunner.class)
public class ApiKeyManagerImplTest {

	@Mock
	private ApiKeyDao apiKeyDao;

	@InjectMocks
	private ApiKeyManagerImpl apiKeyManager;

	@Test
	public void shouldFindApiKeyById() {
		// arrange
		ApiKey mockApiKey = ApiKeyEntityBuilder.createApiKey(1L);
		when(apiKeyDao.findById(anyLong())).thenReturn(mockApiKey);

		// act
		ApiKeyDto apiKey = apiKeyManager.getApiKeyById(1L);

		// assert
		assertNotNull(apiKey);
		verify(apiKeyDao, times(1)).findById(anyLong());
	}

	@Test
	public void shouldFindApiKeyByKey() {
		// arrange
		Optional<ApiKey> mockApiKey = Optional.of(ApiKeyEntityBuilder.createApiKey(1L));
		when(apiKeyDao.findApiKeyByKey(anyString())).thenReturn(mockApiKey);

		// act
		Optional<ApiKeyDto> maybeApiKey = apiKeyManager.getApiKeyByKey("123123");

		// assert
		assertNotNull(maybeApiKey);
		assertThat("ApiKeyDto is present", maybeApiKey.isPresent());
		verify(apiKeyDao, times(1)).findApiKeyByKey(anyString());
	}

	@Test
	public void shouldUpdateApiKeys() {
		// arrange
		List<ApiKeyDto> mockApiKeys = ApiKeyDtoBuilder.getApiKeys();
		ApiKey mockApiKey = ApiKeyEntityBuilder.createApiKey(1L);
		when(apiKeyDao.update(any(ApiKey.class))).thenReturn(mockApiKey);

		List<ApiKeyDto> updatedApiKeys = apiKeyManager.updateApiKeys(mockApiKeys);

		// assert
		assertNotNull(updatedApiKeys);
		verify(apiKeyDao, times(1)).update(any(ApiKey.class));
	}

	@Test
	public void shouldCreateApiKeys() {
		// arrange
		List<ApiKeyDto> mockApiKeys = ApiKeyDtoBuilder.getApiKeys();
		ApiKey mockApiKey = ApiKeyEntityBuilder.createApiKey(1L);

		when(apiKeyDao.create(any(ApiKey.class))).thenReturn(mockApiKey);

		List<ApiKeyDto> updatedApiKeys = apiKeyManager.createApiKeys(mockApiKeys);

		// assert
		assertNotNull(updatedApiKeys);
		verify(apiKeyDao, times(1)).create(any(ApiKey.class));
	}

	@Test
	public void shouldDeleteApiKeyById() {
		// arrange
		doNothing().when(apiKeyDao).delete(anyLong());

		// act
		boolean success = apiKeyManager.deleteApiKey(1L);

		// assert
		assertThat("ApiKey deletion failed", success);
		verify(apiKeyDao, times(1)).delete(anyLong());
	}

}
