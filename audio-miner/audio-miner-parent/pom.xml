<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>

	<parent>
		<groupId>com.futurefragment</groupId>
		<artifactId>audio-miner</artifactId>
		<version>1.0.0-SNAPSHOT</version>
	</parent>

	<artifactId>audio-miner-parent</artifactId>

	<packaging>pom</packaging>

	<name>audio-miner-parent</name>

	<properties>
		<version.apache.commons>3.5</version.apache.commons>
		<version.apache.mime4j>0.6.1</version.apache.mime4j>
		<version.aws.s3.sdk>1.11.123</version.aws.s3.sdk>
		<version.commons-io>1.3.2</version.commons-io>
		<version.commons-validator>1.6</version.commons-validator>
		<version.apache.deltaspike>1.7.2</version.apache.deltaspike>
		<version.fasterxml.jackson>2.8.5</version.fasterxml.jackson>
		<version.google-api-client>1.22.0</version.google-api-client>
		<version.h2>1.4.187</version.h2>
		<version.hamcrest>1.3</version.hamcrest>
		<version.httpcomponents>4.5.3</version.httpcomponents>
		<version.javamelody>1.67.0</version.javamelody>
		<version.jaxrs>2.0.1</version.jaxrs>
		<version.jaxrs-api>3.0.12.Final</version.jaxrs-api>
		<version.jbcrypt>0.4</version.jbcrypt>
		<version.jboss.bom>10.1.1.Final</version.jboss.bom>
		<version.jrobin>1.5.9</version.jrobin>
		<version.jwt>3.1.0</version.jwt>
		<version.liquibase>3.5.3</version.liquibase>
		<version.maven.compiler.plugin>3.6.0</version.maven.compiler.plugin>
		<version.maven.war.plugin>3.0.0</version.maven.war.plugin>
		<version.maven.jar.plugin>3.0.0</version.maven.jar.plugin>
		<version.mockito>2.7.5</version.mockito>
		<version.postgresql>9.1-901-1.jdbc4</version.postgresql>
		<version.restassured>2.8.0</version.restassured>
		<version.slf4j>1.7.22</version.slf4j>
	</properties>

	<dependencyManagement>
		<dependencies>

			<dependency>
				<groupId>com.futurefragment</groupId>
				<artifactId>audio-miner-api</artifactId>
				<version>${project.version}</version>
			</dependency>

			<dependency>
				<groupId>com.futurefragment</groupId>
				<artifactId>audio-miner-core</artifactId>
				<version>${project.version}</version>
			</dependency>

			<dependency>
				<groupId>com.futurefragment</groupId>
				<artifactId>audio-miner-integration</artifactId>
				<version>${project.version}</version>
			</dependency>

			<dependency>
				<groupId>com.futurefragment</groupId>
				<artifactId>audio-miner-datamodel</artifactId>
				<version>${project.version}</version>
			</dependency>

			<dependency>
				<groupId>com.futurefragment</groupId>
				<artifactId>audio-miner-common</artifactId>
				<version>${project.version}</version>
			</dependency>

			<dependency>
				<groupId>org.wildfly.bom</groupId>
				<artifactId>wildfly-javaee7-with-tools</artifactId>
				<version>${version.jboss.bom}</version>
				<type>pom</type>
				<scope>import</scope>
			</dependency>

			<dependency>
				<groupId>org.slf4j</groupId>
				<artifactId>slf4j-api</artifactId>
				<version>${version.slf4j}</version>
				<scope>provided</scope>
			</dependency>

			<dependency>
				<groupId>org.apache.commons</groupId>
				<artifactId>commons-lang3</artifactId>
				<version>${version.apache.commons}</version>
			</dependency>

			<dependency>
				<groupId>org.apache.deltaspike.core</groupId>
				<artifactId>deltaspike-core-api</artifactId>
				<version>${version.apache.deltaspike}</version>
			</dependency>

			<dependency>
				<groupId>org.apache.deltaspike.core</groupId>
				<artifactId>deltaspike-core-impl</artifactId>
				<version>${version.apache.deltaspike}</version>
			</dependency>

			<dependency>
				<groupId>org.mindrot</groupId>
				<artifactId>jbcrypt</artifactId>
				<version>${version.jbcrypt}</version>
			</dependency>

			<dependency>
				<groupId>org.liquibase</groupId>
				<artifactId>liquibase-core</artifactId>
				<version>${version.liquibase}</version>
			</dependency>

			<dependency>
				<groupId>com.google.api-client</groupId>
				<artifactId>google-api-client</artifactId>
				<version>${version.google-api-client}</version>
			</dependency>

			<dependency>
				<groupId>org.apache.commons</groupId>
				<artifactId>commons-io</artifactId>
				<version>${version.commons-io}</version>
			</dependency>

			<dependency>
				<groupId>org.apache.httpcomponents</groupId>
				<artifactId>httpclient</artifactId>
				<version>${version.httpcomponents}</version>
			</dependency>

			<dependency>
				<groupId>org.apache.httpcomponents</groupId>
				<artifactId>httpmime</artifactId>
				<version>${version.httpcomponents}</version>
			</dependency>

			<dependency>
				<groupId>org.apache.james</groupId>
				<artifactId>apache-mime4j</artifactId>
				<version>${version.apache.mime4j}</version>
			</dependency>

			<dependency>
				<groupId>com.amazonaws</groupId>
				<artifactId>aws-java-sdk-s3</artifactId>
				<version>${version.aws.s3.sdk}</version>
			</dependency>

			<dependency>
				<groupId>commons-validator</groupId>
				<artifactId>commons-validator</artifactId>
				<version>${version.commons-validator}</version>
			</dependency>

			<!-- javamelody-core -->
			<dependency>
				<groupId>net.bull.javamelody</groupId>
				<artifactId>javamelody-core</artifactId>
				<version>${version.javamelody}</version>
			</dependency>
			
			<dependency>
				<groupId>org.jrobin</groupId>
				<artifactId>jrobin</artifactId>
				<version>${version.jrobin}</version>
			</dependency>

			<!-- Test dependencies -->
			<dependency>
				<groupId>com.jayway.restassured</groupId>
				<artifactId>rest-assured</artifactId>
				<version>${version.restassured}</version>
				<scope>test</scope>
			</dependency>

			<dependency>
				<groupId>com.h2database</groupId>
				<artifactId>h2</artifactId>
				<version>${version.h2}</version>
				<scope>test</scope>
			</dependency>

		</dependencies>
	</dependencyManagement>

	<dependencies>
		<!-- Test dependencies -->
		<dependency>
			<groupId>junit</groupId>
			<artifactId>junit</artifactId>
			<scope>test</scope>
		</dependency>

		<dependency>
			<groupId>org.mockito</groupId>
			<artifactId>mockito-core</artifactId>
			<version>${version.mockito}</version>
			<scope>test</scope>
		</dependency>

		<dependency>
			<groupId>org.hamcrest</groupId>
			<artifactId>hamcrest-all</artifactId>
			<version>${version.hamcrest}</version>
			<scope>test</scope>
		</dependency>

	</dependencies>

	<build>
		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
				<version>${version.maven.compiler.plugin}</version>
				<configuration>
					<source>${version.java}</source>
					<target>${version.java}</target>
				</configuration>
			</plugin>
		</plugins>
	</build>

</project>
