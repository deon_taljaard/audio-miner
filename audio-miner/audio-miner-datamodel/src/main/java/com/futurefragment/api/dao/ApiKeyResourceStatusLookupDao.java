package com.futurefragment.api.dao;

import javax.ejb.Stateless;

import com.futurefragment.api.entity.ApiKeyResourceStatusLookup;
import com.futurefragment.common.dao.GenericDaoImpl;

@Stateless
public class ApiKeyResourceStatusLookupDao extends GenericDaoImpl<ApiKeyResourceStatusLookup> {

	public ApiKeyResourceStatusLookupDao() {
		super(ApiKeyResourceStatusLookup.class);
	}

}
