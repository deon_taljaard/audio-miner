package com.futurefragment.api.dao;

import java.util.List;
import java.util.Optional;

import javax.ejb.Stateless;
import javax.inject.Inject;

import com.futurefragment.api.entity.ApiKey;
import com.futurefragment.common.QueryParameter;
import com.futurefragment.common.dao.GenericDaoImpl;
import com.futurefragment.common.dao.ResourceStatusDao;
import com.futurefragment.common.entity.ResourceStatus;
import com.futurefragment.enumlookups.ResourceStatusLookup;

@Stateless
public class ApiKeyDao extends GenericDaoImpl<ApiKey> {

	@Inject
	private ResourceStatusDao resourceStatusDao;

	public ApiKeyDao() {
		super(ApiKey.class);
	}

	/**
	 * Do not delete the API key completely. Use the API key's resource status
	 * to mark them as deleted.
	 */
	@Override
	public void delete(Long id) {
		ApiKey apiKey = super.findById(id);

		ResourceStatus resourceStatus = resourceStatusDao.findByDescription(ResourceStatusLookup.DELETED.name());
		apiKey.getApiKeyResourceStatus().setResourceStatus(resourceStatus);

		super.update(apiKey);
	}

	public Optional<ApiKey> findApiKeyByKey(String key) {
		List<ApiKey> apiKeys = this.findWithNamedQuery("ApiKey.findByKey",
				QueryParameter.with("key", key).parameters());

		if (apiKeys.isEmpty()) {
			return Optional.empty();
		}

		return Optional.of(apiKeys.get(0));
	}

	public void setResourceStatusDao(ResourceStatusDao resourceStatusDao) {
		this.resourceStatusDao = resourceStatusDao;
	}

}
