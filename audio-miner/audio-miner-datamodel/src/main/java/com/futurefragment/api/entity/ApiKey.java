package com.futurefragment.api.entity;

import java.io.Serializable;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.futurefragment.common.entity.VoiceCollageEntity;
import com.futurefragment.util.KeyGenerator;

@Entity
@Table(name = "api_key")
@NamedQueries({ @NamedQuery(name = "ApiKey.findByKey", query = "SELECT a FROM ApiKey a WHERE a.key = :key") })
public class ApiKey implements Serializable, VoiceCollageEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "api_key_id_generator", sequenceName = "api_key_id_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "api_key_id_generator")
	private Long id;

	@Column(nullable = false, updatable = false)
	private String key;

	@Column(nullable = false)
	private String description;

	@OneToOne(cascade = CascadeType.ALL)
	@PrimaryKeyJoinColumn
	private ApiKeyResourceStatusLookup apiKeyResourceStatus;

	@PrePersist
	public void prePersist() {
		this.key = KeyGenerator.newKey(UUID.randomUUID().toString(), false);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public ApiKeyResourceStatusLookup getApiKeyResourceStatus() {
		return apiKeyResourceStatus;
	}

	public void setApiKeyResourceStatus(ApiKeyResourceStatusLookup apiKeyResourceStatus) {
		this.apiKeyResourceStatus = apiKeyResourceStatus;
	}

}