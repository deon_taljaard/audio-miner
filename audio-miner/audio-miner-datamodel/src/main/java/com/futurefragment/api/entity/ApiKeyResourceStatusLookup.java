package com.futurefragment.api.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapsId;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.futurefragment.common.entity.ResourceStatus;
import com.futurefragment.common.entity.VoiceCollageEntity;

@Entity
@Table(name = "api_key_resource_status_lookup")
@NamedQueries({
		@NamedQuery(name = "ApiKeyResourceStatusLookup.findByApiKeyId", query = "SELECT u FROM ApiKeyResourceStatusLookup u WHERE u.apiKey.id = :id") })
public class ApiKeyResourceStatusLookup implements Serializable, VoiceCollageEntity {

	private static final long serialVersionUID = 1L;

	@Id
	private Long id;

	@MapsId
	@OneToOne(mappedBy = "apiKeyResourceStatus")
	@JoinColumn(name = "id")
	private ApiKey apiKey;

	@JoinColumn(name = "resource_status_id", nullable = false, updatable = false)
	@OneToOne(fetch = FetchType.EAGER, cascade = { CascadeType.DETACH })
	private ResourceStatus resourceStatus;

	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_date", nullable = false, updatable = false)
	private Date createdDate;

	@UpdateTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_date")
	private Date updatedDate;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ApiKey getApiKey() {
		return apiKey;
	}

	public void setApiKey(ApiKey apiKey) {
		this.apiKey = apiKey;
	}

	public ResourceStatus getResourceStatus() {
		return resourceStatus;
	}

	public void setResourceStatus(ResourceStatus resourceStatus) {
		this.resourceStatus = resourceStatus;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

}
