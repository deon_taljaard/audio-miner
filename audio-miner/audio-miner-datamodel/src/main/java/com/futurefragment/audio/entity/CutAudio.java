package com.futurefragment.audio.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;

import com.futurefragment.common.entity.VoiceCollageEntity;

@Entity
@Table(name = "cut_audio")
@NamedQueries({
		@NamedQuery(name = "CutAudio.findByCutAudioName", query = "SELECT c FROM CutAudio c WHERE lower(c.description) = :description") })
public class CutAudio implements Serializable, VoiceCollageEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "cut_audio_id_generator", sequenceName = "cut_audio_id_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "cut_audio_id_generator")
	private Long id;

	@Column(nullable = false)
	private String description;

	@Column(name = "resource_path", nullable = false)
	private String resourcePath;

	@JoinColumn(name = "audio_id", nullable = false, updatable = false)
	@ManyToOne(cascade = { CascadeType.DETACH })
	private Audio audio;

	@Column(name = "sequence_id", nullable = false)
	private Integer sequenceId;

	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_date", nullable = false, updatable = false)
	private Date createdDate;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getResourcePath() {
		return resourcePath;
	}

	public void setResourcePath(String resourcePath) {
		this.resourcePath = resourcePath;
	}

	public Audio getAudio() {
		return audio;
	}

	public void setAudio(Audio audio) {
		this.audio = audio;
	}

	public Integer getSequenceId() {
		return sequenceId;
	}

	public void setSequenceId(Integer sequenceId) {
		this.sequenceId = sequenceId;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

}
