package com.futurefragment.audio.dao;

import javax.ejb.Stateless;

import com.futurefragment.audio.entity.Audio;
import com.futurefragment.common.dao.GenericDaoImpl;

@Stateless
public class AudioDao extends GenericDaoImpl<Audio> {

	public AudioDao() {
		super(Audio.class);
	}

}
