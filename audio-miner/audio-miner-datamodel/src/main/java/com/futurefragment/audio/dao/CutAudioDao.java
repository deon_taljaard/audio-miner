package com.futurefragment.audio.dao;

import java.util.List;
import java.util.Optional;

import javax.ejb.Stateless;

import com.futurefragment.audio.entity.CutAudio;
import com.futurefragment.common.QueryParameter;
import com.futurefragment.common.dao.GenericDaoImpl;
import com.futurefragment.constants.PersistenceConstants;

@Stateless
public class CutAudioDao extends GenericDaoImpl<CutAudio> {

	public CutAudioDao() {
		super(CutAudio.class);
	}

	public Optional<CutAudio> findCutAudioByName(String description) {
		List<CutAudio> cutAudios = super.findWithNamedQuery("CutAudio.findByCutAudioName", QueryParameter
				.with(PersistenceConstants.PARAMETER_NAME_DESCRIPTION, description.toLowerCase()).parameters());

		if (cutAudios.isEmpty()) {
			return Optional.empty();
		}

		return Optional.of(cutAudios.get(0));
	}

}
