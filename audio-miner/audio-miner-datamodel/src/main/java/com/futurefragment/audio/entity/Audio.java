package com.futurefragment.audio.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;

import com.futurefragment.common.entity.VoiceCollageEntity;
import com.futurefragment.constant.CommonConstants;
import com.futurefragment.util.KeyGenerator;
import com.futurefragment.util.PathUtil;

@Entity
@Table(name = "audio")
public class Audio implements Serializable, VoiceCollageEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "audio_id_generator", sequenceName = "audio_id_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "audio_id_generator")
	private Long id;

	@Column(nullable = false)
	private String description;

	@Column(name = "resource_path", nullable = false)
	private String resourcePath;

	@OneToMany(mappedBy = "audio", cascade = { CascadeType.ALL })
	private List<CutAudio> cutAudios;

	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_date", nullable = false, updatable = false)
	private Date createdDate;

	@PrePersist
	public void prePersist() {
		this.resourcePath = PathUtil.buildPathFromArgs(CommonConstants.AUDIOS, KeyGenerator.newKey());
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getResourcePath() {
		return resourcePath;
	}

	public void setResourcePath(String resourcePath) {
		this.resourcePath = resourcePath;
	}

	public List<CutAudio> getCutAudios() {
		return cutAudios;
	}

	public void setCutAudios(List<CutAudio> cutAudios) {
		this.cutAudios = cutAudios;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
}
