package com.futurefragment.constants;

public class PersistenceConstants {

	// Entity graph names

	public static final String ENTITY_GRAPH_NAME_USER_WITH_CURRENT_LOCATION = "userWithCurrentLocation";

	// Named query names

	public static final String NAMED_QUERY_NAME_USER_FIND_USER_BY_ID = "User.findUserById";

	// Query Parameter names

	public static final String QUERY_PARAM_NAME_ENTITY_GRAPH_NAME = "entityGraphName";
	public static final String QUERY_PARAM_NAME_NAMED_QUERY_NAME = "namedQueryName";

	// Query hints

	public static final String QUERY_HINT_LOADGRAPH = "javax.persistence.loadgraph";

	// Parameter names

	public static final String PARAMETER_NAME_ID = "id";
	public static final String PARAMETER_NAME_DESCRIPTION = "description";
	public static final String PARAMETER_NAME_EMAIL = "email";
	public static final String PARAMETER_NAME_GOOGLE_ID = "google_id";
	
	// Common exception messages

}
