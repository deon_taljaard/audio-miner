package com.futurefragment.common.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "resource_status")
@NamedQueries({
		@NamedQuery(name = "ResourceStatus.findByDescription", query = "SELECT r FROM ResourceStatus r WHERE lower(r.description) = :description") })
public class ResourceStatus implements Serializable, VoiceCollageEntity {

	private static final long serialVersionUID = 1L;

	@Id
	private Long id;

	private String description;

	public ResourceStatus() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
