package com.futurefragment.common.dao;

import java.util.List;
import java.util.Map;

public interface GenericDao<T> {

	T create(T entity);

	void delete(Long id);

	T findById(Long id);

	List<T> findAll();

	T update(T entity);

	List<T> findWithNamedQuery(String namedQueryName, Map<String, Object> parameters);

	int excecuteNamedQueryForDeleteOrUpdate(String namedQueryName, Map<String, Object> parameters);

	Long countRecords();

	void detachEntity(T entity);

}
