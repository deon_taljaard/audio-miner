package com.futurefragment.common.dao;

import java.util.List;
import java.util.Map;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.futurefragment.common.entity.VoiceCollageEntity;
import com.futurefragment.constant.ExceptionConstants;
import com.futurefragment.constants.PersistenceConstants;
import com.futurefragment.exception.ExceptionContext;
import com.futurefragment.exception.Severity;
import com.futurefragment.exception.AudioMinerExceptionFactory;

public class GenericDaoImpl<T extends VoiceCollageEntity> implements GenericDao<T> {

	private static final String QUERY_FIND_ALL = "select t from %s t";
	private static final String QUERY_COUNT_RECORDS = "select count(*) from %s";

	// Method names
	private static final String METHOD_CREATE = "create";
	private static final String METHOD_DELETE = "delete";
	private static final String METHOD_FIND_BY_ID = "findById";
	private static final String METHOD_FIND_ALL = "findAll";
	private static final String METHOD_UPDATE = "update";
	private static final String METHOD_FIND_BY_NAMED_QUERY = "findByNamedQuery";

	// Messages
	public static final String ERR_ENTITY_EXISTS_ADMIN_DESCRIPTION = "The entity already exists.";
	protected static final String ERR_UNKNOWN_ENTITY_EXISTS_ADMIN_DESCRIPTION = "An unknown error occured while creating an entity.";
	public static final String ERR_ENTITY_EXISTS_ADMIN_CORRECTION = "Ensure the id field on the entity has not been set.";
	public static final String ERR_DELETE_ENTITY_ADMIN_DESCRIPTION = "An unknown error occured while deleting an entity.";
	public static final String ERR_FIND_BY_ID_ADMIN_DESCRIPTION = "An unknown error occured while finding an entity by id.";
	public static final String ERR_ENSURE_VALID_ID_ADMIN_CORRECTION = "Ensure the id is valid.";
	public static final String ERR_ENTITY_DOES_NOT_EXIST = "The entity does not exist.";
	protected static final String ERR_FIND_ALL_INVALID_QUERY = "The query to find all entities is invalid.";
	protected static final String ERR_FIND_ALL_INVALID_QUERY_CORRECTION = "Ensure that the findAll query is valid and well formed. See the query used in the paramters section of the exception context.";
	protected static final String ERR_FIND_ALL_ADMIN_DESCRIPTION = "An unknown error occured while finding all the entities for a record.";
	public static final String ERR_UPDATE_INVALID_ENTITY_OR_REMOVED_ADMIN_DESCRIPTION = "The entity is invalid or has been removed.";
	public static final String ERR_UPDATE_INVALID_ENTITY_OR_REMOVED_ADMIN_CORRECTION = "The entity to update is either invalid or it has been removed, i.e. the id may not be used anymore.";
	public static final String ERR_UPDATE_ADMIN_DESCRIPTION = "An unknown error occured while updating an entity.";
	protected static final String ERR_UNABLE_TO_FIND_RESOURCE_DESCRIPTION = "Unable to find the resource as requested.";
	public static final String ERR_FIND_NAMED_QUERY_INVALID = "The named query to find an entity is invalid.";
	public static final String ERR_FIND_NAMED_QUERY_INVALID_CORRECTION = "Ensure that the named query is valid and well formed. See the name of the query used as well as the parameters passed in the exception context.";
	protected static final String ERR_FIND_NAMED_QUERY_DESCRIPTION = "An unknown error occured while finding an entity using a named query.";
	protected static final String ERR_UPDATE_DELETE_NAMED_QUERY_DESCRIPTION = "An unknown error occured while updating/deteleting an entity using a named query.";
	protected static final String ERR_COUNT_QUERY_INVALID = "The count query is invalid.";
	protected static final String ERR_COUNT_QUERY_INVALID_CORRECTION = "Ensure that the count query is valid and well formed. See the query used in the exception context.";
	protected static final String ERR_COUNT_QUERY_DESCRIPTION = "An unknown error occured while counting the records for an entity.";

	private static final String NAMED_QUERY = "namedQuery";
	private static final String COUNT_RECORDS = "countRecords";

	@PersistenceContext(unitName = "voice-collage-pu")
	protected EntityManager entityManager;

	private final Class<T> entityClass;

	public GenericDaoImpl(Class<T> entityClass) {
		this.entityClass = entityClass;
	}

	@Override
	public T create(T entity) {
		try {
			entityManager.persist(entity);
			return entity;
		} catch (EntityExistsException ex) {
			// @formatter:off
			ExceptionContext exceptionContext = ExceptionContext
					.getInstance(ExceptionConstants.APPLICATION_LAYER_PERSISTENCE, entityClass.getSimpleName(),
							METHOD_CREATE, ex)
					.setSeverity(Severity.FATAL)
					.setAdminErrorDescription(
							ERR_ENTITY_EXISTS_ADMIN_DESCRIPTION + ExceptionConstants.MSG + ex.getMessage())
					.setAdminErrorCorrection(ERR_ENTITY_EXISTS_ADMIN_CORRECTION)
					.setUserErrorDescription("Unable to create the resource as requested.")
					.setUserErrorCorrection(ExceptionConstants.ERR_CONTACT_ADMIN_USER_CORRECTION);

			throw AudioMinerExceptionFactory.raiseException(exceptionContext);
			// @formatter:on
		} catch (Exception ex) {
			// @formatter:off
			ExceptionContext exceptionContext = ExceptionContext
					.getInstance(ExceptionConstants.APPLICATION_LAYER_PERSISTENCE, entityClass.getSimpleName(),
							METHOD_CREATE, ex)
					.setSeverity(Severity.FATAL)
					.setAdminErrorDescription(
							ERR_UNKNOWN_ENTITY_EXISTS_ADMIN_DESCRIPTION + ExceptionConstants.MSG + ex.getMessage())
					.setAdminErrorCorrection(ERR_ENTITY_EXISTS_ADMIN_CORRECTION)
					.setUserErrorDescription(ERR_UNKNOWN_ENTITY_EXISTS_ADMIN_DESCRIPTION)
					.setUserErrorCorrection(ExceptionConstants.ERR_CONTACT_ADMIN_USER_CORRECTION);

			throw AudioMinerExceptionFactory.raiseException(exceptionContext);
			// @formatter:on
		} catch (Throwable t) {
			System.out.println();
			return null;
		}
	}

	@Override
	public void delete(Long id) {
		try {
			entityManager.remove(entityManager.getReference(entityClass, id));
		} catch (IllegalArgumentException ex) {
			// @formatter:off
			ExceptionContext exceptionContext = ExceptionContext
					.getInstance(ExceptionConstants.APPLICATION_LAYER_PERSISTENCE, entityClass.getSimpleName(),
							METHOD_DELETE, ex)
					.setSeverity(Severity.FATAL).addParameter(PersistenceConstants.PARAMETER_NAME_ID, id)
					.setAdminErrorDescription(ERR_ENTITY_DOES_NOT_EXIST + ExceptionConstants.MSG + ex.getMessage())
					.setAdminErrorCorrection(ERR_ENSURE_VALID_ID_ADMIN_CORRECTION)
					.setUserErrorDescription("Unable to delete the resource as requested.")
					.setUserErrorCorrection(ExceptionConstants.ERR_CONTACT_ADMIN_USER_CORRECTION);

			throw AudioMinerExceptionFactory.raiseException(exceptionContext);
			// @formatter:on
		} catch (Exception ex) {
			// @formatter:off
			ExceptionContext exceptionContext = ExceptionContext
					.getInstance(ExceptionConstants.APPLICATION_LAYER_PERSISTENCE, entityClass.getSimpleName(),
							METHOD_DELETE, ex)
					.setSeverity(Severity.FATAL).addParameter(PersistenceConstants.PARAMETER_NAME_ID, id)
					.setAdminErrorDescription(
							ERR_DELETE_ENTITY_ADMIN_DESCRIPTION + ExceptionConstants.MSG + ex.getMessage())
					.setAdminErrorCorrection(ERR_ENSURE_VALID_ID_ADMIN_CORRECTION)
					.setUserErrorDescription(ERR_DELETE_ENTITY_ADMIN_DESCRIPTION)
					.setUserErrorCorrection(ExceptionConstants.ERR_CONTACT_ADMIN_USER_CORRECTION);

			throw AudioMinerExceptionFactory.raiseException(exceptionContext);
			// @formatter:on
		}
	}

	@Override
	public T findById(Long id) {
		try {
			T entity = entityManager.find(entityClass, id);

			if (entity == null) {
				throw new IllegalArgumentException("Id with value " + id + " does not exist.");
			}

			return entity;
		} catch (IllegalArgumentException ex) {
			// @formatter:off
			ExceptionContext exceptionContext = ExceptionContext
					.getInstance(ExceptionConstants.APPLICATION_LAYER_PERSISTENCE, entityClass.getSimpleName(),
							METHOD_FIND_BY_ID, ex)
					.setSeverity(Severity.FATAL).addParameter(PersistenceConstants.PARAMETER_NAME_ID, id)
					.setAdminErrorDescription(ERR_ENTITY_DOES_NOT_EXIST + ExceptionConstants.MSG + ex.getMessage())
					.setAdminErrorCorrection(ERR_ENSURE_VALID_ID_ADMIN_CORRECTION)
					.setUserErrorDescription(ERR_UNABLE_TO_FIND_RESOURCE_DESCRIPTION)
					.setUserErrorCorrection(ExceptionConstants.ERR_CONTACT_ADMIN_USER_CORRECTION);

			throw AudioMinerExceptionFactory.raiseException(exceptionContext);
			// @formatter:on
		} catch (Exception ex) {
			// @formatter:off
			ExceptionContext exceptionContext = ExceptionContext
					.getInstance(ExceptionConstants.APPLICATION_LAYER_PERSISTENCE, entityClass.getSimpleName(),
							METHOD_FIND_BY_ID, ex)
					.setSeverity(Severity.FATAL).addParameter(PersistenceConstants.PARAMETER_NAME_ID, id)
					.setAdminErrorDescription(
							ERR_FIND_BY_ID_ADMIN_DESCRIPTION + ExceptionConstants.MSG + ex.getMessage())
					.setAdminErrorCorrection(ERR_ENSURE_VALID_ID_ADMIN_CORRECTION)
					.setUserErrorDescription(ERR_FIND_BY_ID_ADMIN_DESCRIPTION)
					.setUserErrorCorrection(ExceptionConstants.ERR_CONTACT_ADMIN_USER_CORRECTION);

			throw AudioMinerExceptionFactory.raiseException(exceptionContext);
			// @formatter:on
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<T> findAll() {
		String findAllQuery = String.format(QUERY_FIND_ALL, entityClass.getSimpleName());
		try {
			return entityManager.createQuery(findAllQuery).getResultList();
		} catch (IllegalArgumentException ex) {
			// @formatter:off
			ExceptionContext exceptionContext = ExceptionContext
					.getInstance(ExceptionConstants.APPLICATION_LAYER_PERSISTENCE, entityClass.getSimpleName(),
							METHOD_FIND_ALL, ex)
					.setSeverity(Severity.FATAL).addParameter("findAll", findAllQuery)
					.setAdminErrorDescription(ERR_FIND_ALL_INVALID_QUERY + ExceptionConstants.MSG + ex.getMessage())
					.setAdminErrorCorrection(ERR_FIND_ALL_INVALID_QUERY_CORRECTION)
					.setUserErrorDescription("Unable to find the resources as requested.")
					.setUserErrorCorrection(ExceptionConstants.ERR_CONTACT_ADMIN_USER_CORRECTION);

			throw AudioMinerExceptionFactory.raiseException(exceptionContext);
			// @formatter:on
		} catch (Exception ex) {
			// @formatter:off
			ExceptionContext exceptionContext = ExceptionContext
					.getInstance(ExceptionConstants.APPLICATION_LAYER_PERSISTENCE, entityClass.getSimpleName(),
							METHOD_FIND_ALL, ex)
					.setSeverity(Severity.FATAL)
					.setAdminErrorDescription(ERR_FIND_ALL_ADMIN_DESCRIPTION + ExceptionConstants.MSG + ex.getMessage())
					.setAdminErrorCorrection(ERR_FIND_ALL_INVALID_QUERY_CORRECTION)
					.setUserErrorDescription(ERR_FIND_ALL_ADMIN_DESCRIPTION)
					.setUserErrorCorrection(ExceptionConstants.ERR_CONTACT_ADMIN_USER_CORRECTION);

			throw AudioMinerExceptionFactory.raiseException(exceptionContext);
			// @formatter:on
		}
	}

	@Override
	public T update(T entity) {
		try {
			return entityManager.merge(entity);
		} catch (IllegalArgumentException ex) {
			// @formatter:off
			ExceptionContext exceptionContext = ExceptionContext
					.getInstance(ExceptionConstants.APPLICATION_LAYER_PERSISTENCE, entityClass.getSimpleName(),
							METHOD_UPDATE, ex)
					.setSeverity(Severity.FATAL)
					.setAdminErrorDescription(ERR_UPDATE_INVALID_ENTITY_OR_REMOVED_ADMIN_DESCRIPTION
							+ ExceptionConstants.MSG + ex.getMessage())
					.setAdminErrorCorrection(ERR_UPDATE_INVALID_ENTITY_OR_REMOVED_ADMIN_CORRECTION)
					.setUserErrorDescription("Unable to update the resource as requested.")
					.setUserErrorCorrection(ExceptionConstants.ERR_CONTACT_ADMIN_USER_CORRECTION);

			throw AudioMinerExceptionFactory.raiseException(exceptionContext);
			// @formatter:on
		} catch (Exception ex) {
			// @formatter:off
			ExceptionContext exceptionContext = ExceptionContext
					.getInstance(ExceptionConstants.APPLICATION_LAYER_PERSISTENCE, entityClass.getSimpleName(),
							METHOD_UPDATE, ex)
					.setSeverity(Severity.FATAL)
					.setAdminErrorDescription(ERR_UPDATE_ADMIN_DESCRIPTION + ExceptionConstants.MSG + ex.getMessage())
					.setAdminErrorCorrection(ERR_UPDATE_INVALID_ENTITY_OR_REMOVED_ADMIN_CORRECTION)
					.setUserErrorDescription(ERR_UPDATE_ADMIN_DESCRIPTION)
					.setUserErrorCorrection(ExceptionConstants.ERR_CONTACT_ADMIN_USER_CORRECTION);

			throw AudioMinerExceptionFactory.raiseException(exceptionContext);
			// @formatter:on
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<T> findWithNamedQuery(String namedQueryName, Map<String, Object> parameters) {
		try {
			return buildQuery(namedQueryName, parameters).getResultList();
		} catch (IllegalArgumentException ex) {
			// @formatter:off
			ExceptionContext exceptionContext = ExceptionContext
					.getInstance(ExceptionConstants.APPLICATION_LAYER_PERSISTENCE, entityClass.getSimpleName(),
							METHOD_FIND_BY_NAMED_QUERY, ex)
					.setSeverity(Severity.FATAL).addParameter(NAMED_QUERY, namedQueryName).addParameters(parameters)
					.setAdminErrorDescription(ERR_FIND_NAMED_QUERY_INVALID + ExceptionConstants.MSG + ex.getMessage())
					.setAdminErrorCorrection(ERR_FIND_NAMED_QUERY_INVALID_CORRECTION)
					.setUserErrorDescription(ERR_UNABLE_TO_FIND_RESOURCE_DESCRIPTION)
					.setUserErrorCorrection(ExceptionConstants.ERR_CONTACT_ADMIN_USER_CORRECTION);

			throw AudioMinerExceptionFactory.raiseException(exceptionContext);
			// @formatter:on
		} catch (Exception ex) {
			// @formatter:off
			ExceptionContext exceptionContext = ExceptionContext
					.getInstance(ExceptionConstants.APPLICATION_LAYER_PERSISTENCE, entityClass.getSimpleName(),
							METHOD_FIND_BY_NAMED_QUERY, ex)
					.setSeverity(Severity.FATAL).addParameter(NAMED_QUERY, namedQueryName).addParameters(parameters)
					.setAdminErrorDescription(
							ERR_FIND_NAMED_QUERY_DESCRIPTION + ExceptionConstants.MSG + ex.getMessage())
					.setAdminErrorCorrection(ERR_FIND_NAMED_QUERY_INVALID_CORRECTION)
					.setUserErrorDescription(ERR_FIND_NAMED_QUERY_DESCRIPTION)
					.setUserErrorCorrection(ExceptionConstants.ERR_CONTACT_ADMIN_USER_CORRECTION);

			throw AudioMinerExceptionFactory.raiseException(exceptionContext);
			// @formatter:on
		}
	}

	@Override
	public int excecuteNamedQueryForDeleteOrUpdate(String namedQueryName, Map<String, Object> parameters) {
		try {
			entityManager.getTransaction().begin();
			int result = buildQuery(namedQueryName, parameters).executeUpdate();
			entityManager.getTransaction().begin();
			return result;
		} catch (Exception ex) {
			// @formatter:off
			ExceptionContext exceptionContext = ExceptionContext
					.getInstance(ExceptionConstants.APPLICATION_LAYER_PERSISTENCE, entityClass.getSimpleName(),
							METHOD_FIND_BY_NAMED_QUERY, ex)
					.setSeverity(Severity.FATAL).addParameter(NAMED_QUERY, namedQueryName).addParameters(parameters)
					.setAdminErrorDescription(
							ERR_UPDATE_DELETE_NAMED_QUERY_DESCRIPTION + ExceptionConstants.MSG + ex.getMessage())
					.setAdminErrorCorrection(ERR_FIND_NAMED_QUERY_INVALID_CORRECTION)
					.setUserErrorDescription(ERR_UPDATE_DELETE_NAMED_QUERY_DESCRIPTION)
					.setUserErrorCorrection(ExceptionConstants.ERR_CONTACT_ADMIN_USER_CORRECTION);

			throw AudioMinerExceptionFactory.raiseException(exceptionContext);
			// @formatter:on
		}
	}

	private Query buildQuery(String namedQueryName, Map<String, Object> parameters) {
		Query query = entityManager.createNamedQuery(namedQueryName);
		parameters.forEach((key, value) -> query.setParameter(key, value));
		return query;
	}

	@Override
	public Long countRecords() {
		String countRecordsQuery = String.format(QUERY_COUNT_RECORDS, entityClass.getSimpleName());
		try {
			return (Long) entityManager.createQuery(countRecordsQuery).getSingleResult();
		} catch (IllegalArgumentException ex) {
			// @formatter:off
			ExceptionContext exceptionContext = ExceptionContext
					.getInstance(ExceptionConstants.APPLICATION_LAYER_PERSISTENCE, entityClass.getSimpleName(),
							COUNT_RECORDS, ex)
					.setSeverity(Severity.FATAL).addParameter(COUNT_RECORDS, countRecordsQuery)
					.setAdminErrorDescription(ERR_COUNT_QUERY_INVALID + ExceptionConstants.MSG + ex.getMessage())
					.setAdminErrorCorrection(ERR_COUNT_QUERY_INVALID_CORRECTION)
					.setUserErrorDescription(ERR_COUNT_QUERY_DESCRIPTION)
					.setUserErrorCorrection(ExceptionConstants.ERR_CONTACT_ADMIN_USER_CORRECTION);

			throw AudioMinerExceptionFactory.raiseException(exceptionContext);
			// @formatter:on
		} catch (Exception ex) {
			// @formatter:off
			ExceptionContext exceptionContext = ExceptionContext
					.getInstance(ExceptionConstants.APPLICATION_LAYER_PERSISTENCE, entityClass.getSimpleName(),
							COUNT_RECORDS, ex)
					.setSeverity(Severity.FATAL).addParameter(COUNT_RECORDS, countRecordsQuery)
					.setAdminErrorDescription(ERR_COUNT_QUERY_DESCRIPTION + ExceptionConstants.MSG + ex.getMessage())
					.setAdminErrorCorrection(ERR_COUNT_QUERY_INVALID_CORRECTION)
					.setUserErrorDescription(ERR_COUNT_QUERY_DESCRIPTION)
					.setUserErrorCorrection(ExceptionConstants.ERR_CONTACT_ADMIN_USER_CORRECTION);

			throw AudioMinerExceptionFactory.raiseException(exceptionContext);
			// @formatter:on
		}
	}

	@Override
	public void detachEntity(T entity) {
		entityManager.detach(entity);

	}

	/**
	 * Auxiliary method for DAO integration tests. I really don't like this. I
	 * need to find a better way to do this than to clutter source code with
	 * methods only tests will ever use.
	 * 
	 * @param em
	 *            an instance of <tt>EntityManager</tt>
	 */
	public void setEntityManager(EntityManager em) {
		this.entityManager = em;
	}
}
