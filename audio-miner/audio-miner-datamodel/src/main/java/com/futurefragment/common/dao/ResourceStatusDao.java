package com.futurefragment.common.dao;

import java.util.List;

import com.futurefragment.common.QueryParameter;
import com.futurefragment.common.entity.ResourceStatus;
import com.futurefragment.constant.ExceptionConstants;
import com.futurefragment.constants.PersistenceConstants;
import com.futurefragment.exception.ExceptionContext;
import com.futurefragment.exception.AudioMinerExceptionFactory;

public class ResourceStatusDao extends GenericDaoImpl<ResourceStatus> {

	private static final String CLASS_NAME = ResourceStatusDao.class.getSimpleName();

	public static final String ERR_INVALID_DESCRIPTION_DESCRIPTION = "Invalid resource status description.";
	public static final String ERR_INVALID_DESCRIPTION_CORRECTION = "Ensure a valid resource status description is used. See the com.voicecollage.enumlookups.ResourceStatusLookup enum.";

	public ResourceStatusDao() {
		super(ResourceStatus.class);
	}

	// This method is not returning an <code>Optional</code> because if a
	// resource status cannot be found, for example to delete a person, then
	// something is wrong. This might change though as currently this method
	// should only be used internally to lookup a resource status.
	public ResourceStatus findByDescription(String description) {
		List<ResourceStatus> resourceStatus = super.findWithNamedQuery("ResourceStatus.findByDescription",
				QueryParameter.with(PersistenceConstants.PARAMETER_NAME_DESCRIPTION, description.toLowerCase())
						.parameters());

		if (resourceStatus.isEmpty()) {
			//@formatter:off
			ExceptionContext exceptionContext = ExceptionContext.getInstance(ExceptionConstants.APPLICATION_LAYER_PERSISTENCE, CLASS_NAME, "findByDescription", null)
					.addParameter(PersistenceConstants.PARAMETER_NAME_DESCRIPTION, description)
					.setAdminErrorDescription(ERR_INVALID_DESCRIPTION_DESCRIPTION)
					.setAdminErrorCorrection(ERR_INVALID_DESCRIPTION_CORRECTION)
					.setUserErrorDescription(ERR_INVALID_DESCRIPTION_DESCRIPTION)
					.setUserErrorCorrection(ExceptionConstants.ERR_CONTACT_ADMIN_USER_CORRECTION);
			//@formatter:on

			throw AudioMinerExceptionFactory.raiseException(exceptionContext);
		}

		return resourceStatus.get(0);
	}
}
