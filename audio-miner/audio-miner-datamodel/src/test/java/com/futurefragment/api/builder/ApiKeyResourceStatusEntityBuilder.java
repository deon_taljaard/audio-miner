package com.futurefragment.api.builder;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.LongStream;

import com.futurefragment.api.entity.ApiKeyResourceStatusLookup;
import com.futurefragment.common.ResourceStatusEntityBuilder;

public final class ApiKeyResourceStatusEntityBuilder {

	private static final long SYS_TIME = System.currentTimeMillis();

	private ApiKeyResourceStatusEntityBuilder() {
	}

	public static List<ApiKeyResourceStatusLookup> getResourceStatus() {
		List<ApiKeyResourceStatusLookup> resourceStatus = new ArrayList<>();

		LongStream.range(1, 5).forEach(i -> resourceStatus.add(createResourceStatus(i)));

		return resourceStatus;
	}

	public static ApiKeyResourceStatusLookup createResourceStatus() {
		ApiKeyResourceStatusLookup resourceStatus = new ApiKeyResourceStatusLookup();

		resourceStatus.setResourceStatus(ResourceStatusEntityBuilder.createResourceStatus());
		resourceStatus.setCreatedDate(new Timestamp(SYS_TIME));
		resourceStatus.setUpdatedDate(new Timestamp(SYS_TIME));

		return resourceStatus;
	}

	public static ApiKeyResourceStatusLookup createResourceStatus(Long i) {
		ApiKeyResourceStatusLookup resourceStatus = createResourceStatus();

		resourceStatus.setId(i);

		return resourceStatus;
	}

}
