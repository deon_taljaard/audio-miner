package com.futurefragment.api.builder;

import com.futurefragment.api.entity.ApiKey;
import com.futurefragment.util.KeyGenerator;

public final class ApiKeyEntityBuilder {

	private ApiKeyEntityBuilder() {
	}

	public static ApiKey createApiKey() {
		ApiKey apiKey = new ApiKey();

		apiKey.setKey(KeyGenerator.newKey());
		apiKey.setDescription("Mobille Client 1 Key");
		apiKey.setApiKeyResourceStatus(ApiKeyResourceStatusEntityBuilder.createResourceStatus());
		apiKey.getApiKeyResourceStatus().setApiKey(apiKey);

		return apiKey;
	}

	public static ApiKey createApiKey(Long i) {
		ApiKey apiKey = createApiKey();

		apiKey.setId(i);
		apiKey.getApiKeyResourceStatus().setId(i);

		return apiKey;
	}
}
