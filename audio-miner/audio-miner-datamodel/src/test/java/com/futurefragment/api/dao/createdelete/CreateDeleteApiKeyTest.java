package com.futurefragment.api.dao.createdelete;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.futurefragment.api.builder.ApiKeyEntityBuilder;
import com.futurefragment.api.dao.ApiKeyDao;
import com.futurefragment.api.entity.ApiKey;
import com.futurefragment.common.JpaHibernateTest;
import com.futurefragment.common.dao.GenericDaoImpl;
import com.futurefragment.common.dao.ResourceStatusDao;
import com.futurefragment.enumlookups.ResourceStatusLookup;
import com.futurefragment.exception.AudioMinerException;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CreateDeleteApiKeyTest extends JpaHibernateTest {

	private static EntityManagerFactory emf;
	private static EntityManager em;

	private ApiKeyDao apiKeyDao;
	private ResourceStatusDao resourceStatusDao;

	@BeforeClass
	public static void init() {
		emf = Persistence.createEntityManagerFactory(DEFAULT_PERSISTENCE_UNIT);
		em = emf.createEntityManager();
	}

	@Before
	public void setupTest() throws Exception {
		this.dbSetup(em);
		this.setupLiquibaseTestFixtureWithoutTestData();

		apiKeyDao = new ApiKeyDao();
		apiKeyDao.setEntityManager(em);

		resourceStatusDao = new ResourceStatusDao();
		resourceStatusDao.setEntityManager(em);
		apiKeyDao.setResourceStatusDao(resourceStatusDao);
	}

	@After
	public void tearDown() throws Exception {
		this.dropDb();
	}

	@AfterClass
	public static void closeTestFixture() {
		em.close();
		emf.close();
	}

	@Test
	public void t1shouldCreateApiKey() {
		// arrange
		ApiKey newApiKey = ApiKeyEntityBuilder.createApiKey();

		// act
		ApiKey createdApiKey = apiKeyDao.create(newApiKey);

		// assert
		assertNotNull(createdApiKey);
		assertNotNull(createdApiKey.getId());
	}

	@Test
	public void t2ShouldDeleteApiKeyById() {
		// arrange
		Long id = 1L;

		// act
		apiKeyDao.delete(id);

		// assert
		ApiKey apiKeyMarkedAsDeleted = apiKeyDao.findById(id);
		assertNotNull(apiKeyMarkedAsDeleted);
		assertThat(ResourceStatusLookup.DELETED.name().toLowerCase(),
				is(equalTo(apiKeyMarkedAsDeleted.getApiKeyResourceStatus().getResourceStatus().getDescription())));
	}

	@Test
	public void t3ShouldHandleDeleteApiKeyByInvalidId() {
		// arrange
		Long id = 999L;

		try {
			// act
			apiKeyDao.delete(id);
			fail("VoiceCollageException expected");
		} catch (AudioMinerException ex) {
			// assert
			assertNotNull(ex);
			assertThat(ex.getAdministratorErrorDescription(), containsString(GenericDaoImpl.ERR_ENTITY_DOES_NOT_EXIST));
			assertThat(ex.getAdministratorErrorCorrection(),
					is(equalTo(GenericDaoImpl.ERR_ENSURE_VALID_ID_ADMIN_CORRECTION)));
		}
	}

}
