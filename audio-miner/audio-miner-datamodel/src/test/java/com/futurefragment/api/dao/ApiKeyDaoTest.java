package com.futurefragment.api.dao;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.futurefragment.api.builder.ApiKeyEntityBuilder;
import com.futurefragment.api.entity.ApiKey;
import com.futurefragment.common.JpaHibernateTest;
import com.futurefragment.common.dao.GenericDaoImpl;
import com.futurefragment.exception.AudioMinerException;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ApiKeyDaoTest extends JpaHibernateTest {

	private static EntityManagerFactory emf;
	private static EntityManager em;

	private ApiKeyDao apiKeyDao;

	@BeforeClass
	public static void init() {
		emf = Persistence.createEntityManagerFactory(DEFAULT_PERSISTENCE_UNIT);
		em = emf.createEntityManager();
	}

	@Before
	public void setupTest() throws Exception {
		this.dbSetup(em);
		this.setupLiquibaseTestFixtureWithTestData();

		apiKeyDao = new ApiKeyDao();
		apiKeyDao.setEntityManager(em);
	}

	@After
	public void tearDown() throws Exception {
		this.dropDb();
	}

	@AfterClass
	public static void closeTestFixture() {
		em.close();
		emf.close();
	}

	@Test
	public void t1ShouldFindApiKeyById() throws Exception {
		// arrange
		Long id = 1L;

		// act
		ApiKey apiKey = apiKeyDao.findById(id);

		// assert
		assertNotNull(apiKey);
		assertThat(apiKey.getId(), is(equalTo(id)));
	}

	@Test
	public void t2ShouldHandleFindApiKeyByInvalidId() {
		// arrange
		Long id = 999999L;

		try {
			// act
			apiKeyDao.findById(id);
			fail("VoiceCollageException expected");
		} catch (AudioMinerException ex) {
			// assert
			assertNotNull(ex);
			assertThat(ex.getAdministratorErrorDescription(), containsString(GenericDaoImpl.ERR_ENTITY_DOES_NOT_EXIST));
			assertThat(ex.getAdministratorErrorCorrection(),
					is(equalTo(GenericDaoImpl.ERR_ENSURE_VALID_ID_ADMIN_CORRECTION)));
		}
	}

	@Test
	public void t3ShouldFindAllApiKeys() {
		// act
		List<ApiKey> resultList = apiKeyDao.findAll();

		// assert
		assertNotNull(resultList);
		assertThat(resultList.size(), is(equalTo(1)));
	}

	@Test
	public void t4ShouldUpdateApiKey() {
		// arrange
		String newDescription = "Frodo";
		ApiKey apiKey = apiKeyDao.findById(1L);
		apiKey.setDescription(newDescription);

		// act
		ApiKey updatedApiKey = apiKeyDao.update(apiKey);

		// assert
		assertNotNull(updatedApiKey);
		assertThat(updatedApiKey.getDescription(), is(equalTo(newDescription)));
	}

	@Test
	public void t5ShouldHandleUpdateApiKeyWithoutId() {
		// arrange
		Long id = 1L;
		ApiKey apiKey = apiKeyDao.findById(id);
		apiKey.setId(null);

		try {
			// act
			apiKeyDao.update(ApiKeyEntityBuilder.createApiKey());
			fail("VoiceCollageException expected");
		} catch (AudioMinerException ex) {
			// assert
			assertNotNull(ex);
			assertThat(ex.getAdministratorErrorDescription(),
					containsString(GenericDaoImpl.ERR_UPDATE_ADMIN_DESCRIPTION));
			assertThat(ex.getAdministratorErrorCorrection(),
					is(containsString(GenericDaoImpl.ERR_UPDATE_INVALID_ENTITY_OR_REMOVED_ADMIN_CORRECTION)));
		}
	}

	@Test
	public void t6ShouldHandleCreateApiKeyWithExistingId() {
		// arrange
		ApiKey createdApiKey = ApiKeyEntityBuilder.createApiKey();

		try {
			// act
			apiKeyDao.create(createdApiKey);
			fail("VoiceCollageException expected");
		} catch (AudioMinerException ex) {
			// assert
			assertNotNull(ex);
			assertThat(ex.getAdministratorErrorDescription(),
					containsString(GenericDaoImpl.ERR_ENTITY_EXISTS_ADMIN_DESCRIPTION));
			assertThat(ex.getAdministratorErrorCorrection(),
					is(equalTo(GenericDaoImpl.ERR_ENTITY_EXISTS_ADMIN_CORRECTION)));
		}
	}

	@Test
	public void t7ShouldFindApiKeyByKey() {
		// arrange
		String key = apiKeyDao.findById(1L).getKey();

		// act
		Optional<ApiKey> maybeApiKey = apiKeyDao.findApiKeyByKey(key);

		// assert
		assertNotNull(maybeApiKey);
		assertThat(maybeApiKey.get().getKey(), is(equalTo(key)));
	}

	@Test
	public void t8ShouldReturnEmptyOptionalWhenNoApiKeyIsFoundByKey() {
		// arrange
		String key = "";

		// act
		Optional<ApiKey> maybeApiKey = apiKeyDao.findApiKeyByKey(key);

		// assert
		assertNotNull(maybeApiKey);
		assertFalse(maybeApiKey.isPresent());
	}
}
