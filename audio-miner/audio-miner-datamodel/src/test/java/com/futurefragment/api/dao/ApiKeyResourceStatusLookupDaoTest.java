package com.futurefragment.api.dao;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.futurefragment.api.builder.ApiKeyEntityBuilder;
import com.futurefragment.api.entity.ApiKey;
import com.futurefragment.api.entity.ApiKeyResourceStatusLookup;
import com.futurefragment.common.JpaHibernateTest;
import com.futurefragment.common.dao.GenericDaoImpl;
import com.futurefragment.enumlookups.ResourceStatusLookup;
import com.futurefragment.exception.AudioMinerException;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ApiKeyResourceStatusLookupDaoTest extends JpaHibernateTest {

	private static EntityManagerFactory emf;
	private static EntityManager em;

	private ApiKeyDao apiKeyDao;
	private ApiKeyResourceStatusLookupDao apiKeyResourceStatusLookupDao;

	@BeforeClass
	public static void init() {
		emf = Persistence.createEntityManagerFactory(DEFAULT_PERSISTENCE_UNIT);
		em = emf.createEntityManager();
	}

	@Before
	public void setupTest() throws Exception {
		this.dbSetup(em);
		this.setupLiquibaseTestFixtureWithTestData();

		apiKeyDao = new ApiKeyDao();
		apiKeyDao.setEntityManager(em);

		apiKeyResourceStatusLookupDao = new ApiKeyResourceStatusLookupDao();
		apiKeyResourceStatusLookupDao.setEntityManager(em);
	}

	@After
	public void tearDown() throws Exception {
		this.dropDb();
	}

	@AfterClass
	public static void closeTestFixture() {
		em.close();
		emf.close();
	}

	@Test
	public void t1ShouldFindApiKeyResourceStatusById() {
		// arrange
		apiKeyDao.create(ApiKeyEntityBuilder.createApiKey());
		Long id = 1L;

		// act
		ApiKeyResourceStatusLookup apiKeyResourceStatusLookup = apiKeyResourceStatusLookupDao.findById(id);

		// assert
		assertNotNull(apiKeyResourceStatusLookup);
		assertThat(apiKeyResourceStatusLookup.getId(), is(equalTo(id)));
	}

	@Test
	public void t2ShouldHandleFindApiKeyResourceStatusByInvalidId() {
		// arrange
		Long id = 999999L;

		try {
			// act
			apiKeyResourceStatusLookupDao.findById(id);
			fail("VoiceCollageException expected");
		} catch (AudioMinerException ex) {
			// assert
			assertNotNull(ex);
			assertThat(ex.getAdministratorErrorDescription(), containsString(GenericDaoImpl.ERR_ENTITY_DOES_NOT_EXIST));
			assertThat(ex.getAdministratorErrorCorrection(),
					is(equalTo(GenericDaoImpl.ERR_ENSURE_VALID_ID_ADMIN_CORRECTION)));
		}
	}

	@Test
	public void t3ShouldUpdateApiKeyResourceStatus() {
		// arrange
		Long deletedResourceStatusId = ResourceStatusLookup.DELETED.getId();
		String deletedResourceDescription = ResourceStatusLookup.DELETED.name().toLowerCase();
		ApiKeyResourceStatusLookup apiKeyResourceStatusLookup = apiKeyResourceStatusLookupDao.findById(1L);
		apiKeyResourceStatusLookup.getResourceStatus().setId(deletedResourceStatusId);
		apiKeyResourceStatusLookup.getResourceStatus().setDescription(deletedResourceDescription);

		// act
		ApiKeyResourceStatusLookup updatedApiKeyResourceStatusLookup = apiKeyResourceStatusLookupDao
				.update(apiKeyResourceStatusLookup);

		// assert
		assertNotNull(updatedApiKeyResourceStatusLookup);
		assertThat(updatedApiKeyResourceStatusLookup.getResourceStatus().getId(), is(equalTo(deletedResourceStatusId)));
		assertThat(updatedApiKeyResourceStatusLookup.getResourceStatus().getDescription(),
				is(equalTo(deletedResourceDescription)));
	}

	@Test
	public void t4ShouldHandleUpdateApiKeyWithoutId() {
		// arrange
		ApiKey apiKey = ApiKeyEntityBuilder.createApiKey();

		try {
			// act
			apiKeyResourceStatusLookupDao.update(apiKey.getApiKeyResourceStatus());
			fail("VoiceCollageException expected");
		} catch (AudioMinerException ex) {
			// assert
			assertNotNull(ex);
			assertThat(ex.getAdministratorErrorDescription(),
					containsString(GenericDaoImpl.ERR_UPDATE_ADMIN_DESCRIPTION));
			assertThat(ex.getAdministratorErrorCorrection(),
					is(containsString(GenericDaoImpl.ERR_UPDATE_INVALID_ENTITY_OR_REMOVED_ADMIN_CORRECTION)));
		}
	}

}
