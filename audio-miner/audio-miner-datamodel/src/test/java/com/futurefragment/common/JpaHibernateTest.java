package com.futurefragment.common;

import java.sql.SQLException;
import java.util.Map;

import javax.persistence.EntityManager;

import org.h2.jdbcx.JdbcDataSource;

import liquibase.Liquibase;
import liquibase.database.Database;
import liquibase.database.DatabaseConnection;
import liquibase.database.DatabaseFactory;
import liquibase.exception.DatabaseException;
import liquibase.exception.LiquibaseException;
import liquibase.exception.LockException;
import liquibase.resource.ClassLoaderResourceAccessor;

public class JpaHibernateTest {

	private static final String DB_URL_KEY = "hibernate.connection.url";
	private static final String DEFAULT_SCHEMA = "audio_miner";
	private static final String CONTEXT = "test";
	private static final String CHANGELOG_FILE_WITH_TEST_DATA = "liquibase/test.db.changelog.xml";
	private static final String CHANGELOG_FILE_WITHOUT_TEST_DATA = "liquibase/test.db.changelog_noTestData.xml";
	
	protected static final String DEFAULT_PERSISTENCE_UNIT = "audio-miner-pu";

	private Liquibase liquibase;
	private JdbcDataSource dataSource;

	protected void dbSetup(EntityManager em, String changelogFile, String contexts)
			throws SQLException, LiquibaseException {
		dbSetup(em);
	}

	protected void dbSetup(EntityManager em) throws SQLException, LiquibaseException {
		Map<String, Object> propsMap = em.getEntityManagerFactory().getProperties();

		dataSource = new JdbcDataSource();
		dataSource.setURL((String) propsMap.get(DB_URL_KEY));
		dataSource.setUser("SA");
	}

	protected void liquibaseSetup(String changelogFile, String contexts, String defaultSchema)
			throws SQLException, LiquibaseException {
		DatabaseConnection dbConn = new liquibase.database.jvm.JdbcConnection(dataSource.getConnection());

		Database db = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(dbConn);
		if (defaultSchema != null) {
			db.setDefaultSchemaName(defaultSchema);
		}

		liquibase = new Liquibase(changelogFile, new ClassLoaderResourceAccessor(), db);
		liquibase.update(contexts);
	}

	protected void setupLiquibaseTestFixtureWithTestData() throws Exception {
		this.liquibaseSetup(CHANGELOG_FILE_WITH_TEST_DATA, CONTEXT, DEFAULT_SCHEMA);
	}

	protected void setupLiquibaseTestFixtureWithoutTestData() throws Exception {
		this.liquibaseSetup(CHANGELOG_FILE_WITHOUT_TEST_DATA, CONTEXT, DEFAULT_SCHEMA);
	}

	protected void dropDb() throws DatabaseException, LockException {
		liquibase.dropAll();
	}
}
