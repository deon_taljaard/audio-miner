package com.futurefragment.common;

import com.futurefragment.common.entity.ResourceStatus;

public final class ResourceStatusEntityBuilder {

	private ResourceStatusEntityBuilder() {
	}

	public static ResourceStatus createResourceStatus() {
		ResourceStatus resourceStatus = new ResourceStatus();

		resourceStatus.setId(1L);
		resourceStatus.setDescription("active");

		return resourceStatus;
	}
}
