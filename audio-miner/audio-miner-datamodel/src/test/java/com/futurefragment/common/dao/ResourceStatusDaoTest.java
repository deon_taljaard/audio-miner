package com.futurefragment.common.dao;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNotNull;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.futurefragment.common.JpaHibernateTest;
import com.futurefragment.common.entity.ResourceStatus;
import com.futurefragment.exception.AudioMinerException;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ResourceStatusDaoTest extends JpaHibernateTest {

	private static EntityManagerFactory emf;
	private static EntityManager em;

	private ResourceStatusDao resourceStatusDao;

	@BeforeClass
	public static void init() {
		emf = Persistence.createEntityManagerFactory(DEFAULT_PERSISTENCE_UNIT);
		em = emf.createEntityManager();
	}

	@Before
	public void setupTest() throws Exception {
		this.dbSetup(em);
		this.setupLiquibaseTestFixtureWithTestData();

		resourceStatusDao = new ResourceStatusDao();
		resourceStatusDao.setEntityManager(em);
	}

	@After
	public void tearDown() throws Exception {
		this.dropDb();
	}

	@AfterClass
	public static void closeTestFixture() {
		em.close();
		emf.close();
	}

	@Test
	public void t1ShouldFindResourceStatusById() throws Exception {
		// arrange
		Long id = 1L;

		// act
		ResourceStatus resourceStatus = resourceStatusDao.findById(id);

		// assert
		assertNotNull(resourceStatus);
		assertThat(resourceStatus.getId(), is(equalTo(id)));
	}

	@Test
	public void t2ShouldHandleFindResourceStatusByInvalidId() {
		// arrange
		Long id = 999999L;

		try {
			// act
			resourceStatusDao.findById(id);
		} catch (AudioMinerException ex) {
			// assert
			assertNotNull(ex);
			assertThat(ex.getAdministratorErrorDescription(), containsString(GenericDaoImpl.ERR_ENTITY_DOES_NOT_EXIST));
			assertThat(ex.getAdministratorErrorCorrection(),
					is(equalTo(GenericDaoImpl.ERR_ENSURE_VALID_ID_ADMIN_CORRECTION)));
		}
	}

	@Test
	public void t7ShouldFindResourceStatusByResourceStatusName() throws Exception {
		// arrange
		String resourceStatusDescription = "active";

		// act
		ResourceStatus resourceStatus = resourceStatusDao.findByDescription(resourceStatusDescription);

		// assert
		assertNotNull(resourceStatus);
		assertThat(resourceStatus.getDescription(), is(equalTo(resourceStatusDescription)));
	}

	@Test
	public void t8ShouldHandleFindResourceStatusByInvalidResourceStatusName() throws Exception {
		// arrange
		String resourceStatusName = "active";

		try {
			// act
			resourceStatusDao.findByDescription(resourceStatusName);
		} catch (AudioMinerException ex) {
			// assert
			assertNotNull(ex);
			assertThat(ex.getAdministratorErrorDescription(),
					is(equalTo(ResourceStatusDao.ERR_INVALID_DESCRIPTION_DESCRIPTION)));
			assertThat(ex.getAdministratorErrorCorrection(),
					is(equalTo(ResourceStatusDao.ERR_INVALID_DESCRIPTION_CORRECTION)));
		}
	}

	@Test
	public void t9ShouldFindResourceStatusByResourceStatusNameWithCaseInsensitiveSearch() throws Exception {
		// arrange
		String resourceStatusDescription = "aCtIve";

		// act
		ResourceStatus resourceStatus = resourceStatusDao.findByDescription(resourceStatusDescription);

		// assert
		assertNotNull(resourceStatus);
		assertThat(resourceStatus.getDescription().toLowerCase(), is(equalTo(resourceStatusDescription.toLowerCase())));
	}
}
