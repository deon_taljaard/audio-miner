package com.futurefragment.audio.builder;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.stream.LongStream;

import com.futurefragment.audio.entity.Audio;
import com.futurefragment.audio.entity.CutAudio;
import com.futurefragment.util.KeyGenerator;

public final class CutAudioEntityBuilder {

	private CutAudioEntityBuilder() {
	}

	public static List<CutAudio> getCutAudios() {
		List<CutAudio> voices = new ArrayList<>();

		LongStream.range(1, 2).forEach(i -> voices.add(createCutAudioWithId(i)));

		return voices;
	}

	public static CutAudio createCutAudio() {
		CutAudio cutAudio = new CutAudio();

		cutAudio.setDescription("Audio desc ");
		Audio audio = new Audio();
		audio.setId(1L);
		cutAudio.setAudio(audio);
		cutAudio.setCreatedDate(Calendar.getInstance().getTime());
		cutAudio.setSequenceId(1);
		cutAudio.setResourcePath(KeyGenerator.newKey());

		return cutAudio;
	}

	public static CutAudio createCutAudioWithId(Long i) {
		CutAudio cutAudio = createCutAudio();

		cutAudio.setId(i);

		return cutAudio;
	}
}
