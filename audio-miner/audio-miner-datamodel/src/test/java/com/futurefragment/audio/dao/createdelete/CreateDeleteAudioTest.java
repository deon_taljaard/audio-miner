package com.futurefragment.audio.dao.createdelete;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.futurefragment.audio.builder.AudioEntityBuilder;
import com.futurefragment.audio.dao.AudioDao;
import com.futurefragment.audio.entity.Audio;
import com.futurefragment.common.JpaHibernateTest;
import com.futurefragment.common.dao.GenericDaoImpl;
import com.futurefragment.exception.AudioMinerException;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CreateDeleteAudioTest extends JpaHibernateTest {

	private static EntityManagerFactory emf;
	private static EntityManager em;

	private AudioDao voiceDao;

	@BeforeClass
	public static void init() {
		emf = Persistence.createEntityManagerFactory(DEFAULT_PERSISTENCE_UNIT);
		em = emf.createEntityManager();
	}

	@Before
	public void setupTest() throws Exception {
		this.dbSetup(em);
		this.setupLiquibaseTestFixtureWithoutTestData();

		voiceDao = new AudioDao();
		voiceDao.setEntityManager(em);
	}

	@After
	public void tearDown() throws Exception {
		this.dropDb();
	}

	@AfterClass
	public static void closeTestFixture() {
		em.close();
		emf.close();
	}

	@Test
	public void t1shouldCreateVoice() {
		// arrange
		Audio newVoice = AudioEntityBuilder.createAudio();

		// act
		Audio createdVoice = voiceDao.create(newVoice);

		// assert
		assertNotNull(createdVoice);
		assertNotNull(createdVoice.getId());
	}

	@Test
	public void t2ShouldDeleteVoiceById() {
		// arrange
		Long id = 1L;

		// act
		voiceDao.delete(id);

		try {
			voiceDao.findById(id);
			fail("VoiceCollageException expected");
		} catch (AudioMinerException ex) {
			// assert
			assertNotNull(ex);
			assertThat(ex.getAdministratorErrorDescription(), containsString(GenericDaoImpl.ERR_ENTITY_DOES_NOT_EXIST));
			assertThat(ex.getAdministratorErrorCorrection(),
					is(equalTo(GenericDaoImpl.ERR_ENSURE_VALID_ID_ADMIN_CORRECTION)));
		}
	}

	@Test
	public void t3ShouldHandleDeleteVoiceByInvalidId() {
		// arrange
		Long id = 999L;

		try {
			// act
			voiceDao.delete(id);
			fail("VoiceCollageException expected");
		} catch (AudioMinerException ex) {
			// assert
			assertNotNull(ex);
			assertThat(ex.getAdministratorErrorDescription(),
					containsString(GenericDaoImpl.ERR_DELETE_ENTITY_ADMIN_DESCRIPTION));
			assertThat(ex.getAdministratorErrorCorrection(),
					is(equalTo(GenericDaoImpl.ERR_ENSURE_VALID_ID_ADMIN_CORRECTION)));
		}
	}

}
