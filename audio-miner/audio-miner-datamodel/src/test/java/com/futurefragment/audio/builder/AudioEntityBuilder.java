package com.futurefragment.audio.builder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.stream.LongStream;

import com.futurefragment.audio.entity.Audio;
import com.futurefragment.util.KeyGenerator;

public final class AudioEntityBuilder {

	private AudioEntityBuilder() {
	}

	public static List<Audio> getAudios() {
		List<Audio> voices = new ArrayList<>();

		LongStream.range(1, 2).forEach(i -> voices.add(createAudioWithId(i)));

		return voices;
	}

	public static Audio createAudio() {
		Audio audio = new Audio();

		audio.setDescription("Audio desc ");
		audio.setCreatedDate(Calendar.getInstance().getTime());
		audio.setResourcePath(KeyGenerator.newKey());
		audio.setCutAudios(Arrays.asList(CutAudioEntityBuilder.createCutAudio()));

		return audio;
	}

	public static Audio createAudioWithId(Long i) {
		Audio audio = createAudio();

		audio.setId(i);

		return audio;
	}
}
