package com.futurefragment.audio.dao;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.futurefragment.audio.builder.AudioEntityBuilder;
import com.futurefragment.audio.entity.Audio;
import com.futurefragment.common.JpaHibernateTest;
import com.futurefragment.common.dao.GenericDaoImpl;
import com.futurefragment.exception.AudioMinerException;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class AudioDaoTest extends JpaHibernateTest {

	private static EntityManagerFactory emf;
	private static EntityManager em;

	private AudioDao audioDao;

	@BeforeClass
	public static void init() {
		emf = Persistence.createEntityManagerFactory(DEFAULT_PERSISTENCE_UNIT);
		em = emf.createEntityManager();
	}

	@Before
	public void setupTest() throws Exception {
		this.dbSetup(em);
		this.setupLiquibaseTestFixtureWithTestData();

		audioDao = new AudioDao();
		audioDao.setEntityManager(em);
	}

	@After
	public void tearDown() throws Exception {
		this.dropDb();
	}

	@AfterClass
	public static void closeTestFixture() {
		em.close();
		emf.close();
	}

	@Test
	public void t1ShouldFindAudioById() throws Exception {
		// arrange
		Long id = 1L;

		// act
		Audio audio = audioDao.findById(id);

		// assert
		assertNotNull(audio);
		assertThat(audio.getId(), is(equalTo(id)));
	}

	@Test
	public void t2ShouldHandleFindAudioByInvalidId() {
		// arrange
		Long id = 999999L;

		try {
			// act
			audioDao.findById(id);
			fail("VoiceCollageException expected");
		} catch (AudioMinerException ex) {
			// assert
			assertNotNull(ex);
			assertThat(ex.getAdministratorErrorDescription(), containsString(GenericDaoImpl.ERR_ENTITY_DOES_NOT_EXIST));
			assertThat(ex.getAdministratorErrorCorrection(),
					is(equalTo(GenericDaoImpl.ERR_ENSURE_VALID_ID_ADMIN_CORRECTION)));
		}
	}

	@Test
	public void t3ShouldFindAllAudios() {
		// act
		List<Audio> resultList = audioDao.findAll();

		// assert
		assertNotNull(resultList);
		assertThat(resultList.size(), is(equalTo(2)));
	}

	@Test
	public void t4ShouldHandleCreateAudioWithExistingId() {
		// arrange
		Audio createdAudio = AudioEntityBuilder.createAudio();

		try {
			// act
			audioDao.create(createdAudio);
			fail("VoiceCollageException expected");
		} catch (AudioMinerException ex) {
			// assert
			assertNotNull(ex);
			assertThat(ex.getAdministratorErrorDescription(),
					containsString(GenericDaoImpl.ERR_ENTITY_EXISTS_ADMIN_DESCRIPTION));
			assertThat(ex.getAdministratorErrorCorrection(),
					is(equalTo(GenericDaoImpl.ERR_ENTITY_EXISTS_ADMIN_CORRECTION)));
		}
	}

}
