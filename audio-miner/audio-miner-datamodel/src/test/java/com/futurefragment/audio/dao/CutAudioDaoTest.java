package com.futurefragment.audio.dao;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.futurefragment.audio.builder.CutAudioEntityBuilder;
import com.futurefragment.audio.entity.CutAudio;
import com.futurefragment.common.JpaHibernateTest;
import com.futurefragment.common.dao.GenericDaoImpl;
import com.futurefragment.exception.AudioMinerException;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CutAudioDaoTest extends JpaHibernateTest {

	private static EntityManagerFactory emf;
	private static EntityManager em;

	private CutAudioDao cutAudioDao;

	@BeforeClass
	public static void init() {
		emf = Persistence.createEntityManagerFactory(DEFAULT_PERSISTENCE_UNIT);
		em = emf.createEntityManager();
	}

	@Before
	public void setupTest() throws Exception {
		this.dbSetup(em);
		this.setupLiquibaseTestFixtureWithTestData();

		cutAudioDao = new CutAudioDao();
		cutAudioDao.setEntityManager(em);
	}

	@After
	public void tearDown() throws Exception {
		this.dropDb();
	}

	@AfterClass
	public static void closeTestFixture() {
		em.close();
		emf.close();
	}

	@Test
	public void t1ShouldFindCutAudioById() throws Exception {
		// arrange
		Long id = 1L;

		// act
		CutAudio cutAudio = cutAudioDao.findById(id);

		// assert
		assertNotNull(cutAudio);
		assertThat(cutAudio.getId(), is(equalTo(id)));
	}

	@Test
	public void t2ShouldHandleFindCutAudioByInvalidId() {
		// arrange
		Long id = 999999L;

		try {
			// act
			cutAudioDao.findById(id);
			fail("VoiceCollageException expected");
		} catch (AudioMinerException ex) {
			// assert
			assertNotNull(ex);
			assertThat(ex.getAdministratorErrorDescription(), containsString(GenericDaoImpl.ERR_ENTITY_DOES_NOT_EXIST));
			assertThat(ex.getAdministratorErrorCorrection(),
					is(equalTo(GenericDaoImpl.ERR_ENSURE_VALID_ID_ADMIN_CORRECTION)));
		}
	}

	@Test
	public void t3ShouldFindAllCutAudios() {
		// act
		List<CutAudio> resultList = cutAudioDao.findAll();

		// assert
		assertNotNull(resultList);
		assertThat(resultList.size(), is(equalTo(2)));
	}

	@Test
	public void t4ShouldHandleCreateCutAudioWithExistingId() {
		// arrange
		CutAudio createdCutAudio = CutAudioEntityBuilder.createCutAudio();

		try {
			// act
			cutAudioDao.create(createdCutAudio);
			fail("VoiceCollageException expected");
		} catch (AudioMinerException ex) {
			// assert
			assertNotNull(ex);
			assertThat(ex.getAdministratorErrorDescription(),
					containsString(GenericDaoImpl.ERR_ENTITY_EXISTS_ADMIN_DESCRIPTION));
			assertThat(ex.getAdministratorErrorCorrection(),
					is(equalTo(GenericDaoImpl.ERR_ENTITY_EXISTS_ADMIN_CORRECTION)));
		}
	}

	@Test
	public void t5ShouldFindCutAudioByDescription() {
		// arrange
		String description = "Audio 1";

		Optional<CutAudio> cutAudio = cutAudioDao.findCutAudioByName(description);

		assertNotNull(cutAudio);
		assertThat(cutAudio.get().getDescription(), is(equalTo(description)));
	}

	@Test
	public void t6ShouldReturnEmptyOptionalWhenNoCutAudioIsFoundByDescription() {
		// arrange
		String description = "Unknown";

		Optional<CutAudio> cutAudio = cutAudioDao.findCutAudioByName(description);

		assertNotNull(cutAudio);
		assertFalse(cutAudio.isPresent());
	}

	@Test
	public void t7ShouldFindVoiceByDescriptionWithCaseInsensitiveSearch() {
		// arrange
		String audioName = "aUdIo 2";

		// act
		Optional<CutAudio> cutAudio = cutAudioDao.findCutAudioByName(audioName);

		// assert
		assertNotNull(cutAudio);
		assertThat(cutAudio.get().getDescription().toLowerCase(), is(equalTo(audioName.toLowerCase())));
	}

}
